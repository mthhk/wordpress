<?php
/*
Plugin Name: Guardmaster Demo Content
Plugin URI: http://templatation.com/
Author: Templatation
Author URI: http://templatation.com/
Version: 1.0
Description: Adds a feature to import Guardmaster theme demo. This plugin only works if Templatation-framework is active.
Text Domain: templatation
*/

// Define Constants
defined('TT_ROOT')		or define('TT_ROOT', dirname(__FILE__));
defined('TT_VERSION')	or define('TT_VERSION', '1.0');


//echo TT_ROOT;

if(!class_exists('Webapp_demo_inst')) {

	class Webapp_demo_inst {

		public function __construct() {
			$this->js_load_installer();
		}


		public function js_load_installer() {

				require_once(TT_ROOT .'/'. 'tt-redux-extensions/loader.php' );

		}


		public function tt_dynamic_section( $sections ) {

				$sections[] = array(
					'id'     => 'wbc_importer_section',
					'title'  => esc_html__( 'Demo Content Importer', 'templatation' ),
					// 'desc'   => esc_html__( 'Description Goes Here', 'templatation' ),
					'icon'   => 'el-icon-website',
					'fields' => array(
/*
						array(
							'id'     => 'tt_homepage_notice1',
							'type'   => 'info',
							'notice' => false,
							'style'  => 'info',
							'title'  => __( 'Import Demo Data', 'templatation' ),
							'desc'   => sprintf( __( 'Best if used on new WordPress install. If you are switching demos, or its not a new install, Its recommended that you reset WordPress to its original state using %sWordPress Reset%s Plugin. Be careful though as it deletes all data.', 'templatation' ), '<a href="https://wordpress.org/plugins/wordpress-reset/">', '</a>' ),
						),
						array(
							'id'    => 'opt-notice-info1',
							'type'  => 'info',
							'style' => 'info',
							'title' => __( 'Required plugins.', 'templatation' ),
							'desc'  => __( 'Theme requires few bundled plugins for its full power. Please make sure you have Visual Composer active. You can check by clicking Plugins on left main menu. If they are not, Go to Appearance-> Install plugins(If you do not see this menu, dont worry that means all required plugins are active) to install.', 'templatation' ),
						),
						array(
							'id'   => 'opt-divide44',
							'type' => 'divide'
						),
						array(
							'id'   => 'wbc_demo_importer',
							'type' => 'wbc_importer'
						)*/
					),
				);
				return $sections;
		}

	} // end of class

	new Webapp_demo_inst;
} // end of class_exists



// Function hook on demo installer to setup rest of the stuff.
add_action( 'wbc_importer_after_content_import', 'tt_demo_import', 10, 2 );
function tt_demo_import() {
	// Set reading options
	$homepge = get_page_by_title( 'Home' );
	$posts_pge = get_page_by_title( 'Blog' );
	if( isset( $homepge ) && $homepge->ID ) {
		update_option('show_on_front', 'page');
		update_option('page_on_front', $homepge->ID); // setting up homepage
	}
	if( isset( $posts_pge ) && $posts_pge->ID ) {
		update_option('page_for_posts', $posts_pge->ID); // setting up blog
	}
	if (function_exists('tt_import_rev')) tt_import_rev(); // import revslider
	if (function_exists('tt_set_demo_menus')) tt_set_demo_menus(); // setup menu location

	wp_delete_post(1); // delete hello world post.

} // end of tt_demo_import

if ( ! function_exists( 'tt_set_demo_menus' ) ) {
	function tt_set_demo_menus() {

		// Menus to Import and assign - you can remove or add as many as you want
		$main_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );

		set_theme_mod( 'nav_menu_locations', array(
				'primary-menu' => $main_menu->term_id
			)
		);

	}
}

if ( ! function_exists( 'tt_import_rev' ) ) {
	function tt_import_rev() { // Import Revslider
		global $wpdb;

		if ( class_exists( 'UniteFunctionsRev' ) ) { // if revslider is activated
			$rev_directory = TT_ROOT .'/'. 'revslider/'; // revsliders data dir

			foreach ( glob( $rev_directory . '*.zip' ) as $filename ) { // get all files from revsliders data dir
				$filename    = basename( $filename );
				$rev_files[] = TT_ROOT .'/'. 'revslider/' . $filename;
			}
			$slider = new RevSliderSlider();
			foreach ( $rev_files as $rev_file ) {
				$slider->importSliderFromPost( true, false, $rev_file );
			}
		}
	}
}

if ( !function_exists( 'tt_importer_description_text' ) ) {

	/**
	 * Filter for changing importer description info in options panel
	 * when not setting in Redux config file.
	 *
	 * @param [string] $title description above demos
	 *
	 * @return [string] return.
	 */

	function tt_importer_description_text( $description ) {

		$message = '<p>'. sprintf( __( 'Best if used on new WordPress install. If you are switching demos, or its not a new install, Its recommended that you reset WordPress to its original state using %sWordPress Reset%s Plugin. Be careful though as it deletes all data.', 'templatation' ), '<a href="https://wordpress.org/plugins/wordpress-reset/">', '</a>' ) .'</p>';
		$message .= '<p>'. 'Theme requires few bundled plugins for its full power. Please make sure you have Visual Composer active. You can check by clicking Plugins on left main menu. If they are not, Go to Appearance-> Install plugins(If you do not see this menu, dont worry that means all required plugins are active) to install.' .'</p>';
		$message .= '<p>'. esc_html__( 'For header 2, you might need to go to Layout above and select Header 2 manually after import process finishes.', 'templatation' ) .'</p>';

		return $message;
	}

	add_filter( 'wbc_importer_description', 'tt_importer_description_text', 10 );
}
