<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * The default template for displaying content
 */

//check if post has the image.
$postimgg = '';
if(function_exists('tt_gmaster_post_thumb')) $postimgg = tt_gmaster_post_thumb();
?>
<div <?php post_class( 'row blog-post no-gutter-5' ); ?>>
    <?php if( !empty($postimgg)) { ?>
    <div class="col-lg-5 col-md-5 col-sm-5 post-picture">
        <?php echo wp_kses_post($postimgg); ?>
    </div>
    <?php } ?>
    <div class="<?php echo empty($postimgg) ? 'tt-noimg' : 'col-lg-7 col-md-7 col-sm-7'; ?> post-contents">
        <div class="meta"><span class="gm-comma">-:</span> <?php the_time('M, Y'); ?>  <span> /  <?php the_author(); ?> </span></div>
        <h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        <div class="description">
            <?php tt_gmaster_excerpt_charlength('200'); ?>
        </div>
        <div class="clearfix">
            <div class="button"><a href="<?php the_permalink() ?>" class="btn-orange"> <?php esc_html_e( 'Read More: ', 'guardmaster'); ?><i class="fa fa-chevron-right"></i></a></div>
            <?php if( comments_open()) { ?><div class="comments"><?php comments_number( '0', '1', '%' ); ?></div><?php } ?>
        </div>
    </div>
</div>
