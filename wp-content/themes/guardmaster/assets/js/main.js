// JavaScript Document
jQuery(document).ready(function($) {


/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 */

jQuery.easing.jswing=jQuery.easing.swing;jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,f,a,h,g){return jQuery.easing[jQuery.easing.def](e,f,a,h,g)},easeInQuad:function(e,f,a,h,g){return h*(f/=g)*f+a},easeOutQuad:function(e,f,a,h,g){return -h*(f/=g)*(f-2)+a},easeInOutQuad:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f+a}return -h/2*((--f)*(f-2)-1)+a},easeInCubic:function(e,f,a,h,g){return h*(f/=g)*f*f+a},easeOutCubic:function(e,f,a,h,g){return h*((f=f/g-1)*f*f+1)+a},easeInOutCubic:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f+a}return h/2*((f-=2)*f*f+2)+a},easeInQuart:function(e,f,a,h,g){return h*(f/=g)*f*f*f+a},easeOutQuart:function(e,f,a,h,g){return -h*((f=f/g-1)*f*f*f-1)+a},easeInOutQuart:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f+a}return -h/2*((f-=2)*f*f*f-2)+a},easeInQuint:function(e,f,a,h,g){return h*(f/=g)*f*f*f*f+a},easeOutQuint:function(e,f,a,h,g){return h*((f=f/g-1)*f*f*f*f+1)+a},easeInOutQuint:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f*f+a}return h/2*((f-=2)*f*f*f*f+2)+a},easeInSine:function(e,f,a,h,g){return -h*Math.cos(f/g*(Math.PI/2))+h+a},easeOutSine:function(e,f,a,h,g){return h*Math.sin(f/g*(Math.PI/2))+a},easeInOutSine:function(e,f,a,h,g){return -h/2*(Math.cos(Math.PI*f/g)-1)+a},easeInExpo:function(e,f,a,h,g){return(f==0)?a:h*Math.pow(2,10*(f/g-1))+a},easeOutExpo:function(e,f,a,h,g){return(f==g)?a+h:h*(-Math.pow(2,-10*f/g)+1)+a},easeInOutExpo:function(e,f,a,h,g){if(f==0){return a}if(f==g){return a+h}if((f/=g/2)<1){return h/2*Math.pow(2,10*(f-1))+a}return h/2*(-Math.pow(2,-10*--f)+2)+a},easeInCirc:function(e,f,a,h,g){return -h*(Math.sqrt(1-(f/=g)*f)-1)+a},easeOutCirc:function(e,f,a,h,g){return h*Math.sqrt(1-(f=f/g-1)*f)+a},easeInOutCirc:function(e,f,a,h,g){if((f/=g/2)<1){return -h/2*(Math.sqrt(1-f*f)-1)+a}return h/2*(Math.sqrt(1-(f-=2)*f)+1)+a},easeInElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return -(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e},easeOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return g*Math.pow(2,-10*h)*Math.sin((h*k-i)*(2*Math.PI)/j)+l+e},easeInOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k/2)==2){return e+l}if(!j){j=k*(0.3*1.5)}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}if(h<1){return -0.5*(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e}return g*Math.pow(2,-10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j)*0.5+l+e},easeInBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*(f/=h)*f*((g+1)*f-g)+a},easeOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*((f=f/h-1)*f*((g+1)*f+g)+1)+a},easeInOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}if((f/=h/2)<1){return i/2*(f*f*(((g*=(1.525))+1)*f-g))+a}return i/2*((f-=2)*f*(((g*=(1.525))+1)*f+g)+2)+a},easeInBounce:function(e,f,a,h,g){return h-jQuery.easing.easeOutBounce(e,g-f,0,h,g)+a},easeOutBounce:function(e,f,a,h,g){if((f/=g)<(1/2.75)){return h*(7.5625*f*f)+a}else{if(f<(2/2.75)){return h*(7.5625*(f-=(1.5/2.75))*f+0.75)+a}else{if(f<(2.5/2.75)){return h*(7.5625*(f-=(2.25/2.75))*f+0.9375)+a}else{return h*(7.5625*(f-=(2.625/2.75))*f+0.984375)+a}}}},easeInOutBounce:function(e,f,a,h,g){if(f<g/2){return jQuery.easing.easeInBounce(e,f*2,0,h,g)*0.5+a}return jQuery.easing.easeOutBounce(e,f*2-g,0,h,g)*0.5+h*0.5+a}});

	SmoothMenuScroll();
	function SmoothMenuScroll () {
		var anchor = jQuery('.nav .menu-item');
		if(anchor.length){
			anchor.children('a').bind('click', function (event) {
				if (jQuery(window).scrollTop() > 10) {
					var headerH = '45';
				}else {
					var headerH = '125';
				}
				var target = jQuery(this);
				jQuery('html, body').stop().animate({
					scrollTop: jQuery(target.attr('href')).offset().top - headerH + 'px'
				}, 1200, 'easeInOutExpo');
				/*anchor.removeClass('current');*/
				/*target.parent().addClass('current');*/
				event.preventDefault();
			});
		}
	}

    'use strict';

    /************************************************************************************ CAROUSEL STARTS */


    var owl_team = $('.our-team-carousel');
    if(owl_team.length){
        owl_team.owlCarousel({

            autoplay: 400,
            autoplayHoverPause: true,
            nav: false,
            dots: true,
            mouseDrag: true,
            margin: 0,
            loop: true,
            singleItem: true,
            navText: [
                "<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 2
                }
            },
            loop: owl_team.children().length > 1
        });
    }



    var owl_news = $('.news-carousel');
    if(owl_news.length){
        owl_news.owlCarousel({

            autoplay: 400,
            autoplayHoverPause: true,
            nav: false,
            dots: true,
            mouseDrag: true,
            margin: 0,
            loop: true,
            singleItem: true,
            navText: [
                "<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 2
                }
            },
            loop: owl_news.children().length > 1
        });
    }



    var owl_testimonials = $('.testimonials-carousel');
    if(owl_testimonials.length){
        owl_testimonials.owlCarousel({

            autoplay: 400,
            autoplayHoverPause: true,
            nav: false,
            dots: true,
            mouseDrag: true,
            margin: 0,
            loop: true,
            singleItem: true,
            navText: [
                "<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            },
            loop: owl_testimonials.children().length > 1
        });
    }




    var owl_clients = $('.clients-carousel');
    if(owl_clients.length){
        owl_clients.owlCarousel({

            autoplay: 400,
            autoplayHoverPause: true,
            nav: false,
            dots: true,
            mouseDrag: true,
            margin: 0,
            loop: true,
            singleItem: true,
            navText: [
                "<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 5
                }
            },
            loop: owl_clients.children().length > 1
        });        
    }



    /************************************************************************************ SEARCH TOGGLE STARTS */


    var $searchlink = $('#searchlink');

    /** on click effect**/
    $searchlink.on('click', function(e) {
        var target = e ? e.target : window.event.srcElement;

        if ($(target).attr('id') == 'searchlink') {
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
            } else {
                $(this).addClass('open');
            }
        }
    });

    /************************************************************************************ SEARCH TOGGLE ENDS */


    /************************************************************************************ CAROUSEL ENDS */


    $('[data-toggle="tooltip"]').tooltip();




    /************************************************************************************ TO TOP STARTS */

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').on("click", function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /************************************************************************************ TO TOP ENDS */

    /************************************************************************************ STICKY NAVIGATION STARTS */
	var winScr;
	$(window).on("scroll", function() {
		winScr = $(window).scrollTop();
		stickHeader();
	});
	
	function stickHeader(){
		if ($(window).width() > 991) {	
			if (winScr > 90){
				$("#navigation").addClass("header-fixed");
			} else {
				$("#navigation").removeClass("header-fixed");
			}
		} else {
			$("#navigation").removeClass("header-fixed");
		}		
	}	
/*
    $("#navigation").sticky({
        topSpacing: 0
    });
*/

    /************************************************************************************ STICKY NAVIGATION ENDS */
	
	/************************************************************************************ MOBILE  NAVIGATION STARTS */
	$('.navbar-toggle').on('click', function(){
		$(this).parents('.navbar').find('.navbar-collapse').slideToggle();
		return false;
	});
	$('#navigation ul.nav li.menu-item-has-children > a').on('click', function(){
		if($(window).width()<768){
			$(this).siblings('.sub-menu').slideToggle();
			return false;			
		}
	});
	/************************************************************************************ MOBILE NAVIGATION STARTS */

    /************************************************************************************ FITVID STARTS */

    $(".fitvid").fitVids();

    /************************************************************************************ FITVID ENDS */

    /************************************************************************************ MAGNIFIC POPUP STARTS */

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        /*disableOn: 700,*/
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true
        }

    });

    $('.image-popup-fit-width').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: {
            verticalFit: false
        }
    });

    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    $('.simple-ajax-popup-align-top').magnificPopup({
        type: 'ajax',
        alignTop: true,
        overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
    });

    $('.simple-ajax-popup').magnificPopup({
        type: 'ajax'
    });


    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
            }
        }
    });


    /************************************************************************************ MAGNIFIC POPUP ENDS */

    /************************************************************************************ FAQ STARTS */

    function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator2")
            .toggleClass('fa-plus fa-minus');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);


    $('.panel-collapse').on('show.bs.collapse', function() {
        $(this).prev('.panel-heading').find('.panel-title').addClass("active-panel");
    });

    $('.panel-collapse').on('hide.bs.collapse', function() {
        $(this).prev('.panel-heading').find('.panel-title').removeClass("active-panel");
    });

    /************************************************************************************ FAQ ENDS */

    /************************************************************************************ OVERLAY NAVIGATION ENDS */


    function init() {
        window.addEventListener('scroll', function(e) {
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 90,
                nav = document.querySelector("nav");
            if (distanceY > shrinkOn) {
                classie.add(nav, "smaller");
            } else {
                if (classie.has(nav, "smaller")) {
                    classie.remove(nav, "smaller");
                }
            }
        });
    }
    window.onload = init();


    /************************************************************************************ OVERLAY NAVIGATION ENDS */

    /************************************************************************************ REMOVE CLASSES STARTS */

    var win = $(this);

    if ($(window).width() < 991) {

        $('.row.no-gutter').removeClass('no-gutter-4');
        $('.row.no-gutter').removeClass('no-gutter-5');
        $('.row.no-gutter').removeClass('no-gutter-6');

    } else {

        $('.row.no-gutter').addClass('no-gutter-4');
        $('.row.no-gutter').addClass('no-gutter-5');
        $('.row.no-gutter').addClass('no-gutter-6');
    }


    $(window).resize(function() {
		
        if ($(window).width() < 991) {
            //do whatever you want
            $('.row.no-gutter').removeClass('no-gutter-4');
            $('.row.no-gutter').removeClass('no-gutter-5');
            $('.row.no-gutter').removeClass('no-gutter-6');

        } else {

            $('.row.no-gutter').addClass('no-gutter-4');
            $('.row.no-gutter').addClass('no-gutter-5');
            $('.row.no-gutter').addClass('no-gutter-6');
        }
    });

    /************************************************************************************ REMOVE CLASSES ENDS */

    $('.history-timeline-tabs').find('.nav-justified li').first().addClass('active');
    $('.history-timeline-tabs').find('.tab-content .tab-pane').first().addClass('active');

    $('.accordion-02').find('.panel').first().find('.panel-title').addClass('active-panel');
    $('.accordion-02').find('.panel').first().find('.panel-title i').removeClass('fa-plus').addClass('fa-minus');
    $('.accordion-02').find('.panel').first().find('.collapse').addClass('in');

    var count = 0;
    $('.accordion-02 .panel').each(function() {
        count++;

        $(this).attr('href','#collapse-' + count);
        $(this).find('.panel-heading').attr('href','#collapse-' + count);
        $(this).find('.panel-collapse').attr('id','collapse-' + count);
    })

    /*Enter your js here. before the closing bracket.*/
if ($(window).width() < 767) {
	$('.menu-item-has-children').append('<i class="fa fa-caret-down" aria-hidden="true"></i>');
	$('.menu-item-has-children i.fa').on('click', function(){
		$(this).parent().find('.sub-menu').slideToggle();
	});
}
}); 