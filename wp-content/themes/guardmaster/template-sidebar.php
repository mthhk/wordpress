<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
/**
 * Template Name: Page with sidebar
 *
 * This page has no sidebar.
 *
 */

 get_header();

?>
<!-- MAIN CONTENT BLOCK -->

<?php do_action( 'tt_before_mainblock' ); ?>
<section id="mainblock" class="mainblock">
	<div class="container">
		<div class="row">
			<div <?php post_class( tt_temptt_sidebar_pos() ); ?>>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php echo tt_temptt_post_title('h1'); ?>
				<?php the_content(); ?>
			<?php endwhile; endif;

				// Comments
				if ( comments_open() ) {  ?>
				 <div class="comment-part">
				 <?php comments_template( '', true );  ?>
				 </div>
				<?php
				}
	            ?>

			</div>
			<?php if ( function_exists( 'tt_temptt_get_sidebar' ) ) echo tt_temptt_get_sidebar(); ?>
		</div>

	</div> <!-- ./container -->
</section>

<?php get_footer(); ?>