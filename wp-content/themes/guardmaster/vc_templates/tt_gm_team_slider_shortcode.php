<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>

<div class="home-our-team">
    <div class="our-team-carousel">
        <?php print do_shortcode($content); ?>
    </div>
</div>