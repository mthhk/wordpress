<?php
/*
 * Templatation.com
 *
 * Title with separator shortcode for VC
 *
 */
$italic = $desc_size = $el_class = $sort_1 = $sort_2 = $sort_3 = $sort_4 = $sort_5 =
$title = $title_color = $subtitle = $subtitle_color = $description = $description_color =
$line = $line_color = $b_title = $link = $txt_color = $btn_bg = $border =
$btn_space = $desc_space = $blq = $blq_space = $blq_color = $sbtitle_space = $custom_icon =
$sub_size = $title_size = $icon_color = $title_space = $sp_space = $type = $sub_bold = '';

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

?>

<?php if ($type == 'type_1') { ?>
<div class="<?php echo esc_attr( $css_class ). ' '. esc_attr( $el_class ). ' ' . esc_attr( $alignment ). ' '; ?> herotext universal <?php if ($italic == 'yes') echo 'italic-style'; ?>">
    <?php } ?>

    <?php if ($type == 'type_2') { ?>
    <div class="<?php echo esc_attr( $css_class ). ' '. esc_attr( $el_class ). ' ' . esc_attr( $alignment ). ' '; ?> herotext universal universal-2">
        <div class="icon"><i class="<?php print !empty($custom_icon) ? esc_html($custom_icon) : esc_html($type_icon); ?>"
                             style="<?php if (!empty($icon_color)) echo 'color: ' . esc_html($icon_color) . ';'; ?>"></i>
        </div>
        <?php } ?>

        <?php if ( !($sort_1 == 'o-none')) {
            if ($sort_1 == 'o-subtitle' && !empty($subtitle)) { ?>
            <h2 style="<?php if (!empty($sub_bold)) echo 'font-weight: 900;'; ?><?php if (!empty($sub_size)) echo 'font-size: ' . esc_html($sub_size) . 'px;'; ?><?php if (!empty($subtitle_color)) echo 'color: ' . esc_html($subtitle_color) . ';'; ?><?php if (!empty($sbtitle_space)) echo 'padding-bottom: ' . esc_html($sbtitle_space) . 'px;'; ?>">
                <?php print esc_html($subtitle); ?></h2>
        <?php } ?>
        <?php if ($sort_1 == 'o-title' && !empty($title)) { ?>
            <h1 class="heading" style="<?php if (!empty($title_size)) echo 'font-size: ' . esc_html($title_size) . 'px;'; ?><?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?><?php if (!empty($title_space)) echo 'padding-bottom: ' . esc_html($title_space) . 'px;'; ?>"><?php print esc_html($title); ?></h1>
        <?php } ?>
        <?php if ($sort_1 == 'o-separator' ) { ?>
            <div class="line"
                 style="<?php if (!empty($line_color)) echo 'color: ' . esc_html($line_color) . ';'; ?><?php if (!empty($sp_space)) echo 'margin-bottom: ' . esc_html($sp_space) . 'px;'; ?>"></div>
        <?php } ?>
        <?php if ($sort_1 == 'o-description' && !empty($content)) { ?>
            <div class="text "
                 style="<?php if (!empty($desc_size)) echo 'font-size: ' . esc_html($desc_size) . 'px;'; ?><?php if (!empty($description_color)) echo 'color: ' . esc_html($description_color) . ';'; ?><?php if (!empty($desc_space)) echo 'padding-bottom: ' . esc_html($desc_space) . 'px;'; ?>"><?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?></div>
        <?php } ?>

        <?php if ($sort_1 == 'o-blockquote' && !empty($blq)) { ?>
            <div class="blq  description"
                 style="<?php if (!empty($teaser_size)) echo 'font-size: ' . esc_html($teaser_size) . 'px;'; ?><?php if (!empty($blq_color)) echo 'color: ' . esc_html($blq_color) . ';'; ?><?php if (!empty($blq_space)) echo 'padding-bottom: ' . esc_html($blq_space) . 'px;'; ?>"><?php print esc_html($blq); ?></div>
        <?php } ?>

        <?php if ($sort_1 == 'o-button' && !empty($b_title)) { ?>
            <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
            <?php if (!empty($txt_color)) echo 'color: ' . esc_html($txt_color) . ';'; ?>
            <?php if (!empty($btn_bg)) echo 'background: ' . esc_html($btn_bg) . ';'; ?>
            <?php if (!empty($border)) echo 'border-color: ' . esc_html($border) . ';'; ?>
            <?php if (!empty($btn_space)) echo 'padding-bottom: ' . esc_html($btn_space) . 'px;'; ?>
                ">
                <?php print esc_html($b_title); ?>
                <i class="fa fa-chevron-right"></i>
            </a>
        <?php }
        } ?>


        <?php if ( !($sort_2 == 'o-none')) {
            if ($sort_2 == 'o-subtitle' && !empty($subtitle)) { ?>
            <h2 style="<?php if (!empty($sub_bold)) echo 'font-weight: 900;'; ?><?php if (!empty($sub_size)) echo 'font-size: ' . esc_html($sub_size) . 'px;'; ?><?php if (!empty($subtitle_color)) echo 'color: ' . esc_html($subtitle_color) . ';'; ?><?php if (!empty($sbtitle_space)) echo 'padding-bottom: ' . esc_html($sbtitle_space) . 'px;'; ?>">
                <?php print esc_html($subtitle); ?></h2>
        <?php } ?>
        <?php if ($sort_2 == 'o-title' && !empty($title)) { ?>
            <h1 class="heading" style="<?php if (!empty($title_size)) echo 'font-size: ' . esc_html($title_size) . 'px;'; ?><?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?><?php if (!empty($title_space)) echo 'padding-bottom: ' . esc_html($title_space) . 'px;'; ?>"><?php print esc_html($title); ?></h1>
        <?php } ?>
        <?php if ($sort_2 == 'o-separator') { ?>
            <div class="line"
                 style="<?php if (!empty($line_color)) echo 'color: ' . esc_html($line_color) . ';'; ?><?php if (!empty($sp_space)) echo 'margin-bottom: ' . esc_html($sp_space) . 'px;'; ?>"></div>
        <?php } ?>
        <?php if ($sort_2 == 'o-description' && !empty($content)) { ?>
            <div class="text "
                 style="<?php if (!empty($desc_size)) echo 'font-size: ' . esc_html($desc_size) . 'px;'; ?><?php if (!empty($description_color)) echo 'color: ' . esc_html($description_color) . ';'; ?><?php if (!empty($desc_space)) echo 'padding-bottom: ' . esc_html($desc_space) . 'px;'; ?>"><?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?></div>
        <?php } ?>

        <?php if ($sort_2 == 'o-blockquote' && !empty($blq)) { ?>
            <div class="blq  description"
                 style="<?php if (!empty($teaser_size)) echo 'font-size: ' . esc_html($teaser_size) . 'px;'; ?><?php if (!empty($blq_color)) echo 'color: ' . esc_html($blq_color) . ';'; ?><?php if (!empty($blq_space)) echo 'padding-bottom: ' . esc_html($blq_space) . 'px;'; ?>"><?php print esc_html($blq); ?></div>
        <?php } ?>

        <?php if ($sort_2 == 'o-button' && !empty($b_title)) { ?>
            <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
            <?php if (!empty($txt_color)) echo 'color: ' . esc_html($txt_color) . ';'; ?>
            <?php if (!empty($btn_bg)) echo 'background: ' . esc_html($btn_bg) . ';'; ?>
            <?php if (!empty($border)) echo 'border-color: ' . esc_html($border) . ';'; ?>
            <?php if (!empty($btn_space)) echo 'padding-bottom: ' . esc_html($btn_space) . 'px;'; ?>
                ">
                <?php print esc_html($b_title); ?>
                <i class="fa fa-chevron-right"></i>
            </a>
        <?php }
        } ?>


        <?php if ( !($sort_3 == 'o-none')) {
            if ($sort_3 == 'o-subtitle' && !empty($subtitle)) { ?>
            <h2 style="<?php if (!empty($sub_bold)) echo 'font-weight: 900;'; ?><?php if (!empty($sub_size)) echo 'font-size: ' . esc_html($sub_size) . 'px;'; ?><?php if (!empty($subtitle_color)) echo 'color: ' . esc_html($subtitle_color) . ';'; ?><?php if (!empty($sbtitle_space)) echo 'padding-bottom: ' . esc_html($sbtitle_space) . 'px;'; ?>">
                <?php print esc_html($subtitle); ?></h2>
        <?php } ?>
        <?php if ($sort_3 == 'o-title' && !empty($title)) { ?>
            <h1 class="heading" style="<?php if (!empty($title_size)) echo 'font-size: ' . esc_html($title_size) . 'px;'; ?><?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?><?php if (!empty($title_space)) echo 'padding-bottom: ' . esc_html($title_space) . 'px;'; ?>"><?php print esc_html($title); ?></h1>
        <?php } ?>
        <?php if ($sort_3 == 'o-separator' ) { ?>
            <div class="line"
                 style="<?php if (!empty($line_color)) echo 'color: ' . esc_html($line_color) . ';'; ?><?php if (!empty($sp_space)) echo 'margin-bottom: ' . esc_html($sp_space) . 'px;'; ?>"></div>
        <?php } ?>
        <?php if ($sort_3 == 'o-description' && !empty($content)) { ?>
            <div class="text "
                 style="<?php if (!empty($desc_size)) echo 'font-size: ' . esc_html($desc_size) . 'px;'; ?><?php if (!empty($description_color)) echo 'color: ' . esc_html($description_color) . ';'; ?><?php if (!empty($desc_space)) echo 'padding-bottom: ' . esc_html($desc_space) . 'px;'; ?>"><?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?></div>
        <?php } ?>

        <?php if ($sort_3 == 'o-blockquote' && !empty($blq)) { ?>
            <div class="blq  description"
                 style="<?php if (!empty($teaser_size)) echo 'font-size: ' . esc_html($teaser_size) . 'px;'; ?><?php if (!empty($blq_color)) echo 'color: ' . esc_html($blq_color) . ';'; ?><?php if (!empty($blq_space)) echo 'padding-bottom: ' . esc_html($blq_space) . 'px;'; ?>"><?php print esc_html($blq); ?></div>
        <?php } ?>

        <?php if ($sort_3 == 'o-button' && !empty($b_title)) { ?>
            <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
            <?php if (!empty($txt_color)) echo 'color: ' . esc_html($txt_color) . ';'; ?>
            <?php if (!empty($btn_bg)) echo 'background: ' . esc_html($btn_bg) . ';'; ?>
            <?php if (!empty($border)) echo 'border-color: ' . esc_html($border) . ';'; ?>
            <?php if (!empty($btn_space)) echo 'padding-bottom: ' . esc_html($btn_space) . 'px;'; ?>
                ">
                <?php print esc_html($b_title); ?>
                <i class="fa fa-chevron-right"></i>
            </a>
        <?php }
        } ?>


        <?php if ( !($sort_4 == 'o-none')) {
            if ($sort_4 == 'o-subtitle' && !empty($subtitle)) { ?>
            <h2 style="<?php if (!empty($sub_bold)) echo 'font-weight: 900;'; ?><?php if (!empty($sub_size)) echo 'font-size: ' . esc_html($sub_size) . 'px;'; ?><?php if (!empty($subtitle_color)) echo 'color: ' . esc_html($subtitle_color) . ';'; ?><?php if (!empty($sbtitle_space)) echo 'padding-bottom: ' . esc_html($sbtitle_space) . 'px;'; ?>">
                <?php print esc_html($subtitle); ?></h2>
        <?php } ?>
        <?php if ($sort_4 == 'o-title' && !empty($title)) { ?>
            <h1 class="heading" style="<?php if (!empty($title_size)) echo 'font-size: ' . esc_html($title_size) . 'px;'; ?><?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?><?php if (!empty($title_space)) echo 'padding-bottom: ' . esc_html($title_space) . 'px;'; ?>"><?php print esc_html($title); ?></h1>
        <?php } ?>
        <?php if ($sort_4 == 'o-separator' ) { ?>
            <div class="line"
                 style="<?php if (!empty($line_color)) echo 'color: ' . esc_html($line_color) . ';'; ?><?php if (!empty($sp_space)) echo 'margin-bottom: ' . esc_html($sp_space) . 'px;'; ?>"></div>
        <?php } ?>
        <?php if ($sort_4 == 'o-description' && !empty($content)) { ?>
            <div class="text "
                 style="<?php if (!empty($desc_size)) echo 'font-size: ' . esc_html($desc_size) . 'px;'; ?><?php if (!empty($description_color)) echo 'color: ' . esc_html($description_color) . ';'; ?><?php if (!empty($desc_space)) echo 'padding-bottom: ' . esc_html($desc_space) . 'px;'; ?>"><?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?></div>
        <?php } ?>

        <?php if ($sort_4 == 'o-blockquote' && !empty($blq)) { ?>
            <div class="blq  description"
                 style="<?php if (!empty($teaser_size)) echo 'font-size: ' . esc_html($teaser_size) . 'px;'; ?><?php if (!empty($blq_color)) echo 'color: ' . esc_html($blq_color) . ';'; ?><?php if (!empty($blq_space)) echo 'padding-bottom: ' . esc_html($blq_space) . 'px;'; ?>"><?php print esc_html($blq); ?></div>
        <?php } ?>

        <?php if ($sort_4 == 'o-button' && !empty($b_title)) { ?>
            <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
            <?php if (!empty($txt_color)) echo 'color: ' . esc_html($txt_color) . ';'; ?>
            <?php if (!empty($btn_bg)) echo 'background: ' . esc_html($btn_bg) . ';'; ?>
            <?php if (!empty($border)) echo 'border-color: ' . esc_html($border) . ';'; ?>
            <?php if (!empty($btn_space)) echo 'padding-bottom: ' . esc_html($btn_space) . 'px;'; ?>
                ">
                <?php print esc_html($b_title); ?>
                <i class="fa fa-chevron-right"></i>
            </a>
        <?php }
        } ?>


        <?php if ( !($sort_5 == 'o-none')) {
            if ($sort_5 == 'o-subtitle' && !empty($subtitle)) { ?>
            <h2 style="<?php if (!empty($sub_bold)) echo 'font-weight: 900;'; ?><?php if (!empty($sub_size)) echo 'font-size: ' . esc_html($sub_size) . 'px;'; ?><?php if (!empty($subtitle_color)) echo 'color: ' . esc_html($subtitle_color) . ';'; ?><?php if (!empty($sbtitle_space)) echo 'padding-bottom: ' . esc_html($sbtitle_space) . 'px;'; ?>">
                <?php print esc_html($subtitle); ?></h2>
        <?php } ?>
        <?php if ($sort_5 == 'o-title' && !empty($title)) { ?>
            <h1 class="heading" style="<?php if (!empty($title_size)) echo 'font-size: ' . esc_html($title_size) . 'px;'; ?><?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?><?php if (!empty($title_space)) echo 'padding-bottom: ' . esc_html($title_space) . 'px;'; ?>"><?php print esc_html($title); ?></h1>
        <?php } ?>
        <?php if ($sort_5 == 'o-separator' ) { ?>
            <div class="line"
                 style="<?php if (!empty($line_color)) echo 'color: ' . esc_html($line_color) . ';'; ?><?php if (!empty($sp_space)) echo 'margin-bottom: ' . esc_html($sp_space) . 'px;'; ?>"></div>
        <?php } ?>
        <?php if ($sort_5 == 'o-description' && !empty($content)) { ?>
            <div class="text "
                 style="<?php if (!empty($desc_size)) echo 'font-size: ' . esc_html($desc_size) . 'px;'; ?><?php if (!empty($description_color)) echo 'color: ' . esc_html($description_color) . ';'; ?><?php if (!empty($desc_space)) echo 'padding-bottom: ' . esc_html($desc_space) . 'px;'; ?>"><?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?></div>
        <?php } ?>
        <?php if ($sort_5 == 'o-blockquote' && !empty($blq)) { ?>
            <div class="blq  description"
                 style="<?php if (!empty($teaser_size)) echo 'font-size: ' . esc_html($teaser_size) . 'px;'; ?><?php if (!empty($blq_color)) echo 'color: ' . esc_html($blq_color) . ';'; ?><?php if (!empty($blq_space)) echo 'padding-bottom: ' . esc_html($blq_space) . 'px;'; ?>"><?php print esc_html($blq); ?></div>
        <?php } ?>

        <?php if ($sort_5 == 'o-button' && !empty($b_title)) { ?>
            <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
            <?php if (!empty($txt_color)) echo 'color: ' . esc_html($txt_color) . ';'; ?>
            <?php if (!empty($btn_bg)) echo 'background: ' . esc_html($btn_bg) . ';'; ?>
            <?php if (!empty($border)) echo 'border-color: ' . esc_html($border) . ';'; ?>
            <?php if (!empty($btn_space)) echo 'padding-bottom: ' . esc_html($btn_space) . 'px;'; ?>
                ">
                <?php print esc_html($b_title); ?>
                <i class="fa fa-chevron-right"></i>
            </a>
        <?php }
        } ?>


        <?php if ( !($sort_6 == 'o-none')) {
            if ($sort_6 == 'o-subtitle' && !empty($subtitle)) { ?>
            <h2 style="<?php if (!empty($sub_bold)) echo 'font-weight: 900;'; ?><?php if (!empty($sub_size)) echo 'font-size: ' . esc_html($sub_size) . 'px;'; ?><?php if (!empty($subtitle_color)) echo 'color: ' . esc_html($subtitle_color) . ';'; ?><?php if (!empty($sbtitle_space)) echo 'padding-bottom: ' . esc_html($sbtitle_space) . 'px;'; ?>">
                <?php print esc_html($subtitle); ?></h2>
        <?php } ?>
        <?php if ($sort_6 == 'o-title' && !empty($title)) { ?>
            <h1 class="heading" style="<?php if (!empty($title_size)) echo 'font-size: ' . esc_html($title_size) . 'px;'; ?><?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?><?php if (!empty($title_space)) echo 'padding-bottom: ' . esc_html($title_space) . 'px;'; ?>"><?php print esc_html($title); ?></h1>
        <?php } ?>
        <?php if ($sort_6 == 'o-separator' ) { ?>
            <div class="line"
                 style="<?php if (!empty($line_color)) echo 'color: ' . esc_html($line_color) . ';'; ?><?php if (!empty($sp_space)) echo 'margin-bottom: ' . esc_html($sp_space) . 'px;'; ?>"></div>
        <?php } ?>
        <?php if ($sort_6 == 'o-description' && !empty($content)) { ?>
            <div class="text "
                 style="<?php if (!empty($desc_size)) echo 'font-size: ' . esc_html($desc_size) . 'px;'; ?><?php if (!empty($description_color)) echo 'color: ' . esc_html($description_color) . ';'; ?><?php if (!empty($desc_space)) echo 'padding-bottom: ' . esc_html($desc_space) . 'px;'; ?>"><?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?></div>
        <?php } ?>
        <?php if ($sort_6 == 'o-blockquote' && !empty($blq)) { ?>
            <div class="blq  description"
                 style="<?php if (!empty($teaser_size)) echo 'font-size: ' . esc_html($teaser_size) . 'px;'; ?><?php if (!empty($blq_color)) echo 'color: ' . esc_html($blq_color) . ';'; ?><?php if (!empty($blq_space)) echo 'padding-bottom: ' . esc_html($blq_space) . 'px;'; ?>"><?php print esc_html($blq); ?></div>
        <?php } ?>

        <?php if ($sort_6 == 'o-button' && !empty($b_title)) { ?>
            <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
            <?php if (!empty($txt_color)) echo 'color: ' . esc_html($txt_color) . ';'; ?>
            <?php if (!empty($btn_bg)) echo 'background: ' . esc_html($btn_bg) . ';'; ?>
            <?php if (!empty($border)) echo 'border-color: ' . esc_html($border) . ';'; ?>
            <?php if (!empty($btn_space)) echo 'padding-bottom: ' . esc_html($btn_space) . 'px;'; ?>
                ">
                <?php print esc_html($b_title); ?>
                <i class="fa fa-chevron-right"></i>
            </a>
        <?php }
        } ?>

    </div>
