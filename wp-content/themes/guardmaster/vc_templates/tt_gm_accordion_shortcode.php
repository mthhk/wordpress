<?php
$acc_type = 'one_open';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );


//$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
?>

    <div class="col-lg-12 accordion-02">
        <div class="panel-group" id="accordion">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>


