<?php
/*
 * Templatation.com
 *
 * Hero block box shortcode for VC
 *
 */
$alignment  = $heading = $content = $image = $color = $text_appear = $yoast_bdcmp = $block_padding_top = $block_padding_bottom = '';
$text_appear = 'light';
$yoast_bdcmp = false;

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$imgg = array(); if( !empty($image) && is_numeric($image) ) $imgg = wp_get_attachment_image_src( $image, 'full' ); else $imgg[0] = $image;
$align = ($alignment == 'left') ? 'textleft' : ($alignment == 'right' ? 'textright' : '' );

$block_padding_top = !empty($block_padding_top) ? 'padding-top:'.$block_padding_top.'px;' : '';
$block_padding_bottom = !empty($block_padding_bottom) ? 'padding-bottom:'.$block_padding_bottom.'px;' : '';
$block_padding = $block_padding_top.$block_padding_bottom;

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
?>

<section class="breadcrumb-area <?php echo esc_attr($text_appear). ' '.esc_attr($el_class).' '.esc_attr($css_class); ?>" style="
	<?php if(isset($imgg[0]) ) echo 'background-image: url('. $imgg[0]. ');'; ?>
	 <?php echo esc_attr($block_padding); ?>
	">
    <div class="overlay-clr" style="<?php echo 'background-color: '. $color.';'; ?>"></div>
	<div class="breadcrumb-text-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-text text-center">
					<?php if(!empty($heading) ) { ?>
					<h1 class="tt-title"><?php echo htmlspecialchars_decode($heading); ?></h1>
						<?php } ?>
						<?php if(!empty($content) ) { ?>
					<p class="description">
						<?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?>
					</p>
						<?php }
					// Breadcrumb
					 if ( $yoast_bdcmp && function_exists('yoast_breadcrumb') ) {
						 yoast_breadcrumb('<div class="yt-breadcrumbs">','</div>');
					 }
					?>
                    <div class="line displayinblock"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>