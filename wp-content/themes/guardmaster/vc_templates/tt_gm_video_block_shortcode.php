<?php
/*
 * Templatation.com
 *
 * Block with image and effect shortcode for VC
 *
 */
$title = $subtitle = $link = '';

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$img = (is_numeric($image) && !empty($image)) ? wp_get_attachment_url($image) : '';
?>

<div class="block video tt-video-sc">
    <div class="picture">
        <img src="<?php print esc_url($img); ?>" class="img-responsive" alt="">
        <div class="info-overlay">
            <div class="icon"><a class="popup-vimeo" href="<?php print esc_html($link); ?>"><i class="fa fa-play"></i></a></div>
            <div class="heading"><?php print esc_html($title); ?></div>
            <div class="description"><?php print esc_html($subtitle); ?></div>
        </div>
    </div>
</div>

