<?php
/*
 * Templatation.com
 *
 * Block with image and effect shortcode for VC
 *
 */
$image = '';

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);


$img = (is_numeric($image) && !empty($image)) ? wp_get_attachment_url($image) : '';
?>
<!-- end of code from VC -->

<div class="fullimage">
   <img  src="<?php print esc_url($img); ?>" class="img-responsive" alt="">
</div>

