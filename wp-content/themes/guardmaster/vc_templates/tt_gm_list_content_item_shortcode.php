<?php
$title = $info = $itemcolor =   '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>

<li style="<?php if( !empty($itemcolor)) echo 'color: '. esc_html($itemcolor).';'; ?>">
    <div class="icon"><i class="fa fa-circle"></i></div>
    <div class="info">
        <h2> <?php print esc_html($title); ?></h2>
        <div class="description"> <?php print esc_html($info); ?></div>
    </div>
</li>

