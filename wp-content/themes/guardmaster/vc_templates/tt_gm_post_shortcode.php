<?php
$num_posts = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
is_numeric($num_posts) ? $num_posts = '' : $num_posts = '4';
?>

<div class="latest-news">
<div class="news-carousel">
<?php
$args = array( 'posts_per_page' => $num_posts );
$query = new WP_Query( $args );
while ( $query->have_posts() ) {
    $query->the_post() ?>

        <div class="news">
            <div class="picture"> <?php the_post_thumbnail(array(275, 230)); ?></div>
            <div class="block">
                <div class="date"><span class="gm-comma">-:</span> <?php the_time('M, Y'); ?>   /  <?php the_author(); ?></div>
                <h1><?php the_title(); ?></h1>
                <div class="description"><?php tt_gmaster_excerpt_charlength('120'); ?></div>
                <div class="button"><a href="<?php the_permalink(); ?>" class="btn-orange"><?php esc_html_e('Read More', 'guardmaster'); ?> <i class="fa fa-chevron-right"></i></a></div>
            </div>
        </div>


    <?php
}
wp_reset_postdata();
?>
</div>
</div>

