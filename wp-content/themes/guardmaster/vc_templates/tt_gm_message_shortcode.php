<?php
/*
 * Templatation.com
 *
 * Block with image and effect shortcode for VC
 *
 */
$ms_type = $title = $titlecolor = $info = $infocolor = $bgcolor = $type_icon = $icon_color = '';

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

?>

<?php if($ms_type == 'style_1') { ?>
    <div class="alert-message-02">
        <div class="alert  alert-dismissible" role="alert" class="btn-orange" style="<?php if( !empty($bgcolor)) echo 'background: '. esc_html($bgcolor).';'; ?>">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <i class="<?php print esc_html($type_icon); ?>" style="<?php if( !empty($icon_color)) echo 'color: '. esc_html($icon_color).';'; ?>"></i>
            <div class="info" style="<?php if( !empty($infocolor)) echo 'color: '. esc_html($infocolor).';'; ?>">
                <strong style="<?php if( !empty($titlecolor)) echo 'color: '. esc_html($titlecolor).';'; ?>"> <?php print esc_html($title); ?></strong><br> <?php print esc_html($info); ?>
            </div>
        </div>
    </div>
<?php } ?>

<?php if($ms_type == 'style_2') { ?>
    <div class="alert-message-01">
        <div class="alert  alert-dismissible" role="alert" class="btn-orange" style="<?php if( !empty($bgcolor)) echo 'background: '. esc_html($bgcolor).';'; ?>">
            <i class="<?php print esc_html($type_icon); ?>" style="<?php if( !empty($icon_color)) echo 'color: '. esc_html($icon_color).';'; ?>"></i>
            <span style="<?php if( !empty($titlecolor)) echo 'color: '. esc_html($titlecolor).';'; ?>"><?php print esc_html($title); ?></span>
        </div>
    </div>
<?php } ?>
