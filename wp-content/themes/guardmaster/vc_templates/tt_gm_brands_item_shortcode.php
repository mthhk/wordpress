<?php
$title = $icon = $itemcolor = $iconcolor = $type_icon =   '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$img = (is_numeric($image) && !empty($image)) ? wp_get_attachment_url($image) : '';

?>

<div class="block"><img src="<?php print esc_url($img); ?>" class="img-responsive center-block" alt=""></div>