<?php

$testimonials_type = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>

<div class="testimonials-carousel testimonials <?php if( $testimonials_type == 'style_1') echo 'left-tm'; ?>
<?php if( $testimonials_type == 'style_2') echo 'right-tm'; ?><?php if( $testimonials_type == 'style_3') echo 'center-tm'; ?>">
    <?php print do_shortcode($content); ?>
</div>