<?php
$title = $css = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );


//$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
?>

<div class="panel panel-default">
	<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse-1" aria-expanded="true">
		<h4 class="panel-title ">
			<a class="accordion-toggle">
				<?php print esc_html($title); ?>
			</a><i class="indicator2 fa pull-right fa-plus"></i>
		</h4>
	</div>
	<div id="collapse-1" class="panel-collapse collapse " aria-expanded="true">
		<div class="panel-body">
			<?php print do_shortcode($content); ?>
		</div>
	</div>
</div>