<?php

$title = $year = $titlecolor = $info = $infocolor =   '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$group_val = json_decode(urldecode($atts['values']));

?>

<div class="history-timeline-tabs">
    <ul class="nav nav-justified icon-tabs" role="tablist">
        <?php foreach ($group_val as $val) {
            if(isset($val->year)) { ?>
                <li role="presentation">
                    <a href="#<?php print esc_html($val->year); ?>" aria-controls="<?php print esc_html($val->year); ?>" role="tab" data-toggle="tab">
                        <div class="caption"><?php print esc_html($val->year); ?></div>
                        <div class="icon"><i class="fa fa-caret-down"></i></div>
                    </a>
                </li>
            <?php  } ?>
        <?php } ?>
    </ul>
    <div class="tab-content">
        <?php foreach ($group_val as $val) { ?>
            <div role="tabpanel" class="tab-pane fade in" id="<?php print esc_html($val->year); ?>">
                <h1 style="<?php if( !empty($val->titlecolor)) echo 'color: '. esc_html($val->titlecolor).';'; ?>"><?php print esc_html($val->title); ?></h1>
                <div class="description" style="<?php if( !empty($infocolor)) echo 'color: '. esc_html($val->infocolor).';'; ?>">
                    <?php print esc_html($val->info); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>