<?php

$list_content_type = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>
<?php if($list_content_type == 'style_1') { ?>
    <div class="services-benefits">
        <ul class="left">
            <?php print do_shortcode($content); ?>
        </ul>
    </div>
<?php } ?>

<?php if($list_content_type == 'style_2') { ?>
    <div class="services-benefits">
        <ul class="right">
            <?php print do_shortcode($content); ?>
        </ul>
    </div>
<?php } ?>
