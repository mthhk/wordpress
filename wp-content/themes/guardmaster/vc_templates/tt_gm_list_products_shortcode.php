<?php
/*
 * Templatation.com
 *
 * List Products shortcode for VC
 *
 */
$tab_class = $arrow_bottom = $carousel_autoplay_hover = $carousel_autoplay = $carousel_columns = $carousel_elements = $arrow_slide_speed = $circle_slide_speed = $product_show = $enable_carousel = $data_best_selling_products  = $data_featured_products  = $data_recent_products  = $data_sale_products  = $hide_title = $data_top_rated_products = '' ;

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );


		// Saving needed data in a variable.
		if( ! (strpos($product_show, 'best_selling_products') === false) )
			$data_best_selling_products = do_shortcode('[best_selling_products per_page="'.$per_page.'" columns="'.$columns.'"]');

		if( ! (strpos($product_show, 'featured_products') === false) )
			$data_featured_products = do_shortcode('[featured_products per_page="'.$per_page.'" columns="'.$columns.'"]');

		if( ! (strpos($product_show, 'recent_products') === false) )
			$data_recent_products = do_shortcode('[recent_products per_page="'.$per_page.'" columns="'.$columns.'"]');

		if( ! (strpos($product_show, 'sale_products') === false) )
			$data_sale_products = do_shortcode('[sale_products per_page="'.$per_page.'" columns="'.$columns.'"]');

		if( ! (strpos($product_show, 'top_rated_products') === false) )
			$data_top_rated_products = do_shortcode('[top_rated_products per_page="'.$per_page.'" columns="'.$columns.'"]');

		if( !$hide_title) $tab_class = 'tt-tab'; // this triggers tab control jquery script

		if( $data_best_selling_products == '<div class="woocommerce columns-4"></div>' )
			$data_best_selling_products = '<div class="woocommerce columns-4 mbottom120">'. esc_attr__('No products to show!', 'guardmaster') .'</div>';

		if( $data_featured_products == '<div class="woocommerce columns-4"></div>' )
			$data_featured_products = '<div class="woocommerce columns-4 mbottom120">'. esc_attr__('No products to show!', 'guardmaster') .'</div>';

		if( $data_recent_products == '<div class="woocommerce columns-4"></div>' )
			$data_recent_products = '<div class="woocommerce columns-4 mbottom120">'. esc_attr__('No products to show!', 'guardmaster') .'</div>';

		if( $data_sale_products == '<div class="woocommerce columns-4"></div>' )
			$data_sale_products = '<div class="woocommerce columns-4 mbottom120">'. esc_attr__('No products to show!', 'guardmaster') .'</div>';

		if( $data_top_rated_products == '<div class="woocommerce columns-4"></div>' )
			$data_top_rated_products = '<div class="woocommerce columns-4 mbottom120">'. esc_attr__('No products to show!', 'guardmaster') .'</div>';
?>

	<?php if( $enable_carousel ) { ?>
		<div class="tt-carousel <?php if($arrow_bottom) echo 'arw-btm';?>" data-stop-on-hover="<?php echo esc_attr($carousel_autoplay_hover); ?>" data-autoplay="<?php echo esc_attr($carousel_autoplay); ?>" data-columns="<?php echo esc_attr($carousel_columns); ?>" data-pagination="<?php if( ! (strpos($carousel_elements, 'circles') === false) ) echo 'true'; else echo 'false'; ?>" data-slide-speed="<?php echo esc_attr($arrow_slide_speed); ?>" data-pagination-speed="<?php echo esc_attr($circle_slide_speed); ?>">
	<?php } ?>

	<div class="tt-listprod-sc <?php if(!$hide_title) echo 'tt-prodtabs-wrapper';?>">

		<?php if( !empty ($data_best_selling_products)) { ?>
			<div id="tt-tab-bestselling" class="<?php echo esc_attr($tab_class); ?>">
				<?php echo $data_best_selling_products; ?>
				<?php if( $enable_carousel && ! (strpos($carousel_elements, 'arrows') === false) ) { ?>
					<div class="tt-carousel-nav <?php if($arrow_bottom) echo 'bottom';?> ">
						<a href="#" class="tt-carousel-nav-prev"><?php esc_attr__('Previous', 'guardmaster'); ?></a>
						<a href="#" class="tt-carousel-nav-next"><?php esc_attr__('Next', 'guardmaster'); ?></a>
		            </div><!-- .tt-carousel-nav -->
				<?php } ?>
			</div>
		<?php } if( !empty ($data_featured_products)) { ?>
			<div id="tt-tab-featured" class="<?php echo esc_attr($tab_class); ?>">
				<?php echo $data_featured_products; ?>
				<?php if( $enable_carousel && ! (strpos($carousel_elements, 'arrows') === false) ) { ?>
					<div class="tt-carousel-nav <?php if($arrow_bottom) echo 'bottom';?> ">
						<a href="#" class="tt-carousel-nav-prev"><?php esc_attr__('Previous', 'guardmaster'); ?></a>
						<a href="#" class="tt-carousel-nav-next"><?php esc_attr__('Next', 'guardmaster'); ?></a>
		            </div><!-- .tt-carousel-nav -->
				<?php } ?>
			</div>
		<?php } if( !empty ($data_recent_products)) { ?>
			<div id="tt-tab-latest" class="<?php echo esc_attr($tab_class); ?>">
				<?php echo $data_recent_products; ?>
				<?php if( $enable_carousel && ! (strpos($carousel_elements, 'arrows') === false) ) { ?>
					<div class="tt-carousel-nav <?php if($arrow_bottom) echo 'bottom';?> ">
						<a href="#" class="tt-carousel-nav-prev"><?php esc_attr__('Previous', 'guardmaster'); ?></a>
						<a href="#" class="tt-carousel-nav-next"><?php esc_attr__('Next', 'guardmaster'); ?></a>
		            </div><!-- .tt-carousel-nav -->
				<?php } ?>
			</div>
		<?php } if( !empty ($data_sale_products)) { ?>
			<div id="tt-tab-sale" class="<?php echo esc_attr($tab_class); ?>">
				<?php echo $data_sale_products; ?>
				<?php if( $enable_carousel && ! (strpos($carousel_elements, 'arrows') === false) ) { ?>
					<div class="tt-carousel-nav <?php if($arrow_bottom) echo 'bottom';?> ">
						<a href="#" class="tt-carousel-nav-prev"><?php esc_attr__('Previous', 'guardmaster'); ?></a>
						<a href="#" class="tt-carousel-nav-next"><?php esc_attr__('Next', 'guardmaster'); ?></a>
		            </div><!-- .tt-carousel-nav -->
				<?php } ?>
			</div>
		<?php } if( !empty ($data_top_rated_products)) { ?>
			<div id="tt-tab-toprated" class="<?php echo esc_attr($tab_class); ?>">
				<?php echo $data_top_rated_products; ?>
				<?php if( $enable_carousel && ! (strpos($carousel_elements, 'arrows') === false) ) { ?>
					<div class="tt-carousel-nav <?php if($arrow_bottom) echo 'bottom';?> ">
						<a href="#" class="tt-carousel-nav-prev"><?php esc_attr__('Previous', 'guardmaster'); ?></a>
						<a href="#" class="tt-carousel-nav-next"><?php esc_attr__('Next', 'guardmaster'); ?></a>
		            </div><!-- .tt-carousel-nav -->
				<?php } ?>
			</div>
		<?php } ?>

	</div><!-- .tt-prodtabs-wrapper -->

	<?php if( $enable_carousel ) {
		echo '</div><!-- .tt-carousel -->';
	} ?>

	<?php wp_enqueue_script( 'temptt-carousel' ); ?>
