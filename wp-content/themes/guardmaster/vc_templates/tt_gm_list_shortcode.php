<?php

$list_type = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>
<?php if($list_type == 'style_1') { ?>
    <ul class="gm-list">
        <?php print do_shortcode($content); ?>
    </ul>
<?php } ?>

<?php if($list_type == 'style_2') { ?>
    <ol class="gm-list no-icon">
        <?php print do_shortcode($content); ?>
    </ol>
<?php } ?>

