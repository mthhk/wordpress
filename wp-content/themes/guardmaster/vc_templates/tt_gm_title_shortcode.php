<?php
/*
 * Templatation.com
 *
 * Title with separator shortcode for VC
 *
 */
$title_type = $el_class = $pos_type = $title = $title_color = $titles = $titles_color = $sbtitle =
$sbtitle_color =  $sptitle =  $sptitle_color = $separator  = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

?>

<?php if ($title_type == 'type_1') { ?>
<div class="<?php echo esc_attr( $css_class ). ' '. esc_attr( $el_class ). ' '?> title-block_1 <?php if( $pos_type == 'pos_1') echo 'center-t'; ?>
<?php if( $pos_type == 'pos_2') echo 'left-t'; ?><?php if( $pos_type == 'pos_3') echo 'rigth-t'; ?>">
    <h3 style="<?php if (!empty($sptitle_color)) echo 'color: ' . esc_html($sptitle_color) . ';'; ?>"><?php print esc_html($sptitle); ?> </h3>
    <h2 style="<?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?>"><?php print esc_html($title); ?></h2>
    <div class="line"></div>
</div>
<?php } ?>
<?php if ($title_type == 'type_2') { ?>
<div class="<?php echo esc_attr( $css_class ). ' '. esc_attr( $el_class ). ' '?> title-block_2 <?php if( $pos_type == 'pos_1') echo 'center-t'; ?>
<?php if( $pos_type == 'pos_2') echo 'left-t'; ?><?php if( $pos_type == 'pos_3') echo 'rigth-t'; ?>">
    <div class="boxed-heading">
        <h3 style="<?php if (!empty($sptitle_color)) echo 'color: ' . esc_html($sptitle_color) . ';'; ?>"><?php print esc_html($sptitle); ?> </h3>
        <h2 style="<?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?>"><?php print esc_html($title); ?></h2>
    </div>
    <div class="description" style="<?php if (!empty($sbtitle_color)) echo 'color: ' . esc_html($sbtitle_color) . ';'; ?>"><?php print esc_html($sbtitle); ?></div>
</div>
<?php } ?>
<?php if ($title_type == 'type_3') { ?>
<div class="<?php echo esc_attr( $css_class ). ' '. esc_attr( $el_class ). ' '?> title-block_3 <?php if( $pos_type == 'pos_1') echo 'center-t'; ?>
<?php if( $pos_type == 'pos_2') echo 'left-t'; ?><?php if( $pos_type == 'pos_3') echo 'rigth-t'; ?>">
    <h3 style="<?php if (!empty($sptitle_color)) echo 'color: ' . esc_html($sptitle_color) . ';'; ?>"><?php print esc_html($sptitle); ?> </h3>
    <h2 style="<?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?>"><?php print esc_html($title); ?></h2>
    <div class="description" style="<?php if (!empty($sbtitle_color)) echo 'color: ' . esc_html($sbtitle_color) . ';'; ?>"><?php print esc_html($sbtitle); ?></div>
</div>
<?php } ?>
<?php if ($title_type == 'type_4') { ?>
<div class="<?php echo esc_attr( $css_class ). ' '. esc_attr( $el_class ). ' '?> title-block_4 <?php if( $pos_type == 'pos_1') echo 'center-t'; ?>
<?php if( $pos_type == 'pos_2') echo 'left-t'; ?><?php if( $pos_type == 'pos_3') echo 'rigth-t'; ?>">
    <h2 style="<?php if (!empty($title_color)) echo 'color: ' . esc_html($title_color) . ';'; ?>"><?php print esc_html($title); ?> <span style="<?php if (!empty($titles_color)) echo 'color: ' . esc_html($titles_color) . ';'; ?>"><?php print esc_html($titles); ?></span></h2>
    <div class="description" style="<?php if (!empty($titles_color)) echo 'color: ' . esc_html($titles_color) . ';'; ?>"><?php print esc_html($sbtitle); ?></div>
</div>
<?php } ?>
