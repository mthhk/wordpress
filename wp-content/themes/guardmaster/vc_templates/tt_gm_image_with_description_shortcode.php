<?php
/*
 * Templatation.com
 *
 * Title with separator shortcode for VC
 *
 */
$sort_1 = $sort_2 = $sort_3 = $sort_4 = $sort_5 =
$title = $title_color = $subtitle = $subtitle_color = $description = $description_color =
$line = $line_color = $b_title = $link = $txt_color = $btn_bg = $border =
$btn_space = $desc_space =  $sbtitle_space =  $title_space = $sp_space = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>
<div class="herotext universal">
    <?php if($sort_1 == 'o-subtitle' && !empty($subtitle)) { ?>
        <h2 style="<?php if( !empty($subtitle_color)) echo 'color: '. esc_html($subtitle_color).';'; ?><?php if( !empty($sbtitle_space)) echo 'padding-bottom: '. esc_html($sbtitle_space).'px;'; ?>">
            <?php print esc_html($subtitle); ?></h2>
    <?php } ?>
    <?php if($sort_1 == 'o-title' && !empty($title)) { ?>
        <h1 style="<?php if( !empty($title_color)) echo 'color: '. esc_html($title_color).';'; ?><?php if( !empty($title_space)) echo 'padding-bottom: '. esc_html($title_space).'px;'; ?>"><?php print esc_html($title); ?></h1>
    <?php } ?>
    <?php if($sort_1 == 'o-separator' && $line == 'yes' ) { ?>
        <div class="line" style="<?php if( !empty($line_color)) echo 'color: '. esc_html($line_color).';'; ?><?php if( !empty($sp_space)) echo 'padding-bottom: '. esc_html($sp_space).'px;'; ?>"></div>
    <?php } ?>
    <?php if($sort_1 == 'o-description' && !empty($description)) { ?>
        <div class="text " style="<?php if( !empty($description_color)) echo 'color: '. esc_html($description_color).';'; ?><?php if( !empty($desc_space)) echo 'padding-bottom: '. esc_html($desc_space).'px;'; ?>"><?php print esc_html($description); ?></div>
    <?php } ?>

    <?php if($sort_1 == 'o-button' && !empty($b_title) ) { ?>
        <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
        <?php if( !empty($txt_color)) echo 'color: '. esc_html($txt_color).';'; ?>
        <?php if( !empty($btn_bg)) echo 'background: '. esc_html($btn_bg).';'; ?>
        <?php if( !empty($border)) echo 'border-color: '. esc_html($border).';'; ?>
        <?php if( !empty($btn_space)) echo 'padding-bottom: '. esc_html($btn_space).'px;'; ?>
            ">
            <?php print esc_html($b_title); ?>
            <i class="fa fa-chevron-right"></i>
        </a>
    <?php } ?>

    <?php if($sort_2 == 'o-subtitle' && !empty($subtitle)) { ?>
        <h2 style="<?php if( !empty($subtitle_color)) echo 'color: '. esc_html($subtitle_color).';'; ?><?php if( !empty($sbtitle_space)) echo 'padding-bottom: '. esc_html($sbtitle_space).'px;'; ?>">
            <?php print esc_html($subtitle); ?></h2>
    <?php } ?>
    <?php if($sort_2 == 'o-title' && !empty($title)) { ?>
        <h1 style="<?php if( !empty($title_color)) echo 'color: '. esc_html($title_color).';'; ?><?php if( !empty($title_space)) echo 'padding-bottom: '. esc_html($title_space).'px;'; ?>"><?php print esc_html($title); ?></h1>
    <?php } ?>
    <?php if($sort_2 == 'o-separator' && $line == 'yes' ) { ?>
        <div class="line" style="<?php if( !empty($line_color)) echo 'color: '. esc_html($line_color).';'; ?><?php if( !empty($sp_space)) echo 'padding-bottom: '. esc_html($sp_space).'px;'; ?>"></div>
    <?php } ?>
    <?php if($sort_2 == 'o-description' && !empty($description)) { ?>
        <div class="text " style="<?php if( !empty($description_color)) echo 'color: '. esc_html($description_color).';'; ?><?php if( !empty($desc_space)) echo 'padding-bottom: '. esc_html($desc_space).'px;'; ?>"><?php print esc_html($description); ?></div>
    <?php } ?>

    <?php if($sort_2 == 'o-button' && !empty($b_title) ) { ?>
        <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
        <?php if( !empty($txt_color)) echo 'color: '. esc_html($txt_color).';'; ?>
        <?php if( !empty($btn_bg)) echo 'background: '. esc_html($btn_bg).';'; ?>
        <?php if( !empty($border)) echo 'border-color: '. esc_html($border).';'; ?>
        <?php if( !empty($btn_space)) echo 'padding-bottom: '. esc_html($btn_space).'px;'; ?>
            ">
            <?php print esc_html($b_title); ?>
            <i class="fa fa-chevron-right"></i>
        </a>
    <?php } ?>

    <?php if($sort_3 == 'o-subtitle' && !empty($subtitle)) { ?>
        <h2 style="<?php if( !empty($subtitle_color)) echo 'color: '. esc_html($subtitle_color).';'; ?><?php if( !empty($sbtitle_space)) echo 'padding-bottom: '. esc_html($sbtitle_space).'px;'; ?>">
            <?php print esc_html($subtitle); ?></h2>
    <?php } ?>
    <?php if($sort_3 == 'o-title' && !empty($title)) { ?>
        <h1 style="<?php if( !empty($title_color)) echo 'color: '. esc_html($title_color).';'; ?><?php if( !empty($title_space)) echo 'padding-bottom: '. esc_html($title_space).'px;'; ?>"><?php print esc_html($title); ?></h1>
    <?php } ?>
    <?php if($sort_3 == 'o-separator' && $line == 'yes' ) { ?>
        <div class="line" style="<?php if( !empty($line_color)) echo 'color: '. esc_html($line_color).';'; ?><?php if( !empty($sp_space)) echo 'padding-bottom: '. esc_html($sp_space).'px;'; ?>"></div>
    <?php } ?>
    <?php if($sort_3 == 'o-description' && !empty($description)) { ?>
        <div class="text " style="<?php if( !empty($description_color)) echo 'color: '. esc_html($description_color).';'; ?><?php if( !empty($desc_space)) echo 'padding-bottom: '. esc_html($desc_space).'px;'; ?>"><?php print esc_html($description); ?></div>
    <?php } ?>

    <?php if($sort_3 == 'o-button' && !empty($b_title) ) { ?>
        <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
        <?php if( !empty($txt_color)) echo 'color: '. esc_html($txt_color).';'; ?>
        <?php if( !empty($btn_bg)) echo 'background: '. esc_html($btn_bg).';'; ?>
        <?php if( !empty($border)) echo 'border-color: '. esc_html($border).';'; ?>
        <?php if( !empty($btn_space)) echo 'padding-bottom: '. esc_html($btn_space).'px;'; ?>
            ">
            <?php print esc_html($b_title); ?>
            <i class="fa fa-chevron-right"></i>
        </a>
    <?php } ?>

    <?php if($sort_4 == 'o-subtitle' && !empty($subtitle)) { ?>
        <h2 style="<?php if( !empty($subtitle_color)) echo 'color: '. esc_html($subtitle_color).';'; ?><?php if( !empty($sbtitle_space)) echo 'padding-bottom: '. esc_html($sbtitle_space).'px;'; ?>">
            <?php print esc_html($subtitle); ?></h2>
    <?php } ?>
    <?php if($sort_4 == 'o-title' && !empty($title)) { ?>
        <h1 style="<?php if( !empty($title_color)) echo 'color: '. esc_html($title_color).';'; ?><?php if( !empty($title_space)) echo 'padding-bottom: '. esc_html($title_space).'px;'; ?>"><?php print esc_html($title); ?></h1>
    <?php } ?>
    <?php if($sort_4 == 'o-separator' && $line == 'yes' ) { ?>
        <div class="line" style="<?php if( !empty($line_color)) echo 'color: '. esc_html($line_color).';'; ?><?php if( !empty($sp_space)) echo 'padding-bottom: '. esc_html($sp_space).'px;'; ?>"></div>
    <?php } ?>
    <?php if($sort_4 == 'o-description' && !empty($description)) { ?>
        <div class="text " style="<?php if( !empty($description_color)) echo 'color: '. esc_html($description_color).';'; ?><?php if( !empty($desc_space)) echo 'padding-bottom: '. esc_html($desc_space).'px;'; ?>"><?php print esc_html($description); ?></div>
    <?php } ?>

    <?php if($sort_4 == 'o-button' && !empty($b_title) ) { ?>
        <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
        <?php if( !empty($txt_color)) echo 'color: '. esc_html($txt_color).';'; ?>
        <?php if( !empty($btn_bg)) echo 'background: '. esc_html($btn_bg).';'; ?>
        <?php if( !empty($border)) echo 'border-color: '. esc_html($border).';'; ?>
        <?php if( !empty($btn_space)) echo 'padding-bottom: '. esc_html($btn_space).'px;'; ?>
            ">
            <?php print esc_html($b_title); ?>
            <i class="fa fa-chevron-right"></i>
        </a>
    <?php } ?>

    <?php if($sort_5 == 'o-subtitle' && !empty($subtitle)) { ?>
        <h2 style="<?php if( !empty($subtitle_color)) echo 'color: '. esc_html($subtitle_color).';'; ?><?php if( !empty($sbtitle_space)) echo 'padding-bottom: '. esc_html($sbtitle_space).'px;'; ?>">
            <?php print esc_html($subtitle); ?></h2>
    <?php } ?>
    <?php if($sort_5 == 'o-title' && !empty($title)) { ?>
        <h1 style="<?php if( !empty($title_color)) echo 'color: '. esc_html($title_color).';'; ?><?php if( !empty($title_space)) echo 'padding-bottom: '. esc_html($title_space).'px;'; ?>"><?php print esc_html($title); ?></h1>
    <?php } ?>
    <?php if($sort_5 == 'o-separator' && $line == 'yes' ) { ?>
        <div class="line" style="<?php if( !empty($line_color)) echo 'color: '. esc_html($line_color).';'; ?><?php if( !empty($sp_space)) echo 'padding-bottom: '. esc_html($sp_space).'px;'; ?>"></div>
    <?php } ?>
    <?php if($sort_5 == 'o-description' && !empty($description)) { ?>
        <div class="text " style="<?php if( !empty($description_color)) echo 'color: '. esc_html($description_color).';'; ?><?php if( !empty($desc_space)) echo 'padding-bottom: '. esc_html($desc_space).'px;'; ?>"><?php print esc_html($description); ?></div>
    <?php } ?>

    <?php if($sort_5 == 'o-button' && !empty($b_title) ) { ?>
        <a href="<?php print esc_url($link); ?>" class="btn-orange" style="
        <?php if( !empty($txt_color)) echo 'color: '. esc_html($txt_color).';'; ?>
        <?php if( !empty($btn_bg)) echo 'background: '. esc_html($btn_bg).';'; ?>
        <?php if( !empty($border)) echo 'border-color: '. esc_html($border).';'; ?>
        <?php if( !empty($btn_space)) echo 'padding-bottom: '. esc_html($btn_space).'px;'; ?>
            ">
            <?php print esc_html($b_title); ?>
            <i class="fa fa-chevron-right"></i>
        </a>
    <?php } ?>

</div>
