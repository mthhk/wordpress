<?php
$text = $textcolor = $author = $acolor = $position = $poscolor =   '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>

<div class="block">
    <div class="description" style="<?php if( !empty($textcolor)) echo 'color: '. esc_html($textcolor).';'; ?>" ><?php print esc_html($text); ?></div>
    <div class="name" style="<?php if( !empty($acolor)) echo 'color: '. esc_html($acolor).';'; ?>" ><?php print esc_html($author); ?></div>
    <div class="designation" style="<?php if( !empty($poscolor)) echo 'color: '. esc_html($poscolor).';'; ?>" ><?php print esc_html($position); ?></div>
</div>

