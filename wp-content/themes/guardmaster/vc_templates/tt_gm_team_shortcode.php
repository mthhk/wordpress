<?php
/*
 * Templatation.com
 *
 * Block with image and effect shortcode for VC
 *
 */
$team_type = $image = $image_link = $name = $position = $social_fb = $social_in = $social_li = $social_tw = $social_pi = $social_google = '';

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);


$img = (is_numeric($image) && !empty($image)) ? wp_get_attachment_url($image) : '';
?>
<!-- end of code from VC -->
<div class="home-our-team">
    <div class="block">
        <div class="picture">
            <img src="<?php print esc_url($img); ?>" class="img-responsive" alt="">

            <div class="member-overlay">
                <div class="icons">
                    <div>
                        <?php if (!empty($social_fb)) { ?>
                            <span class="icon"><a href="<?php print esc_url($social_fb); ?>"><i
                                        class="fa fa-facebook"></i></a></span>
                        <?php } ?>
                        <?php if (!empty($social_tw)) { ?>
                            <span class="icon"><a href="<?php print esc_url($social_tw); ?>"><i
                                        class="fa fa-twitter"></i></a></span>
                        <?php } ?>
                        <?php if (!empty($social_li)) { ?>
                            <span class="icon"><a href="<?php print esc_url($social_li); ?>"><i
                                        class="fa fa-linkedin"></i></a></span>
                        <?php } ?>
                        <?php if (!empty($social_google)) { ?>
                            <span class="icon"><a href="<?php print esc_url($social_google); ?>"><i
                                        class="fa fa-google-plus"></i></a></span>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="info">
                <?php if (!empty($name)) { ?>
                    <div class="name"><?php print esc_html($name); ?></div>
                <?php } ?>
                <?php if (!empty($position)) { ?>
                    <div class="designation"><?php print esc_html($position); ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>