<?php
/*
 * Templatation.com
 *
 * Title with separator shortcode for VC
 *
 */
$price_type = $title = $link = $style_1 = $style_2  = $info = $price
    = $detail = $popular = $text = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
if($currency == '') $currency = '$';
if($timing == '') $timing = 'monthly';

?>
<?php if($price_type == 'style_1') { ?>
<div class="pricing-table-01">
    <div class="block <?php if( $popular == 'yes') echo 'best'; ?>">
        <div class="package"><?php print esc_html($title); ?></div>
        <div class="for"><?php print esc_html($info); ?></div>
        <div class="circle bg1">
            <div class="price"><sup><?php print esc_html($currency); ?></sup><?php print esc_html($price); ?></div>
            <div class="heading"><?php print esc_html($timing); ?></div>
        </div>
        <div class="price-det">
            <?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?>
        </div>
        <div class="button"><a href="<?php print esc_html($link); ?>" class="btn-transparent"><?php print esc_html($text); ?> <i class="fa fa-chevron-right"></i></a></div>
    </div>
</div>
<?php } ?>

<?php if($price_type == 'style_2') { ?>
    <div class="pricing-table-02">
        <div class="block <?php if( $popular == 'yes') echo 'best'; ?>">
            <div class="head">
                <div class="circle bg1">
            <div class="price"><sup><?php print esc_html($currency); ?></sup><?php print esc_html($price); ?></div>
            <div class="heading"><?php print esc_html($timing); ?></div>
                </div>
                <div class="package"><?php print esc_html($title); ?></div>
                <div class="for"><?php print esc_html($info); ?></div>
            </div>
            <div class="price-det">
                <?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?>
            </div>
            <div class="button"><a href="<?php print esc_html($link); ?>" class="btn-transparent"><?php print esc_html($text); ?> <i class="fa fa-chevron-right"></i></a></div>

        </div>
    </div>
<?php } ?>