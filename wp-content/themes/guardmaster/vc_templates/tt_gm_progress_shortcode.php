<?php
/*
 * Templatation.com
 *
 * Block with image and effect shortcode for VC
 *
 */
$title = $num = '';

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

?>


<div class="progress-01 skills-graph skill">
    <div class="percentage">
        <div class="count"><?php print esc_html($num); ?>%</div>
        <div class="line2"></div>
    </div>
    <div class="info">
        <div class="caption"><?php print esc_html($title); ?></div>
        <div class="progress-label clearfix" style="width: <?php print esc_html($num); ?>%;">
            <div class="icon"><i class="fa fa-circle"></i></div>
        </div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="<?php print esc_html($num); ?>" aria-valuemin="0" aria-valuemax="<?php print esc_html($num); ?>" style="width: <?php print esc_html($num); ?>%;">
            </div>
        </div>
    </div>
</div>

