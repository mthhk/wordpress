<?php
/*
 * Templatation.com
 *
 * Title with separator shortcode for VC
 *
 */
$title = $txt_color = $number = $num_color = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>

<div class="funfacts">
    <div class="block">
        <div class="box">
            <h2 style=" <?php if( !empty($txt_color)) echo 'color: '. esc_html($txt_color).';'; ?>"><?php print esc_html($title); ?></h2>
            <h1 style=" <?php if( !empty($num_color)) echo 'color: '. esc_html($num_color).';'; ?>"><?php print esc_html($number); ?></h1>
            <div style=" <?php if( !empty($arrow_color)) echo 'color: '. esc_html($arrow_color).';'; ?>" class="caret-down"><i class="fa fa-caret-down"></i></div>
        </div>
    </div>
</div>