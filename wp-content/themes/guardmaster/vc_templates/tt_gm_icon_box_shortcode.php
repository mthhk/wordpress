<?php
/*
 * Templatation.com
 *
 * Infobox shortcode for VC
 *
 */
$image = $source = $img_border_color = $img_style = $subtitle = $title_tag = $custom_icon = $title = $title_text_transform = $img_bdr_cust_color = $subheading = $insert_graphic = $title_link = $ins_button = $button_text = $button_link = $button_link_target = $icon_link = $el_class = '';
$alignment = 'left'; $title_link_target = 'self'; //TODO
$ins_button = false; $text_appear = 'dark';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );


// This code is from VC plugin
// For images

$default_src = vc_asset_url( 'vc/no_image.png' );
$img_style = ( '' !== $img_style ) ? $img_style : '';
$img_border_color = ( '' !== $img_border_color ) ? ' vc_box_border_' . $img_border_color : '';

$img_id = preg_replace( '/[^\d]/', '', $image );

switch ( $source ) {
	case 'media_library':
		$img = wpb_getImageBySize( array(
			'attach_id' => $img_id,
			'thumb_size' => $img_size,
			'class' => 'vc_single_image-img',
		) );

		break;

	case 'external_link':
		$dimensions = vcExtractDimensions( $img_size );
		$hwstring = $dimensions ? image_hwstring( $dimensions[0], $dimensions[1] ) : '';

		$custom_src = $custom_src ? esc_attr( $custom_src ) : $default_src;

		$img = array(
			'thumbnail' => '<img class="vc_single_image-img" ' . $hwstring . ' src="' . $custom_src . '" />',
		);
		break;

	default:
		$img = false;
}

if ( ! $img ) {
	$img['thumbnail'] = '<img class="vc_single_image-img" src="' . $default_src . '" />';
}

$wrapperClass = 'vc_single_image-wrapper ' . $img_style . ' ' . $img_border_color;
$img_link = vc_gitem_create_link( $atts, $wrapperClass );
if( !empty( $img_border_cust_color ))
	$img_bdr_cust_color = 'style="background-color:' . esc_attr( $img_border_cust_color ) . ' !important";';

$image_string = ! empty( $img_link ) ? '<' . $img_link . '>' . $img['thumbnail'] . '</a>' : '<div ' . $img_bdr_cust_color . ' class="' . $wrapperClass . '"> ' . $img['thumbnail'] . ' </div>';

// For icons
// Enqueue needed icon font.
vc_icon_element_fonts_enqueue( $type );
//$icon_link = vc_gitem_create_link( $atts, 'vc_icon_element-link' );

$has_style = false;
if ( strlen( $background_style ) > 0 ) {
	$has_style = true;
	if ( false !== strpos( $background_style, 'outline' ) ) {
		$background_style .= ' vc_icon_element-outline'; // if we use outline style it is border in css
	} else {
		$background_style .= ' vc_icon_element-background';
	}
}

$style = '';
if ( 'custom' === $background_color ) {
	if ( false !== strpos( $background_style, 'outline' ) ) {
		$style = 'border-color:' . $custom_background_color;
	} else {
		$style = 'background-color:' . $custom_background_color;
	}
}
$style = $style ? 'style="' . esc_attr( $style ) . '"' : '';

?>
<!-- end of code from VC -->

<?php
$align = ($alignment == 'left') ? 'textleft' : ($alignment == 'right' ? 'textright' : '' );
if( empty($title_tag) || $title_tag == 'default' ) $title_tag = 'h4';
$title_tag = ($title_tag == 'div') ? 'div' : $title_tag ;
$title_tag = esc_attr( $title_tag );
$title = esc_attr( $title );
$title = empty( $title_link ) ? $title : ('<a href='. esc_url( $title_link ) .' target=_'. $title_link_target .'>'. $title .'</a>');

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

?>


<div class="security-solutions">
<div class="block <?php  echo esc_attr( $css_class ). ' '. esc_attr( $el_class ). ' ' . esc_attr( $text_appear ). ' ' . esc_attr( $alignment ). ' '. esc_attr( $title_text_transform ). ' size-'. esc_attr( $title_tag ); ?>">

	<!-- image settings -->
	<?php
	if( ! (strpos($insert_graphic, 'image') === false) ) { ?>
		<div class="wpb_single_image">
			<figure class="wpb_wrapper vc_figure">
				<?php echo wp_kses_post($image_string); ?>
			</figure>
		</div>
	<?php } ?>

	<!-- icon settings -->
	<?php if( ! (strpos($insert_graphic, 'icon' ) === false) ) { ?>

	<?php if(!empty( $custom_icon ) ) {
		echo '<div class="icon"><i class="fa '.$custom_icon.' "></i></div>';
	}else { ?>

			<div class="icon vc_icon_element-inner vc_icon_element-color-<?php echo esc_attr( $icon_color ); ?> <?php if ( $has_style ) { echo 'vc_icon_element-have-style-inner'; } ?> vc_icon_element-size-<?php echo esc_attr( $size ); ?>  vc_icon_element-style-<?php echo esc_attr( $background_style ); ?> vc_icon_element-background-color-<?php echo esc_attr( $background_color ); ?>" <?php echo $style ?>>
				<span
					class="vc_icon_element-icon <?php echo esc_attr( ${'icon_' . $type} ); ?>" <?php echo( 'custom' === $icon_color ? 'style="color:' . esc_attr( $icon_custom_color ) . ' !important"' : '' ); ?>></span><?php
				if ( strlen( $icon_link ) > 0 ) {
					echo '<' . $icon_link . '></a>';
				}
				?> </div>

		<?php } ?>

	<?php } ?>

	<?php echo '<'. $title_tag .' class="info_title">' . $title .  '</'. $title_tag .'>' ?>
	<?php if( ! empty( $subtitle )) { ?>
		<div class="tt-infosubtitle">
			<?php echo esc_attr( $subtitle ); ?>
		</div>
	<?php } ?>
	<div class="line2"></div>
	<?php if( ! empty( $content )) { ?>
		<div class="description">
			<?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?>
		</div>
	<?php } ?>
	<?php if($ins_button && !empty($button_text)) { ?>
		<a class="tt-button" target="<?php echo $button_link_target;?>" href="<?php echo esc_url( $button_link );?>">
			<?php echo esc_attr( $button_text );?></a>
	<?php }?>
</div></div>