<?php
/*
 * Templatation.com
 *
 * Title with separator shortcode for VC
 *
 */
$headline_type = $title = $link = $style_1 = $style_2  = $titlecolor = $subtitle
    = $sutitlecolor = $bordercolor = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>

<?php if($headline_type == 'style_1') { ?>
    <div class="boxed-heading">
        <h3 style="
        <?php if( !empty($sutitlecolor)) echo 'color: '. esc_html($sutitlecolor).';'; ?>
            "><?php print esc_html($title); ?></h3>
        <h2 style="
        <?php if( !empty($titlecolor)) echo 'color: '. esc_html($titlecolor).';'; ?>
            "><?php print esc_html($subtitle); ?></h2>
    </div>
<?php } ?>

<?php if($headline_type == 'style_2') { ?>
    <div class="download-brochure">
        <div class="button"><a style="<?php if( !empty($titlecolor)) echo 'color: '. esc_html($titlecolor).';'; ?>
                "  href="<?php print esc_url($link); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i><?php print esc_html($title); ?></a></div>
    </div>

<?php } ?>