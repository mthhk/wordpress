<?php
/*
 * Templatation.com
 *
 * Title with separator shortcode for VC
 *
 */
$btn_type = $title = $link = $txt_color = $btn_bg = $border = $add_icon = $type_icon = $icon_color = $border_icon = $icon_bg = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>

<?php if($btn_type == 'btn_normal') { ?>
    <a href="<?php print esc_url($link); ?>" class="btn-orange btn-small <?php if( $add_icon !== 'yes') echo 'border-btn'; ?><?php if( $add_icon == 'yes') echo 'btn-gm'; ?>" style="
    <?php if( !empty($txt_color)) echo 'color: '. esc_html($txt_color).';'; ?>
    <?php if( !empty($btn_bg)) echo 'background: '. esc_html($btn_bg).';'; ?>
    <?php if( !empty($border)) echo 'border-color: '. esc_html($border).';'; ?>
        ">
        <?php print esc_html($title); ?>
    <?php if($add_icon == 'yes') { ?>
    <i class="fa fa-chevron-right" style="color: <?php print esc_html($icon_color); ?>; background:<?php print esc_html($icon_bg); ?>; border-color:<?php print esc_html($border_icon); ?>;"></i>
    <?php } ?>
</a>
<?php } ?>

<?php if($btn_type == 'btn_big') { ?>
    <a href="<?php print esc_url($link); ?>" target="__BLANK" class="btn-orange  btn-big <?php if( $add_icon !== 'yes') echo 'border-btn'; ?><?php if( $add_icon == 'yes') echo 'btn-gm'; ?>" style="
    <?php if( !empty($txt_color)) echo 'color: '. esc_html($txt_color).';'; ?>
    <?php if( !empty($btn_bg)) echo 'background: '. esc_html($btn_bg).';'; ?>
    <?php if( !empty($border)) echo 'border-color: '. esc_html($border).';'; ?>
        ">
        <?php print esc_html($title); ?>
        <?php if($add_icon == 'yes') { ?>
            <i class="fa fa-chevron-right" style="color: <?php print esc_html($icon_color); ?>; background:<?php print esc_html($icon_bg); ?>; border-color:<?php print esc_html($border_icon); ?>;"></i>

        <?php } ?>
    </a>
<?php } ?>