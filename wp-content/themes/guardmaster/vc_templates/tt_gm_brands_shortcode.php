<?php

$list_type = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>


<div class="clients">
    <div class="clients-carousel">
        <?php print do_shortcode($content); ?>
    </div>
</div>


