<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * The main template file.
 *
 * Works as index and fallback.
 *
 */

//  get_header();

?>
<!-- MAIN CONTENT BLOCK -->
<?php do_action( 'tt_before_mainblock' ); ?>
<section id="mainblock" class="mainblock blog index">
<div class="mbottom70 tpadd"></div>
    <div class="container">
        <div class="row">
            <div class="<?php echo tt_temptt_sidebar_pos(); ?>">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile;
				else : ?>
					<!-- no posts -->
				<?php endif; ?>
                <div class=" pager-article  practice-v3-paginat clearfix">
                <?php
			        the_posts_pagination( array(
			        'prev_text'          => '<i class="fa fa-long-arrow-left"></i>',
			        'next_text'          => '<i class="fa fa-long-arrow-right"></i>'
			        ) );
			        ?>
                </div>
             </div> 
            <?php if ( function_exists( 'tt_temptt_get_sidebar' ) ) echo tt_temptt_get_sidebar(); ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>