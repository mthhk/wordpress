<?php get_header(); ?>
<!-- MAIN CONTENT BLOCK -->
<section id="mainblock" class="mainblock">
<div class="mbottom70 tpadd"></div>
	<div class="container">

		<div class="row">

			<div class="<?php print tt_temptt_sidebar_pos( 'content' ); ?>">
				<h1 class="error-title page-title align-center"><?php esc_html_e( '404 - Page Not Found', 'guardmaster' ); ?></h1>

				<div class="mesage-box color-3">
					<div class="description">
						<?php print sprintf( esc_html__( 'We could not find the page you are looking for. Please search below or %1$s.', 'guardmaster' ), '<a href="' . esc_url( home_url('/') ) . '">'.esc_html__( 'Go back to Home', 'guardmaster' ).'</a>' ) ?>
					</div>
					<div class="mbottom70"></div>
					<div class="widget widget_search col-md-11">
						<?php get_search_form(); ?>
					</div>
				</div>
				<div class="spacer30"></div>
			</div>
			<?php if ( function_exists( 'tt_temptt_get_sidebar' ) ) print tt_temptt_get_sidebar(); ?>

		</div>

	</div>
</section>
<?php get_footer(); ?>