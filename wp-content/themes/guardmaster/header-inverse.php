<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="<?php if ( function_exists( 'tt_temptt_hdr_class' ) ) print tt_temptt_hdr_class(); ?>">

		<nav id="navigation" class="<?php if ( function_exists( 'tt_temptt_hdr_class' ) ) print tt_temptt_hdr_class(); ?>">
			<div id="tc" class="container top-contact hidden-sm hidden-xs">
				<div class="row">
					<div class="col-lg-2 col-md-2">
						<!--  Logo Starts -->
						<?php if ( function_exists( 'tt_gmaster_logo' ) ) echo tt_gmaster_logo(); ?>
						<!-- Logo Ends -->
					</div>
					<?php if ( function_exists( 'tt_gmaster_hdr_info' ) ) echo tt_gmaster_hdr_info(); ?>
				</div>
			</div>
			<div class="navbar navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle"> <span class="sr-only"><?php esc_attr_e("Toggle navigation",'guardmaster'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
						<!--  Logo Starts -->
						<!--<a class="navbar-brand external visible-xs" href="index.html"><img src="images/logos/footer-logo.png" alt=""></a>-->
						<!-- Logo Ends -->
					</div>
					<div class="collapse navbar-collapse">
			            <?php
			            if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary-menu' ) ) {
			                wp_nav_menu( array( 'depth'          => 3,
			                                    'sort_column'    => 'menu_order',
			                                    'container'      => 'ul',
			                                    'menu_class'     => 'nav navbar-nav',
			                                    'theme_location' => 'primary-menu'
			                ) );
			            } else {
			                print "Please assign primary menu in wp-admin->Appearance->Menus";
			            } ?>
					<?php if ( function_exists( 'tt_gmaster_social' ) ) echo tt_gmaster_social(); ?>
					<ul>
						<li><a>Test</a></li>
					</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</nav>
</header>