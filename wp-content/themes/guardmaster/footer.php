<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Footer Template
 *
 * Here we setup all logic and XHTML that is required for the footer section of all screens.
 *
 * @package ttFramework
 * @subpackage Template
 */
	$total = tt_temptt_get_option('footer_sidebars', '4');
?>
	<footer id="contact">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6">												
						<ul class="f-address">
							<li>								
								<div class="row">
									<h4 class="mb-4 font-weight-bold"><u>HQ Office</u></h4>
								</div>		
								<div class="row">
									<h4 class="mb-4 font-weight-bold">Corporate Matters</h4>
								</div>
								<div class="row">									
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-map-marker-alt"></i> Address: <label>20 Sin Ming Lane #08-63 Midview City Singapore 573968</label></h5>
									</div>
								</div>
							</li>
							<li>								
								<div class="row">
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-phone-alt"></i> Phone: <label>6352 6769</label></h5>										
									</div>
								</div>
								<div class="row">
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-envelope"></i> Email: <label>hr@metropolis.sg</label></h5>										
									</div>
								</div>
								<div class="row">
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-clock"></i> Operating Hours: <label>Mon - Fri: 8:30 AM to 6:00 PM</label></h5>								
									</div>
								</div>
								<div class="row">
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-comment-alt"></i> Feedback: <label>feedback@metropolis.sg</label></h5>
									</div>
								</div>
							</li></br>
							<li style="padding-top:20px">
								<div class="row">
									<h4 class="mb-4 font-weight-bold">Follow Us!</h4>
								</div>
								<div class="row">
									
									<div class="col-1"></div>
									<div class="col-10">
										<a href="https://www.facebook.com/metropolis888/" style="padding-right:10px;" target="__BLANK"> <i class="fa fa-facebook fa-2xl" ></i></a>	
										<a href="https://www.linkedin.com/company/metropolis-security-systems-pte-ltd/" target="__BLANK"> <i class="fa fa-linkedin fa-2xl" ></i></a>	
									</div>
								</div>
								
							</li>
							
							
						</ul>
					</div>
					
					<div class="col-lg-6 col-md-6 col-sm-6 ">
												
						<ul class="f-address">
							<li>								
								<div class="row">
									<h4 class="mb-4 font-weight-bold"><u>Operations Office</u></h4>
								</div>		
								<div class="row">
									<h4 class="mb-4 font-weight-bold">Operations Matters</h4>
								</div>
								<div class="row">									
									<div class="col-12">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-map-marker-alt"></i> Address: <label>20 Sin Ming Lane #07-52/53Singapore 573968</label></h5>
									</div>
								</div>
							</li>
							<li>		
								<div class="row">
									<h4 class="mb-4 font-weight-bold">Command Centre (24/7)</h4>
								</div>
								<div class="row">									
									<div class="col-6">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-phone-alt"></i> Phone: <label>6748 4233</label></h5>										
									</div>							
								</div>
								<div class="row">
									<h4 class="mb-4 font-weight-bold">Recruitment Matters / QMT</h4>
								</div>
								<div class="row">									
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-phone-alt"></i> Phone: <label>6748 4230</label></h5>										
									</div>
								</div>
								<div class="row">
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-envelope"></i> Email: <label>recruitment@metropolis.sg</label></h5>										
									</div>
								</div>
								<div class="row">
									<h4 class="mb-4 font-weight-bold">Quotation / Sales Enquiries</h4>
								</div>
								<div class="row">									
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-phone-alt"></i> Phone: <label>6748 2801</label></h5>										
									</div>
								</div>
								<div class="row">
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-envelope"></i> Email: <label>quotation@metropolis.sg</label></h5>										
									</div>
								</div>
								<div class="row">
									<div class="col-1"></div>
									<div class="col-10">
										<h5 class="font-weight-bold mb-0"><i class="fas fa-clock"></i> Operating Hours: <label>Mon - Fri: 8:30 AM to 6:00 PM</label></h5>										
									</div>
								</div>								
							</li>
							
							
						</ul>
					</div>
					
								
				</div>
			</div>
		</footer>
		<!-- Copyright -->
		<section class="copyright">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4 col-lg-4">
						<div class="text-center align-middle text-white">
							<a href="http://web.mpolis.sg/privacypoilcy/">Privacy policy</a>
						</div>
					</div>
					<div class="col-md-4 col-lg-4">
						<div class="text-center text-white">
							&copy; 2022 Metropolis Security Systems Pte Ltd. All Rights Reserved.
						</div>
					</div>
					<div class="col-md-4 col-lg-4">
						<img src='https://web.mpolis.sg/wp-content/uploads/2022/03/hr_awards.png' class='img-fluid' width='50'>
						<img src='https://web.mpolis.sg/wp-content/uploads/2022/03/EXSA-2021.png' class='img-fluid' width='50'>
						<img src='https://web.mpolis.sg/wp-content/uploads/2022/03/ihrp.png' class='img-fluid' width='50'>
						<img src='https://web.mpolis.sg/wp-content/uploads/2022/03/HCP.png' class='img-fluid' width='50'>
						<img src='https://web.mpolis.sg/wp-content/uploads/2022/03/use.png' class='img-fluid' width='50'>
						<img src='https://web.mpolis.sg/wp-content/uploads/2022/03/bizsafe-star-new.png' class='img-fluid' width='50'>
						<img src='https://web.mpolis.sg/wp-content/uploads/2022/03/sas.png' class='img-fluid' width='50'>
					</div>
				</div>
			</div>
		</section>
<a href="#" class="scrollup"></a>
<?php wp_footer(); ?>

</body>
</html>
