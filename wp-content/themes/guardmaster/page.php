<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Page Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * author: Templatation
 *
 */
	get_header();
?>
	
<!-- MAIN CONTENT BLOCK -->
<?php do_action( 'tt_before_mainblock' ); ?>
<section id="mainblock" class="mainblock">
	<div class="container">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php echo tt_temptt_post_title('h1'); ?>
		<?php the_content(); ?>

		<?php endwhile; endif;

			if ( comments_open() ) {  ?>
			 <div class="comment-part">
			 <?php comments_template( '', true );  ?>
			 </div>
			<?php
			}
            ?>

	</div>
	
</section>

<?php get_footer(); ?>