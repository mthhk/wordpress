<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Page Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * author: Templatation
 *
 */
	get_header();
?>
	
<!-- MAIN CONTENT BLOCK -->
<?php do_action( 'tt_before_mainblock' ); ?>
<section id="mainblock" class="mainblock">
	<div class="container">

        <!-- The WooCommerce loop -->
        <?php woocommerce_content(); ?>

	</div>
	
</section>

<?php get_footer(); ?>