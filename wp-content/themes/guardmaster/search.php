<?php get_header(); ?>
<!-- MAIN CONTENT BLOCK -->
<section id="mainblock" class="mainblock blog search">
<div class="mbottom70 tpadd"></div>
	<div class="container">
		<?php do_action( 'tt_after_container_start' ); ?>
		<div class="row">

			<div class="<?php echo tt_temptt_sidebar_pos( 'content' ); ?>">
			<div class="single-blog-title"><h1 class="ml-title"><?php esc_html_e( 'Search Results For: ', 'guardmaster' ); echo the_search_query(); ?></h1></div>

			<div class="blog-block">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

			<?php endwhile; else : ?>

			<div class="align-center"><?php esc_html_e( 'We could not find anything for the search term: ', 'guardmaster' ); echo the_search_query(); ?></div>

			<?php endif; ?>
			</div>
			</div>

			<?php if ( function_exists( 'tt_temptt_get_sidebar' ) ) echo tt_temptt_get_sidebar(); ?>

		</div>

	</div>
</section>

<?php get_footer(); ?>