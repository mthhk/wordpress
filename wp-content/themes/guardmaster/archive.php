<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * The main template file.
 *
 * Works as index and fallback.
 *
 */

 get_header();

?>
<!-- MAIN CONTENT BLOCK -->
<?php do_action( 'tt_before_mainblock' ); ?>
<section id="mainblock" class="mainblock blog archive">
<div class="mbottom70 tpadd"></div>
	<div class="container">
		<div class="row">

			<div class="<?php print tt_temptt_sidebar_pos(); ?>">

			<?php
				if ( is_category() ) {
					$title = single_cat_title( '', false );
				} elseif ( is_tag() ) {
					$title = single_tag_title( '', false );
				} elseif ( is_author() ) {
					$title = get_the_author();
				} elseif ( is_year() ) {
					$title = get_the_date( 'Y' );
				} elseif ( is_month() ) {
					$title = get_the_date( 'F Y' );
				} elseif ( is_day() ) {
					$title = get_the_date( 'F j, Y' );
				} elseif ( is_post_type_archive() ) {
					$title = post_type_archive_title( '', false );
				} elseif ( is_tax() ) {
					$tax = get_taxonomy( get_queried_object()->taxonomy );
					$title = $tax->labels->singular_name . ' ' . single_term_title( '', false );
				} elseif ( is_search() ) {
					$title = get_the_title( $post_id ) . ' ' . get_search_query();
				} else {
					$title = esc_html__( 'Archives', 'guardmaster' );
				}
			?>

			<div class="single-blog-title"><h1 class="ml-title"><?php print esc_attr($title); ?></h1></div>
			<?php tt_gmaster_archive_desc(); ?>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

			<?php endwhile; else : ?>

				<!-- no posts -->

			<?php endif; ?>

			</div>
		<?php if ( function_exists( 'tt_temptt_get_sidebar' ) ) print tt_temptt_get_sidebar(); ?>
		</div>

	</div>
</section>

<?php get_footer(); ?>