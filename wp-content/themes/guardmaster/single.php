
<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * The main template file.
 *
 * Works as index and fallback.
 *
 */
 get_header();
//check if post has the image.
$postimgg = '';
if(function_exists('tt_gmaster_post_thumb')) $postimgg = tt_gmaster_post_thumb(760, 368);
?>
<?php do_action( 'tt_before_container' ); ?>
	<div class="container mainblock contents blog single-box">
		<div class="row no-gutter-8 no-gutter no-gutter-4">
			<!-- RIGHT COLUMN STARTS
            ========================================================================= -->
			<div class="col-lg-8 col-md-8">
				<!-- Post Starts -->
				<div class="row blog-post no-gutter-12">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="col-lg-12 post-contents">
						<div class="meta"><span class="gm-comma">-:</span> <?php the_time('M d, Y'); ?>   /  <?php the_author(); ?> </div>
						<div class="clearfix">
							<h1><?php the_title(); ?></h1>
							<?php if( comments_open()) { ?><a href="#comments"><div class="comments-2"><?php comments_number( '0', '1', '%' ); ?></div></a><?php } ?>
						</div>
						<div class="description">
							<?php the_content(); ?>
							<?php wp_link_pages(); ?>
						</div>
					<?php if( has_tag() ) { ?>
			        <div class="tt_line mtop20"></div>
			        <div class="fix tt-single-tags">
			            <div class="fix social-link-left-text">
			                <?php the_tags($before = null, $sep = ', ', $after = ''); ?>
			            </div>
			        </div>
					<?php } ?>
					<?php if( has_category() ) { ?>
					<div class="tt_line mtop20"></div>
					<span class="tt-single-tags"><?php esc_html_e( 'Posted in: ', 'guardmaster' ); ?>
						<?php if( function_exists('tt_gmaster_get_cats') ) echo tt_gmaster_get_cats( ); ?>
					</span>
					<?php } ?>
					</div>
					<?php endwhile;
					else : ?>
						<!-- no posts -->
					<?php endif; ?>
				</div>
				<div class="row prevnext-post no-gutter-12">
					<?php if( function_exists('tt_gmaster_prev_post') ) echo tt_gmaster_prev_post(); ?>
					<?php if( function_exists('tt_gmaster_next_post') ) echo tt_gmaster_next_post(); ?>
				</div>
				<!-- Leave a Reply Starts -->
				<?php
				// Comments
				if ( comments_open() )	comments_template( '', true );
				?>

				<!-- Leave a Reply Ends -->
			</div>

			<div class="col-lg-4 col-md-4 sidebar">
				<?php get_template_part( 'sidebar', get_post_format() ); ?>
			</div>

		</div>
	</div>
<?php get_footer(); ?>