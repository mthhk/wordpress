<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------*/
/* This file hooks the metaboxes to Metabox system powered by TT FW plugin.
/*-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* CS meta boxes override                                                            */
/*-----------------------------------------------------------------------------------*/
// framework options filter example
if( !function_exists( 'tt_temptt_metas_opt' )) {
	function tt_temptt_metas_opt( $options ) {

// Lets create options that we will use mostly, in page, product, pages
	$tt_temptt_default_meta =  array(
				// begin: a section
				array(
					'name'   => 'section_1',
					'title'  => 'General Settings',
					'icon'   => 'fa fa-cog',
					// begin: fields
					'fields' => array(

						array(
						  'id'    => '_hide_title_display',
						  'type'  => 'switcher',
						  'title' => esc_html__( 'Hide default title display in middle content area.', 'guardmaster' ),
						  'desc'  => esc_html__( 'In some case, you might want to hide the default title display. Check this to hide title. If you are not sure about it  leave it unchecked. N/A for woocommerce products.', 'guardmaster' ),
						  'default' => false
						),

						array(
							'id'      => '_single_enable_overlap',
							'type'    => 'switcher',
							'title'   => esc_html__( 'Enable overlapping of header area on this page', 'guardmaster' ),
							'label'   => esc_html__( 'When enabled, the page content starts right from the top, behind the menu area. Recommended to enable if you enable hero section below.', 'guardmaster' ),
							'default' => false
						),

						array(
							'id'      => '_single_enable_headline',
							'type'    => 'switcher',
							'title'   => esc_html__( 'Enable Hero section on this page', 'guardmaster' ),
							'label'   => esc_html__( 'Hero section appears after header and before content area. Note: Hero section is full liquid width. If you want boxed Hero section, use visual composer Hero Component to build it please. Note: Visual composer must be active for Hero section to work. Works best when overlapping enabled above.', 'guardmaster' ),
							'default' => false
						),
						array(
							'id'         => '_single_headline_title',
							'type'       => 'text',
							'title'      => esc_html__( 'Main Title.', 'guardmaster' ),
							'dependency' => array( '_single_enable_headline', '==', 'true' ),
						),
						array(
							'id'    => '_single_page_img',
							'type'  => 'upload',
							'title' => esc_html__( 'Hero area background', 'guardmaster' ),
							'desc'  => esc_html__( 'This image appears in background of hero section above. As the text is light, dark image is suggested. Recommended image size is 1300x400 px. Note: If left empty, default background color displays.', 'guardmaster' ),
							'dependency' => array( '_single_enable_headline', '==', 'true' ),
						),
						array(
						  'id'      => '_single_page_color',
						  'type'    => 'color_picker',
						  'title'   => esc_html__( 'Background color.', 'guardmaster' ),
						  'desc'    => esc_html__( 'You can choose to have background color, also, if you make this color transparent, it will work as overlay color on the image chosen above.', 'guardmaster' ),
						  'default' => '',
						  'rgba'    => true,
						  'dependency' => array( '_single_enable_headline', '==', 'true' ),
						),

						array(
						  'id'       => '_single_text_apprnce',
						  'type'     => 'select',
						  'title' => esc_html__( 'Text Appearance.', 'guardmaster' ),
						  'desc' => esc_html__( 'Based on the color of background image, you might need to make text color Light or Dark.', 'guardmaster' ),
						  'options'  => array(
						    'light'  => esc_html__( 'Light', 'guardmaster' ),
						    'dark'  => esc_html__( 'Dark', 'guardmaster' ),
						  ),
							'dependency' => array( '_single_enable_headline', '==', 'true' ),
						  'default'  => 'light',
						),
						array(
							'id'         => '_single_hero_tpadding',
							'type'       => 'text',
							'title'      => esc_html__( 'Hero Top Padding.', 'guardmaster' ),
							'desc'      => esc_html__( 'Change space above Hero heading if you want to. Default : 200. Enter value without px, eg: 150', 'guardmaster' ),
							'default'      => '200',
							'dependency' => array( '_single_enable_headline', '==', 'true' ),
						),
						array(
							'id'         => '_single_hero_bpadding',
							'type'       => 'text',
							'title'      => esc_html__( 'Hero Bottom Padding.', 'guardmaster' ),
							'desc'      => esc_html__( 'Change space below Hero heading if you want to. Default : 92. Enter value without px, eg: 100', 'guardmaster' ),
							'default'      => '92',
							'dependency' => array( '_single_enable_headline', '==', 'true' ),
						),
						array(
						  'id'    => '_single_hero_breadcrumbs',
						  'type'  => 'switcher',
						  'title' => esc_html__( 'Insert breadcrumbs in Hero section.', 'guardmaster' ),
						  'desc'  => esc_html__( 'Insert breadcrumb in this area. Note: Yoast SEO plugin must be active, and Breadcrumb enabled in SEO/Advanced in wp-admin.', 'guardmaster' ),
						  'default' => true,
						  'dependency' => array( '_single_enable_headline', '==', 'true' ),
						),
						array(
							'id'      => '_single_enable_tpadding',
							'type'    => 'switcher',
							'title'   => esc_html__( 'Enable Top padding', 'guardmaster' ),
							'label'   => esc_html__( 'When enabled, the page has some white space on the top. Recommended to disable if you want page builder to cover from top of this page. Note: This may not work on custom page templates, because they have their own custom setup.', 'guardmaster' ),
							'default' => true
						),
						array(
							'id'      => '_single_enable_bpadding',
							'type'    => 'switcher',
							'title'   => esc_html__( 'Enable bottom padding', 'guardmaster' ),
							'label'   => esc_html__( 'When enabled, the page has some space on the bottom before footer starts. Recommended to disable if you want page builder to cover till last of this page. Note: This may not work on custom page templates, because they have their own custom setup.', 'guardmaster' ),
							'default' => true
						),

					), // end: fields
				) // end: a section
);


/* Start creating meta options. */

$options = array(); // remove old options

// -----------------------------------------
// Page Metabox Options                    -
// -----------------------------------------

		$options[] = array(
			'id'        => '_tt_meta_page_opt',
			'title'     => 'Page Options',
			'post_type' => 'page',
			'context'   => 'normal',
			'priority'  => 'default',
			'sections'  => $tt_temptt_default_meta

		);
		$options[] = array(
			'id'        => '_tt_meta_page_opt',
			'title'     => 'Posts Options',
			'post_type' => 'post',
			'context'   => 'normal',
			'priority'  => 'default',
			'sections'  => $tt_temptt_default_meta

		);
		$options[] = array(
			'id'        => '_tt_meta_page_opt',
			'title'     => 'Products Options',
			'post_type' => 'product',
			'context'   => 'normal',
			'priority'  => 'default',
			'sections'  => $tt_temptt_default_meta

		);

		// Note : Meta options for CPTs are triggered from templatation-framework plugin as needed by this theme.

		return $options;

	}

	add_filter( 'cs_metabox_options', 'tt_temptt_metas_opt' );

// These are default options, these are being used for page,post,product

}


