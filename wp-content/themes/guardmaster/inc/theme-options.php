<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------*/
/* This file hooks the redux options panel. While Redux powered by TT FW plugin.
/*-----------------------------------------------------------------------------------*/


add_filter('redux/options/tt_temptt_opt/sections', 'tt_gmaster_redux_options');

if ( ! function_exists( 'tt_gmaster_redux_options' ) ) {
    function tt_gmaster_redux_options( $sections ) {

	//reset themeoptions array
    $sections = array();

	$shortname = 'tt';

    /*
     *
     * ---> START SECTIONS
     *
     */

/*-----------------------------------------------------------------------------------*/
/* General Settings                                                                  */
/*-----------------------------------------------------------------------------------*/

    $sections[] = array(
        'title'  => esc_html__( 'General Settings', 'guardmaster' ),
        'id'     => 'general',
        'desc'   => esc_html__( 'General Settings.', 'guardmaster' ),
        //'icon'   => 'el el-home ',
        'customizer_width' => '400px',
    );
	// quick start.
    $sections[] = array(
        'title'            => esc_html__( 'Quick Start', 'guardmaster' ),
        'id'               => 'general-quickstart',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id' => $shortname . '_logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'Upload Logo', 'guardmaster' ),
                'readonly'    => false,
                'subtitle'     => esc_html__( 'Upload a logo for your theme, or specify an image URL directly. For retina friendliness you can upload 2x logo and set desired dimension in retina ready graphics option below.', 'guardmaster' ),
                'desc' => esc_html__( 'Ideal size of the logo is 125 x 45 px.', 'guardmaster' ),
            ),
            array(
                'id'       => $shortname . '_retina_favicon',
                'type'     => 'switch',
                'title'    => 'Retina Ready Logo',
                'subtitle' => esc_html__( 'Do you want sharp logo display on retina display devices?', 'guardmaster' ),
                'desc'     => esc_html__( 'To make logo look sharp on Retina devices, you need to upload double size logo above and turn it on to be able to enter desired width/height for logo image. If you are not sure what this means leave it off. Note: To comply with all retina devices app icon etc, please go to Appearance->customize->site identity.', 'guardmaster' ),
                'default'  => false
            ),
            array(
                'id' => $shortname . '_retina_logo_wh',
                'type'           => 'dimensions',
                'units'          => array( 'px' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => esc_html__( 'Dimensions (Width/Height) Option', 'guardmaster' ),
                'subtitle'       => esc_html__( 'Enter desired Width and height for your retina logo. If you want logo to be retina ready, please upload double size version of logo above and enter the dimensions you want it to appear in. Recommended: 125 x 45.', 'guardmaster' ),
                'desc'       => esc_html__( ' Normally it will be half of actual retina logo image dimensions. Enter values without px eg: 50, 70 etc. Recommended: Leave blank to disable this. Note: If your logo is distorted, try putting 0 in Height.', 'guardmaster' ),
                'required' => array( $shortname . '_retina_favicon', '=', true ),
                 'default'        => array(
                    'width'  => 0,
                    'height' => 0,
                )
            ),
            array(
                'id'    => $shortname . '_favicon_info1',
                'type'  => 'media',
                'url'      => true,
                'readonly'    => false,
                'title' => esc_html__( 'Favicon Icon', 'guardmaster' ),
                'desc'  => esc_html__( 'Upload a favicon if you are running below WP 4.3. Please go to Appearance-> customise -> site identity to upload favicon if you are running WordPress version 4.3 or above.', 'guardmaster' ),
            ),
           array(
                'id' => $shortname . '_gmap_api',
                'type'     => 'text',
                'title' => esc_html__( 'Google Map API Text', 'buildcon' ),
                'desc'  => sprintf( esc_html__( '%1$s for complete instructions to get an API key from Google itself.', 'buildcon' ), '<a href="https://developers.google.com/maps/documentation/javascript/get-api-key">' . esc_html__( 'Click here', 'buildcon' ) . '</a>' ),
                'subtitle' => esc_html__( 'Since June 2016, Google requires an API key for displaying maps.', 'buildcon' ),
           ),
         )
);

/*-----------------------------------------------------------------------------------*/
/* Custom css                                                                        */
/*-----------------------------------------------------------------------------------*/

    $sections[] = array(
        'title'            => esc_html__( 'Custom CSS', 'guardmaster' ),
        'id'               => 'display-options',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(

            array(
    			'id' => $shortname . '_custom_css',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'Custom CSS', 'guardmaster' ),
    			'subtitle' => esc_html__( 'Quickly add some CSS to your theme by adding it to this block.', 'guardmaster' ),
                'mode'     => 'css',
                'theme'    => 'monokai',
                'default'  => ''
            ),

        )
    );

/*-----------------------------------------------------------------------------------*/
/* Style Settings                                                                  */
/*-----------------------------------------------------------------------------------*/

    $sections[] = array(
        'title'            => esc_html__( 'Styling', 'guardmaster' ),
        'id'               => 'body-options',
        'customizer_width' => '450px',
        'icon'   => 'el el-icon-brush',
        'fields'           => array(

            array(
                'id' => $shortname . '_live_cust_info',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => esc_html__( 'Live Customizer', 'guardmaster' ),
                'desc'   =>  sprintf( esc_html__( 'Please note that theme main colors settings can also be done using Live customizer, you can see live preview if you use live customizer. %1$s.', 'guardmaster' ), '<a href="' . esc_url( home_url('/') ) . 'wp-admin/customize.php">' . esc_html__( 'Go to Live Customer (Appearance-Customize) by clicking here', 'guardmaster' ) . '</a>' ),
            ),
           array(
                'id' => $shortname . '_main_acnt_clr',
                'type'     => 'color',
                'title'    => esc_html__( 'Main Accent Color', 'guardmaster' ),
                'subtitle' => esc_html__( 'Main accent color of the theme. Note: Some color are powered by page builder/images. Contact support if you need help.', 'guardmaster' ),
                'desc'     => esc_html__( 'Default: leave blank.', 'guardmaster' ),
                'default'  => ''
           ),

/*            array(
                'id'    => $shortname . '_hdrstyle_info1',
                'type'  => 'info',
                // 'notice' => false,
                'style' => 'info',
                'title' => esc_html__( 'Header styling', 'guardmaster' ),
                'desc'     => esc_html__( 'Header styling options can be found in Layout tab..', 'guardmaster' ),
            ),*/
            array(
                'id'    => $shortname . '_hdrstyle_info122',
                'type'  => 'info',
                // 'notice' => false,
                'style' => 'info',
                'title' => esc_html__( 'Header styling', 'guardmaster' ),
                'desc'     => esc_html__( 'Note that header styling can be changed from The next Layout(Header/Sidebar) menu.', 'guardmaster' ),
            ),


           array(
                'id' => $shortname . '_body_stng',
                'type'     => 'background',
                'output'   => array( '.mainblock' ),
                'title' => esc_html__( 'Body Background Setting', 'guardmaster' ),
                'subtitle' => esc_html__( 'You can customize body section here. Choose a color or an image and setup as per your liking.', 'guardmaster' ),
                'desc' => esc_html__( 'Note: Prebuilt pages are built on full width Visual composer grid. So this change might not be visible on them. It will be visiable on Default page template, and posts.', 'guardmaster' ),
		        'default'  => array(
		            'background-color' => '',
		        )
           ),

           array(
                'id' => $shortname . '_hdr_stng',
                'type'     => 'background',
                'output'   => array( '#header' ),
                'title' => esc_html__( 'Header Settings', 'guardmaster' ),
                'subtitle' => esc_html__( 'You can customize header area here.', 'guardmaster' ),
                //'default'   => '#FFFFFF',
           ),

            array(
                'id' => $shortname . '_tt_ft_bg',
                'type'     => 'background',
                'output'   => array( '#footer' ),
                'title'    => esc_html__( 'Footer Background', 'guardmaster' ),
                'subtitle' => esc_html__( 'Select background image or color for footer area, where widgets appear.', 'guardmaster' ),
                'desc'   =>  sprintf( esc_html__( 'Note: As by default an image is set on this area, selecting only color will not work, if you want to have color only, you might need to set image as transparent image(and then select color.). %1$s.', 'guardmaster' ), '<a href="'. esc_url(get_template_directory_uri()) .'/images/zTransparent.png" target="_blank">' . esc_html__( 'Here is a transparent image you can download and use.', 'guardmaster' ) . '</a>' ),
            ),
            array(
                'id' => $shortname . '_font_body',
                'type'     => 'typography',
                'title' => __( 'General Typography', 'guardmaster' ) ,
                'subtitle' => __( 'Change the general font. Default  color: #555555, font-size: 15px, line-height: 26px, font-family::"PT Sans",sans-serif', 'guardmaster' ) ,
                'google'   => true,
                'output' => array('body'), // An array of CSS selectors to apply this font style to dynamically
                'default'  => array(
                    'color'       => '',
                    'font-size'   => '',
                    'line-height'   => '',
                    'font-family' => '',
                    'google'      => true,
                ),
            ),
        )
        );
/*-----------------------------------------------------------------------------------*/
/* Layout Settings                                                                  */
/*-----------------------------------------------------------------------------------*/

    $sections[] = array(
        'title' => esc_html__( 'Layout (Header)', 'guardmaster' ),
        'id'               => 'layout-options',
        'desc'   => esc_html__( 'Layout Settings for Header.', 'guardmaster' ),
        'icon'   => 'el el-photo ',
        'customizer_width' => '400px',
        'fields'           => array(
/*
            array(
                'id' => $shortname . '_site_layout',
                'type'     => 'image_select',
                'title' => esc_html__( 'Main Layout', 'guardmaster' ),
                'subtitle' => esc_html__( 'Select which layout you want for your site.', 'guardmaster' ),
                'desc'     => esc_html__( 'Select default sidebar position for the website, you can override this setting on per post/page level too.', 'guardmaster' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    'layout-right-content' => array(
                        'alt' => '2 Column Left',
                        'img' => TT_TEMPTT_THEME_DIRURI. 'inc/images/2cl.png'
                    ),
                    'layout-left-content' => array(
                        'alt' => '2 Column Right',
                        'img' => TT_TEMPTT_THEME_DIRURI. 'inc/images/2cr.png'
                    ),
                ),
                'default'  => 'layout-right-content'
            ),*/
            array(
                'id' => $shortname . '_header_layout',
                'type' => 'image_select',
                'title' => esc_html__('Header Layout', 'guardmaster'),
                'subtitle' => esc_html__('Select a header layout option from the examples.', 'guardmaster'),
                'desc' => '',
                'options' => array(
                    'header-1' => array('title' => 'Overlapped header. Content overlaps behind this header.', 'img' => get_template_directory_uri().'/inc/images/gm-header-1.png'),
                    'header-2' => array('title' => 'Non-overlapped header', 'img' => get_template_directory_uri().'/inc/images/gm-header-2.png'),
                ),
                'default' => 'header-1'
            ),
            array(
                'id' => $shortname . '_hdr_phone',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable Phone in header', 'guardmaster' ),
                'desc'   => esc_html__( 'Phone number will be fetched from Social/Contact setting below.', 'guardmaster' ),
                'default'  => true,
                'required' => array( $shortname . '_header_layout', '=', 'header-1' ),
            ),

            array(
                'id' => $shortname . '_hdr_email',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable Email in header', 'guardmaster' ),
                'desc'   => esc_html__( 'Email address will be fetched from Social/Contact setting below.', 'guardmaster' ),
                'default'  => true,
                'required' => array( $shortname . '_header_layout', '=', 'header-1' ),
            ),

            array(
                'id' => $shortname . '_hdr_search',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable Search in header', 'guardmaster' ),
                'default'  => true
            ),

            array(
                'id' => $shortname . '_hdr_social',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable Social icons in header', 'guardmaster' ),
                'subtitle' => esc_html__( 'Values of social icons will be fetched from Social/Contact setting below.', 'guardmaster' ),
                'default'  => true,
                'required' => array( $shortname . '_header_layout', '=', 'header-1' ),
            ),


            array(
                'id' => $shortname . '_hdr_styling1',
                'type'     => 'background',
                'output'   => array( 'header.no-Olap #navigation' ),
                'title' => esc_html__( 'Header 1 Settings', 'guardmaster' ),
                'desc'     => esc_html__( 'Note: Color only shows up if there is no image uploaded.', 'guardmaster' ),
                'subtitle' => esc_html__( 'In header 1, page content overlaps the header area. You can define default behaviour here for header area(When there is no hero image is defined on the page, this default setting shows up). This setting can be turned off from particular page metabox too.', 'guardmaster' ),
		        'default'  => array(
		            'background-color' => '#111213',
		            //'background-image' => TT_TEMPTT_THEME_DIRURI. '/images/headerbg.jpg'
		        ),
                'required' => array( $shortname . '_header_layout', '=', 'header-1' ),
           ),

            array(
                'id' => $shortname . '_hdr_styling2',
                'type'     => 'background',
                'output'   => array( '.header-2.no-Olap #navigation' ),
                'title' => esc_html__( 'Header 2 Settings', 'guardmaster' ),
                'desc'     => esc_html__( 'Note: Color only shows up if there is no image uploaded.', 'guardmaster' ),
                'subtitle' => esc_html__( 'Change look of header 2.', 'guardmaster' ),
		        'default'  => array(
		            'background-color' => '#f8f8f8',
		            //'background-image' => TT_TEMPTT_THEME_DIRURI. '/images/headerbg.jpg'
		        ),
                'required' => array( $shortname . '_header_layout', '=', 'header-2' ),
           ),

        )
    );

/*-----------------------------------------------------------------------------------*/
/* Footer Settings                                                                  */
/*-----------------------------------------------------------------------------------*/

    $sections[] = array(
        'title' => esc_html__( 'Footer Customization', 'guardmaster' ),
        'id'               => 'footer-settings1',
        'customizer_width' => '450px',
        'fields'           => array(


           array(
                'id' => $shortname . '_extreme_footer_notice',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => esc_html__( 'Footer area customization', 'guardmaster' ),
                'desc'   => esc_html__( 'Customize footer area here. Note: Background colors for footer areas can be changed from Styling tab.', 'guardmaster' )
           ),

           array(
                'id' => $shortname . '_footer_sidebars',
                'type'     => 'image_select',
                'title' => esc_html__( 'Footer Widget Areas', 'guardmaster' ),
                'subtitle' => esc_html__( 'Select how many footer widget areas you want to display.', 'guardmaster' ),
                'desc'     => esc_html__( 'Select default sidebar position for the website, you can override this setting on per post/page level too.', 'guardmaster' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    '0' => array(
                        'img' => TT_TEMPTT_THEME_DIRURI .'inc/images/footer-widgets-0.png',
                    ),
                    '1' => array(
                        'img' => TT_TEMPTT_THEME_DIRURI .'inc/images/footer-widgets-1.png',
                    ),
                    '2' => array(
                        'img' => TT_TEMPTT_THEME_DIRURI .'inc/images/footer-widgets-2.png',
                    ),
                    '3' => array(
                        'img' => TT_TEMPTT_THEME_DIRURI .'inc/images/footer-widgets-3.png',
                    ),
                    '4' => array(
                        'img' => TT_TEMPTT_THEME_DIRURI .'inc/images/footer-widgets-4.png',
                    ),
                ),
                'default'  => '4'
           ),

           array(
                'id' => $shortname . '_footer_left',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable Footer copyright text', 'guardmaster' ),
                'subtitle' => esc_html__( 'Activate to add the custom text below to the theme footer.', 'guardmaster' ),
                'desc'   => esc_html__( 'If turned off, default text will appear.', 'guardmaster' ),
                'default'  => false
            ),

           array(
                'id' => $shortname . '_footer_left_text',
                'type'     => 'textarea',
                'title' => esc_html__( 'Custom Footer Text', 'guardmaster' ),
                'subtitle' => esc_html__( 'Custom HTML and Text that will appear in the extreme footer of your website.', 'guardmaster' ),
                'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
                'default'  => ''
           ),

        )
    );

/*-----------------------------------------------------------------------------------*/
/* Social Settings                                                                  */
/*-----------------------------------------------------------------------------------*/

    $sections[] = array(
        'title' => esc_html__( 'Social / Contact Settings', 'guardmaster' ),
        'id'               => 'connect-settings',
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id' => $shortname . '_multiphone',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable Multiple Phone', 'guardmaster' ),
                'subtitle' => esc_html__( 'If you have just single phone number, keep it disabled. It will then link to that single phone number. If you have multiple phone numbers, you can enable this. Enabling this option will enable the HTML on below Phone number field so that you can put html code to link the phone numbers as you desire.', 'guardmaster' ),
                 'default'  => false
            ),
            array(
                'id' => $shortname . '_contact_phone',
                'type'     => 'text',
                'title' => esc_html__( 'Phone Number', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enter phone number, it is displayed on the header, if enabled in Layout above.', 'guardmaster' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_contact_email',
                'type'     => 'text',
                'title' => esc_html__( 'Email', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enter email number, it is displayed on the header, if enabled in Layout above.', 'guardmaster' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_contact_address',
                'type'     => 'textarea',
                'title' => esc_html__( 'Address', 'guardmaster' ),
                'subtitle' => esc_html__( 'To be used in the widget in footer.', 'guardmaster' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_connect_rss',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable RSS', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enable the subscribe and RSS icon.', 'guardmaster' ),
                 'default'  => true
            ),
            array(
                'id' => $shortname . '_connect_twitter',
                'type'     => 'text',
                'title' => esc_html__( 'Twitter URL', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your %1$s URL e.g. http://www.twitter.com/templatation', 'guardmaster' ), '<a href="http://www.twitter.com/">'.esc_html__( 'Twitter', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_connect_facebook',
                'type'     => 'text',
                'title' => esc_html__( 'Facebook URL', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your %1$s URL e.g. http://www.facebook.com/templatation', 'guardmaster' ), '<a href="http://www.facebook.com/">'.esc_html__( 'Facebook', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_connect_youtube',
                'type'     => 'text',
                'title' => esc_html__( 'YouTube URL', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your %1$s URL e.g. http://www.youtube.com/templatation', 'guardmaster' ), '<a href="http://www.youtube.com/">'.esc_html__( 'YouTube', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_connect_flickr',
                'type'     => 'text',
                'title' => esc_html__( 'Flickr URL', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your %1$s URL e.g. http://www.flickr.com/templatation', 'guardmaster' ), '<a href="http://www.flickr.com/">'.esc_html__( 'Flickr', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_connect_linkedin',
                'type'     => 'text',
                'title' => esc_html__( 'LinkedIn URL', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your %1$s URL e.g. http://www.linkedin.com/in/templatation', 'guardmaster' ), '<a href="http://www.www.linkedin.com.com/">'.esc_html__( 'LinkedIn', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_connect_pinterest',
                'type'     => 'text',
                'title' => esc_html__( 'Pinterest URL', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your %1$s URL e.g. http://www.pinterest.com/templatation', 'guardmaster' ), '<a href="http://www.pinterest.com/">'.esc_html__( 'Pinterest', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_connect_instagram',
                'type'     => 'text',
                'title' => esc_html__( 'Instagram URL', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your %1$s URL e.g. http://www.instagram.com/templatation', 'guardmaster' ), '<a href="http://www.instagram.com/">'.esc_html__( 'Instagram', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_connect_googleplus',
                'type'     => 'text',
                'title' => esc_html__( 'Google+ URL', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your %1$s URL e.g. https://plus.google.com/104560124403688998123/', 'guardmaster' ), '<a href="http://plus.google.com/">'.esc_html__( 'Google+', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_open_social_newtab',
                'type'     => 'switch',
                'title' => esc_html__( 'Open in New tab', 'guardmaster' ),
                'subtitle' => esc_html__( 'If this is turned on, the social icon links open in new tab instead of same window.', 'guardmaster' ),
                 'default'  => false
            ),
        )
    );

/*-----------------------------------------------------------------------------------*/
/* Contact Settings                                                                  */
/*-----------------------------------------------------------------------------------*/
/*
    $sections[] = array(
        'title' => esc_html__( 'Contact Page', 'guardmaster' ),
        'id'     => 'contact-setting',
        'desc'   => esc_html__( 'Contact Settings.', 'guardmaster' ),
        'icon'   => 'el el-envelope',
        'customizer_width' => '400px',
    );

    $sections[] = array(
        'title' => esc_html__( 'Contact Information', 'guardmaster' ),
        'id'               => 'contact-settings1',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(
*/
/*            array(
                'id' => $shortname.'_contact_panel',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable Contact Information Panel', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enable the contact informal panel', 'guardmaster' ),
                'default'  => false
            ),*/
/*            array(
                'id' => $shortname . '_contact_title',
                'type'     => 'text',
                'title' => esc_html__( 'Location Name', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enter the location name. Example: London Office', 'guardmaster' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_contact_intro',
                'type'     => 'textarea',
                'title' => esc_html__( 'Location Intro', 'guardmaster' ),
                'subtitle' => esc_html__( "Enter 4-5 lines for this office location, if you have any.", 'guardmaster' ),
                'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
                'default'  => '',

            ),
            array(
                'id' => $shortname . '_contact_address',
                'type'     => 'textarea',
                'title' => esc_html__( 'Location Address', 'guardmaster' ),
                'subtitle' => esc_html__( "Enter your company's address", 'guardmaster' ),
                'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_contact_fax',
                'type'     => 'text',
                'title' => esc_html__( 'Fax', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enter your fax number', 'guardmaster' ),
                'default'  => '',

            ),
            array(
                'id' => $shortname . '_contact_number',
                'type'     => 'text',
                'title' => esc_html__( 'Telephone', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enter your telephone number', 'guardmaster' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname.'_contactform_email',
                'type'     => 'text',
                'title' => esc_html__( 'Contact Form E-Mail', 'guardmaster' ),
                'subtitle' => esc_html__( "Enter your E-mail address to use on the 'Contact Form' page Template.", 'guardmaster' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_contact_twitter',
                'type'     => 'text',
                'title' => esc_html__( 'Your Twitter Shortcode', 'guardmaster' ),
                'subtitle' => esc_html__( 'You can use any plugin for twitter needs and enter the supplied shortcode to display tweets here. Enter shortcode eg : [twitter-widget username="yourTwitterUsername"]. Note: This will not work if shortcode is not valid. Contact support for more info.', 'guardmaster' ),
                'default'  => '',
            ),
        )
    );

    $sections[] = array(
        'title' => esc_html__( 'Maps', 'guardmaster' ),
        'id'               => 'map-settings1',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id' => $shortname . '_contactform_map_coords',
                'type'     => 'text',
                'title' => esc_html__( 'Contact Form Google Maps Coordinates', 'guardmaster' ),
                'subtitle' => sprintf( esc_html__( 'Enter your Google Map coordinates to display a map on the Contact Form page template. You can get coordinates details from %1$s', 'guardmaster' ), '<a href="http://www.mapcoordinates.net/en/" target="_blank">'.esc_html__( 'Here', 'guardmaster' ).'</a>' ),
                'default'  => '',
            ),
            array(
                'id' => $shortname . '_maps_scroll',
                'type'     => 'switch',
                'title' => esc_html__( 'Disable Mousescroll', 'guardmaster' ),
                'subtitle' => esc_html__( 'Turn off the mouse scroll action for all the Google Maps on the site. This could improve usability on your site.', 'guardmaster' ),
                'default'  => true
            ),
            array(
                'id' => $shortname . '_maps_single_height',
                'type'     => 'text',
                'title' => esc_html__( 'Map Height', 'guardmaster' ),
                'subtitle' => esc_html__( 'Height in pixels for the maps displayed on Single.php pages.', 'guardmaster' ),
                'default' => '500',
            ),
            array(
                'id' => $shortname . '_maps_default_mapzoom',
                'type'          => 'slider',
                'title' => esc_html__( 'Default Map Zoom Level', 'guardmaster' ),
                'subtitle' => esc_html__( 'Set this to adjust the default in the post & page edit backend.', 'guardmaster' ),
                'default'       => 9,
                'min'           => 1,
                'step'          => 1,
                'max'           => 20,
                'display_value' => 'label'
            ),
            array(
                'id' => $shortname . '_maps_default_maptype',
                'type'     => 'select',
                'title' => esc_html__( 'Default Map Type', 'guardmaster' ),
                'subtitle' => esc_html__( 'Set this to the default rendered in the post backend.', 'guardmaster' ),
                'options' => array( 'G_NORMAL_MAP' => esc_html__( 'Normal', 'guardmaster' ), 'G_SATELLITE_MAP' => esc_html__( 'Satellite', 'guardmaster' ),'G_HYBRID_MAP' => esc_html__( 'Hybrid', 'guardmaster' ), 'G_PHYSICAL_MAP' => esc_html__( 'Terrain', 'guardmaster' ) ),
                'default' => 'G_NORMAL_MAP',
            ),
            array(
                'id' => $shortname . '_maps_callout_text',
                'type'     => 'textarea',
                'title' => esc_html__( 'Map Callout Text', 'guardmaster' ),
                'subtitle' => esc_html__( 'Text or HTML that will be output when you click on the map marker for your location.', 'guardmaster' ),
                'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
                'default'  => ''
            ),
        )
    );
*/
/*-----------------------------------------------------------------------------------*/
/* Included Plugins                                                                 */
/*-----------------------------------------------------------------------------------*/
/*
   $sections[] = array(
        'title' => esc_html__( 'Included Plugins', 'guardmaster' ),
        'id'               => 'included-plugins',
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id' => $shortname . '_inc_plugins',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => esc_html__( 'Included Plugins', 'guardmaster' ),
                'desc'   => sprintf( esc_html__( 'The theme requires/recommends some plugin to function properly. Theme also includes some premium plugins with your purchase, to manage plugins, please go to Appearance -> Install plugins. Note: The menu only appears if all required plugins are not activated. So not to worry if you do not find that menu link.', 'guardmaster' ), '<a href="' . esc_url( home_url('/') ) . 'wp-admin/themes.php?page=tgmpa-install-plugins">'.esc_html__( 'Click Here', 'guardmaster' ).'</a>' ),
            ),
        )
    );

   $sections[] = array(
        'title'  => esc_html__( 'Theme Updates', 'guardmaster' ),
        'id'     => 'theme-update',
        'icon'   => 'el el-refresh',
        'customizer_width' => '450px',
        'fields' => array(

            array(
                'id' => $shortname . '_theme_update',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title' => esc_html__( 'Theme Update', 'guardmaster' ),
                'desc'   => sprintf( esc_html__( 'The theme is regularly updated with new features and other bug fixes and improvement. You can enable updates from here and you will be able to automatically update theme to latest version when its available without having to download from Themeforest. Note if you modified any theme files, it will be overwritten with update. Again, make sure to keep regular backups. You can use awesome free plugin named %1$s  to create backups periodically(automatically) without hassles.', 'guardmaster' ), '<a href="https://wordpress.org/plugins/updraftplus/">'.esc_html__( 'UpdraftPlus', 'guardmaster' ).'</a>' ),
            ),*/
/*            array(
                'id' => $shortname . '_theme_update1',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'alert',
                'title' => esc_html__( 'No license key ?', 'guardmaster' ),
                'desc'   => sprintf( esc_html__( 'In case you do not have license key, its highly recommended to buy theme and get license key %1$s . It took over 3 months to build this theme and its still being worked on. Please support the development if you like it and use it. It doesnt cost much. :). <br />Precious customers and  fans, if you are very happy and want to support us even more, you can purchase again as well.', 'guardmaster' ), '<a href="http://themeforest.net/item/justshop-cake-bakery-wordpress-theme/4747148?ref=templatation">'.esc_html__( 'From Here', 'guardmaster' ).'</a>' ),
            ),*/
 /*           array(
                'id' => $shortname . '_lic_key',
                'type'     => 'text',
                'title'    => esc_html__( 'Themeforest Purchase Key', 'guardmaster' ),
                'subtitle'    => esc_html__( 'Once you enter valid purchase code and save. You can update theme easily in future with 1 click by going to Dashboard > Updates. No more downloading from Themeforest required.', 'guardmaster' ),
                'desc' => sprintf( esc_html__( 'Enter your purchase key here, if you are unsure how to find it, please check video below. <p>e.g. xxxx-xxxx-xxxx-xxxx-xxxx</p><p></p> %1$s', 'guardmaster' ), '<a href="https://www.youtube.com/watch?v=Sv5VaORIS3A">https://www.youtube.com/watch?v=Sv5VaORIS3A</a>' ),
                'default'  => '',
            ),
        )
    );
*/
/*-----------------------------------------------------------------------------------*/
/* CPT Settings                                                                      */
/*-----------------------------------------------------------------------------------*/
// Display options to manage CPT, as required in this theme.
$tt_temptt_cpt = '';
$tt_temptt_cpt = get_option('tt_temptt_components');
if( is_array( $tt_temptt_cpt )) {
    /*$sections[] = array(
        'title'            => esc_html__( 'Custom Post Types', 'guardmaster' ),
        'id'               => 'theme-cpts',
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id'     => $shortname . '_inc_cpt',
                'type'   => 'info',
                'notice' => false,
                'style'  => 'info',
                'title'  => esc_html__( 'Custom Post Types', 'guardmaster' ),
                'desc'   => esc_html__( 'The theme adds some post types to manage content like testimonials, portfolios etc. You can disable the Post types that you do not need. You can also change the permalink structure for those custom post types here, for example, if you would like portfolio item url to be like http://yoursite.com/portfolio-item/item or http://yoursite.com/my-work/item. If you are not sure what it all means, its ok to skip this section totally.', 'guardmaster' ),
            ),
            array(
                'id'    => $shortname . '_inc_cpt1',
                'type'  => 'info',
                // 'notice' => false,
                'style' => 'info',
                'title' => esc_html__( 'Important !', 'guardmaster' ),
                'desc'  => esc_html__( 'After changing any settings on this section or its subsections, you need to re-save the Settings → Permalinks page. Otherwise you will get 404 errors.', 'guardmaster' ),
            ),
            array(
                'id' => $shortname.'_cpt_with_front',
                'type'     => 'switch',
                'title' => esc_html__( 'Enable With Front', 'guardmaster' ),
                'subtitle' => esc_html__( 'Recommended: Off', 'guardmaster' ),
                'desc' => esc_html__( 'Prepend the permalink structure with the front base. ( example: if your permalink structure is /blog/, then your links will be: With-Front disabled -> /portfolio-item/, With-Front enabled -> /blog/portfolio-item/ ).', 'guardmaster' ),
                'default'  => false
            ),
        )
    );*/

// Portfolio CPT
/*if( ! empty( $tt_temptt_cpt['portfolio_cpt'] ) ) {
    $sections[] = array(
        'title'            => esc_html__( 'Portfolio', 'guardmaster' ),
        'id'               => 'portfolio-cpt-setting',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id'    => $shortname . '_inc_portfolio',
                'type'  => 'info',
                // 'notice' => false,
                'style' => 'info',
                'title' => esc_html__( 'Important !', 'guardmaster' ),
                'desc'  => esc_html__( 'After changing any settings on this section or its subsections, you need to re-save the Settings → Permalinks page. Otherwise you will get 404 errors.', 'guardmaster' ),
            ),
            array(
                'id'       => $shortname . '_portfolio_cpt',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Portfolio', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enable the Portfolio Custom Post type.', 'guardmaster' ),
                'default'  => $tt_temptt_cpt['portfolio_cpt'] // Hint: will be 1 always.
            ),
            array(
                'id'      => $shortname . '_portfolio_cpt_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Portfolio Slug', 'guardmaster' ),
                'default' => 'portfolio-item',
            ),
            array(
                'id'      => $shortname . '_portfolio_cpt_cat_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Portfolio Category Slug', 'guardmaster' ),
                'default' => 'portfolio-cats',
            ),
            array(
                'id'       => $shortname . '_portfolio_cpt_single',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Single Page', 'guardmaster' ),
                'desc' => esc_html__( 'Do you want singular item page for this Custom post type. eg: It makes sense to have a Portfolio item single page with more details about that item. But it makes less sense to have single item page for testimonials.', 'guardmaster' ),
                'default'  => true
            ),
        )
    );
}*/
// Testimonial CPT
/*if( ! empty( $tt_temptt_cpt['testimonial_cpt'] ) ) {
    $sections[] = array(
        'title'            => esc_html__( 'Testimonial', 'guardmaster' ),
        'id'               => 'testimonial-cpt-setting',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id'    => $shortname . '_inc_testimonial',
                'type'  => 'info',
                // 'notice' => false,
                'style' => 'info',
                'title' => esc_html__( 'Important !', 'guardmaster' ),
                'desc'  => esc_html__( 'After changing any settings on this section or its subsections, you need to re-save the Settings → Permalinks page. Otherwise you will get 404 errors.', 'guardmaster' ),
            ),
            array(
                'id'       => $shortname . '_testimonial_cpt',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Testimonial', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enable the Testimonial Custom Post type.', 'guardmaster' ),
                'default'  => $tt_temptt_cpt['testimonial_cpt'] // Hint: will be 1 always.
            ),
            array(
                'id'      => $shortname . '_testimonial_cpt_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Testimonial Slug', 'guardmaster' ),
                'default' => 'testimonial-item',
            ),
            array(
                'id'      => $shortname . '_testimonial_cpt_cat_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Testimonial Category Slug', 'guardmaster' ),
                'default' => 'testimonial-cats',
            ),
            array(
                'id'       => $shortname . '_testimonial_cpt_single',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Single Page', 'guardmaster' ),
                'desc' => esc_html__( 'Do you want singular item page for this Custom post type. eg: It makes sense to have a Portfolio item single page with more details about that item. But it makes less sense to have single item page for testimonials.', 'guardmaster' ),
                'default'  => false
            ),
        )
    );
}*/
// Team CPT
/*if( ! empty( $tt_temptt_cpt['team_cpt'] ) ) {
    $sections[] = array(
        'title'            => esc_html__( 'Team', 'guardmaster' ),
        'id'               => 'team-cpt-setting',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id'    => $shortname . '_inc_team',
                'type'  => 'info',
                // 'notice' => false,
                'style' => 'info',
                'title' => esc_html__( 'Important !', 'guardmaster' ),
                'desc'  => esc_html__( 'After changing any settings on this section or its subsections, you need to re-save the Settings → Permalinks page. Otherwise you will get 404 errors.', 'guardmaster' ),
            ),
            array(
                'id'       => $shortname . '_team_cpt',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Team', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enable the Team Custom Post type.', 'guardmaster' ),
                'default'  => $tt_temptt_cpt['team_cpt'] // Hint: will be 1 always.
            ),
            array(
                'id'      => $shortname . '_team_cpt_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Team Slug', 'guardmaster' ),
                'default' => 'team-item',
            ),
            array(
                'id'      => $shortname . '_team_cpt_cat_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Team Category Slug', 'guardmaster' ),
                'default' => 'team-cats',
            ),
            array(
                'id'       => $shortname . '_team_cpt_single',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Single Page', 'guardmaster' ),
                'desc' => esc_html__( 'Do you want singular item page for this Custom post type. eg: It makes sense to have a Portfolio item single page with more details about that item. But it makes less sense to have single item page for testimonials.', 'guardmaster' ),
                'default'  => true
            ),
        )
    );
}*/
// Client CPT
/*if( ! empty( $tt_temptt_cpt['client_cpt'] ) ) {
    $sections[] = array(
        'title'            => esc_html__( 'Client', 'guardmaster' ),
        'id'               => 'client-cpt-setting',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id'    => $shortname . '_inc_client',
                'type'  => 'info',
                // 'notice' => false,
                'style' => 'info',
                'title' => esc_html__( 'Important !', 'guardmaster' ),
                'desc'  => esc_html__( 'After changing any settings on this section or its subsections, you need to re-save the Settings → Permalinks page. Otherwise you will get 404 errors.', 'guardmaster' ),
            ),
            array(
                'id'       => $shortname . '_client_cpt',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Client', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enable the Client Custom Post type.', 'guardmaster' ),
                'default'  => $tt_temptt_cpt['client_cpt'] // Hint: will be 1 always.
            ),
            array(
                'id'      => $shortname . '_client_cpt_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Client Slug', 'guardmaster' ),
                'default' => 'client-item',
            ),
            array(
                'id'      => $shortname . '_client_cpt_cat_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Client Category Slug', 'guardmaster' ),
                'default' => 'client-cats',
            ),
            array(
                'id'       => $shortname . '_client_cpt_single',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Single Page', 'guardmaster' ),
                'desc' => esc_html__( 'Do you want singular item page for this Custom post type. eg: It makes sense to have a Portfolio item single page with more details about that item. But it makes less sense to have single item page for testimonials.', 'guardmaster' ),
                'default'  => false
            ),
        )
    );
}*/
// Project CPT
/*if( ! empty( $tt_temptt_cpt['project_cpt'] ) ) {
    $sections[] = array(
        'title'            => esc_html__( 'Project', 'guardmaster' ),
        'id'               => 'project-cpt-setting',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id'    => $shortname . '_inc_project',
                'type'  => 'info',
                // 'notice' => false,
                'style' => 'info',
                'title' => esc_html__( 'Important !', 'guardmaster' ),
                'desc'  => esc_html__( 'After changing any settings on this section or its subsections, you need to re-save the Settings → Permalinks page. Otherwise you will get 404 errors.', 'guardmaster' ),
            ),
            array(
                'id'       => $shortname . '_project_cpt',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Project', 'guardmaster' ),
                'subtitle' => esc_html__( 'Enable the Project Custom Post type.', 'guardmaster' ),
                'default'  => $tt_temptt_cpt['project_cpt'] // Hint: will be 1 always.
            ),
            array(
                'id'      => $shortname . '_project_cpt_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Project Slug', 'guardmaster' ),
                'default' => 'project-item',
            ),
            array(
                'id'      => $shortname . '_project_cpt_cat_slug',
                'type'    => 'text',
                'title'   => esc_html__( 'Project Category Slug', 'guardmaster' ),
                'default' => 'project-cats',
            ),
            array(
                'id'       => $shortname . '_project_cpt_single',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Single Page', 'guardmaster' ),
                'desc' => esc_html__( 'Do you want singular item page for this Custom post type. eg: It makes sense to have a Portfolio item single page with more details about that item. But it makes less sense to have single item page for testimonials.', 'guardmaster' ),
                'default'  => true
            ),
        )
    );
}*/
} // is_array($tt_temptt_cpt)


    return $sections;

    }
}