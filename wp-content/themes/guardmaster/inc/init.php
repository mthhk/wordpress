<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------*/
/* Load the files for theme, with support for overriding the widget via a child theme.
/*-----------------------------------------------------------------------------------*/

include_once get_template_directory(). '/inc/theme-essentials.php';   // Theme Essentials
include_once get_template_directory(). '/inc/theme-options.php';      // Theme options
include_once get_template_directory(). '/inc/theme-functions.php';    // Custom theme functions
include_once get_template_directory(). '/inc/theme-metabox.php';      // Theme Metaboxes
include_once get_template_directory(). '/inc/theme-widgets.php';      // Theme widgets
include_once get_template_directory(). '/inc/theme-comments.php';     // Theme comments


/**
 * Initialize theme required features & components.
 * This is the base setting for required CPTs, based on these settings customer sees options to disable/rename rewrite for cpts in themeoptions.
 */
if(!( function_exists('tt_temptt_theme_components') )){

	function tt_temptt_theme_components() {

		$theme_components = array(
			'portfolio_cpt'             => '0',
			'team_cpt'                  => '0',
			'client_cpt'                => '0',
			'testimonial_cpt'           => '0',
			'project_cpt'               => '0',
			'metaboxes'                 => '1',
			'theme_options'             => '1',
			'common_shortcodes'         => '1',
			'integrate_VC'              => '1',
		);
		// Let filter modify it
		$theme_components = apply_filters( 'tt_theme_components', $theme_components );
		update_option('tt_temptt_components', $theme_components);
	}

	// only trigger on first install
	global $pagenow;
	if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' || is_admin() && isset( $_GET['theme'] ) && $pagenow == 'customize.php' ){
		add_action( 'init', 'tt_temptt_theme_components', 1 );
		add_action( 'init', 'tt_temptt_user_manage_cpt', 2 ); // Trigger first run of this fn.
	}
}

/**
 * Let user disable CPT as per his needs.
 */
if(!( function_exists('tt_temptt_user_manage_cpt') )){

	function tt_temptt_user_manage_cpt() {

		// Fetch from DB.
		$theme_components = get_option('tt_temptt_components');
		if( !$theme_components ) return;

		// User settings.
		$theme_user_cpts = array(
			'portfolio_cpt'             => tt_temptt_get_option( 'portfolio_cpt', $theme_components['portfolio_cpt'] ),
			'testimonial_cpt'           => tt_temptt_get_option( 'testimonial_cpt', $theme_components['testimonial_cpt'] ),
			'team_cpt'                  => tt_temptt_get_option( 'team_cpt', $theme_components['team_cpt'] ),
			'client_cpt'                => tt_temptt_get_option( 'client_cpt', $theme_components['client_cpt'] ),
			'project_cpt'               => tt_temptt_get_option( 'project_cpt', $theme_components['project_cpt'] ),
		);

		// Overwrite theme defaults with new user settings.
		$new_theme_components = wp_parse_args( $theme_user_cpts, $theme_components );

		// Save
		update_option('tt_temptt_components_user', $new_theme_components);

	}

	// only trigger on permalink page
	global $pagenow;
	if ( is_admin() && $pagenow == 'options-permalink.php' ){
		add_action( 'init', 'tt_temptt_user_manage_cpt', 2 );
	}
}

/**
 * TGM class for plugin includes.
 */
if( is_admin() ){
	if (!( class_exists( 'TGM_Plugin_Activation' ) ))
		get_template_part( 'inc/tgm-activation/tt-plugins' );
}

/**
 * Woocommerce integration
 */
if ( class_exists( 'woocommerce' ) ) {
	get_template_part( 'inc/integrations/woocommerce/theme-woocommerce' );
}

/**
 * VC integration
 */
if ( function_exists( 'vc_set_as_theme' ) ) {
	get_template_part( 'inc/integrations/visual-composer/vc-init' );
}


/**
 * Content Width
 * ( WP requires it and LC uses is to figure out the wrapper width )
 */

if ( ! isset( $content_width ) )
	$content_width = 1140;


/**
 * Basic Theme Setup
 */

if ( ! function_exists( 'tt_gmaster_theme_setup' ) ) {

	function tt_gmaster_theme_setup() {

		// Load the translations
		load_theme_textdomain( 'guardmaster', get_template_directory() . '/lang' );

		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		// Add admin editor style.
		add_theme_support('editor-styles');
		add_editor_style( array( 'assets/css/editor-style.css', tt_gmaster_g_fonts() ) );
		add_theme_support( 'wp-block-styles' );

		// Add custom background support.
		add_theme_support( 'custom-background', array( 'default-color' => 'f7f7f7' ) );

		// Enable Post Thumbnails ( Featured Image )
		add_theme_support( 'post-thumbnails' );

		// Title tag support
		add_theme_support( 'title-tag' );

		// Register Navigation Menus
		register_nav_menus( array(
			'primary-menu' => esc_html__( 'Primary Menu', 'guardmaster' ),
		) );

		// Enable support for HTML5 markup.
		add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
		add_image_size( 'tt-blog', 700, 400, true );
	}

} add_action( 'after_setup_theme', 'tt_gmaster_theme_setup' );

