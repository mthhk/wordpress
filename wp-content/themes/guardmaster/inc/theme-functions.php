<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------
 *
 * This file contains required functions for the theme.
 * @templatation.com
 *
-----------------------------------------------------------------------------------*/

add_action( 'wp_head', 'tt_gmaster_wp_head', 9 );
if ( ! function_exists( 'tt_gmaster_wp_head' ) ) {
/**
 * Output the default ttFramework "head" markup in the "head" section.
 * @since  2.0.0
 * @return void
 */
function tt_gmaster_wp_head() {
	do_action( 'tt_gmaster_wp_head_before' );

	// Output custom favicon icons
	if ( function_exists( 'tt_gmaster_custom_favicon' ) && ! function_exists( 'wp_site_icon' ) )
		tt_gmaster_custom_favicon();
	// Output custom styles to Head.
	if ( function_exists( 'tt_gmaster_custom_styling' ) )
		tt_gmaster_custom_styling();

	do_action( 'tt_gmaster_wp_head_after' );
} // End tt_gmaster_wp_head()
}

/*-----------------------------------------------------------------------------------*/
/* tt_get_dynamic_value() */
/* Replace values in a provided array with theme options, if available. */
/*
/* $settings array should resemble: $settings = array( 'theme_option_without_tt' => 'default_value' );
/*
/* @since 4.4.4 */
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'tt_temptt_opt_values' )) {
	function tt_temptt_opt_values( $settings ) {
		global $tt_temptt_opt;

		if ( is_array( $tt_temptt_opt ) ) {
			foreach ( $settings as $k => $v ) {

				if ( is_array( $v ) ) {
					foreach ( $v as $k1 => $v1 ) {
						if ( isset( $tt_temptt_opt[ 'tt_' . $k ][ $k1 ] ) && ( $tt_temptt_opt[ 'tt_' . $k ][ $k1 ] != '' ) ) {
							$settings[ $k ][ $k1 ] = $tt_temptt_opt[ 'tt_' . $k ][ $k1 ];
						}
					}
				} else {
					if ( isset( $tt_temptt_opt[ 'tt_' . $k ] ) && ( $tt_temptt_opt[ 'tt_' . $k ] != '' ) ) {
						$settings[ $k ] = $tt_temptt_opt[ 'tt_' . $k ];
					}
				}
			}
		}

		return $settings;
	} // End tt_temptt_opt_values()
}


/*-----------------------------------------------------------------------------------*/
/* tt_temptt_get_option() */
/* Replace values in a provided variable with theme options, if available. */
/*
 * field id should be without tt_
 */
/* TT @since 6.0 */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_temptt_get_option' ) ) {
	function tt_temptt_get_option( $var, $default = false ) {
		global $tt_temptt_opt;

		if ( isset( $tt_temptt_opt[ 'tt_' . $var ] ) ) {
			$var = $tt_temptt_opt[ 'tt_' . $var ];
		} else {
			$var = $default;
		}

		return $var;
	} // End tt_temptt_get_option()
}

/*-----------------------------------------------------------------------------------*/
/* Render sidebar, if we need to. */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_temptt_get_sidebar' ) ) {
	function tt_temptt_get_sidebar() {
		global $wp_query;

		ob_start(); ?>
					<div class="col-lg-4 col-md-4 sidebar">
						<?php get_sidebar(); ?>
					</div>
		<?php

		return ob_get_clean();
	} // End tt_temptt_get_sidebar()
}

/*-----------------------------------------------------------------------------------*/
/* Get Sidebar positions                                                             */
/*-----------------------------------------------------------------------------------*/
// returns push pull for sidebar and content laying. pass content/sidebar to fetch relevant value.

if (!function_exists( 'tt_temptt_sidebar_pos')) {
function tt_temptt_sidebar_pos( $value='' ){

	 return 'col-lg-8 col-md-8';
}
}

/*-----------------------------------------------------------------------------------*/
/* Post/page title
/*-----------------------------------------------------------------------------------*/
// returns title based on the requirement.
// if simple is passed in return, it just returns the default title. Used in pages like single.
if (!function_exists( 'tt_temptt_post_title')) {
function tt_temptt_post_title( $tag='', $return='' ){
	if( empty($tag)) $tag = 'h3';
	if( $return == 'simple' )
		 return the_title( sprintf( '<'.$tag.' class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></'.$tag.'>' );

	global $wp_query;
	$tt_post_id = $single_item_layout = $tt_lay_content = $tt_lay_sidebar = $single_data2 = '';

		if ( ! is_404() && ! is_search() ) {
			if ( ! empty( $wp_query->post->ID ) ) {
				$tt_post_id = $wp_query->post->ID;
			}
		}
		if ( ! empty( $tt_post_id ) ) {
			$single_data2 = get_post_meta( $tt_post_id, '_tt_meta_page_opt', true );
		}
		if ( is_array( $single_data2 ) ) {
			if ( !empty( $single_data2['_hide_title_display'] ) ) {
				return '';
			}
		}

	return '<div class="single-blog-title"><'.$tag. ' class="ml-title">'. get_the_title() .'</'.$tag.'> </div>';
}
}

/*-----------------------------------------------------------------------------------*/
/* Add a class to body_class if fullwidth slider to appear. */
/*-----------------------------------------------------------------------------------*/

add_filter( 'body_class','tt_gmaster_body_class', 10 );// Add layout to body_class output

if ( ! function_exists( 'tt_gmaster_body_class' ) ) {
function tt_gmaster_body_class( $classes ) {

	global $tt_temptt_opt, $wp_query;
	$current_page_template = $single_data2 = $tt_post_id = $single_enable_headline = '';

	//setting up defaults.
	$settings6 = tt_temptt_opt_values( array(  'enable_sticky' => '1',
												'img_corner'    => '1',
											 ) );

	if ( !is_404() && !is_search() ) {
		if ( ! empty( $wp_query->post->ID ) ) {
			$tt_post_id = $wp_query->post->ID;
		}
	}
	$single_data2 = get_post_meta( $tt_post_id, '_tt_meta_page_opt', true );
	if( is_array($single_data2) ) {
		if ( isset( $single_data2['_single_enable_headline'] ) ) $single_enable_headline = $single_data2['_single_enable_headline'];
		if ( isset( $single_data2['_single_enable_overlap'] ) ) $single_enable_overlap = $single_data2['_single_enable_overlap'];
		if ( isset( $single_data2['_single_enable_bpadding'] ) ) $single_enable_bpadding = $single_data2['_single_enable_bpadding'];
		if ( isset( $single_data2['_single_enable_tpadding'] ) ) $single_enable_tpadding = $single_data2['_single_enable_tpadding'];
	}
	// setting defaults
	if ( !is_page() && (!isset($single_enable_headline) || empty($single_enable_headline)) ) $single_enable_headline = false;
	if ( !isset($single_enable_overlap) ) $single_enable_overlap = false;
	if ( !isset($single_enable_bpadding) ) $single_enable_bpadding = true;
	if ( !isset($single_enable_tpadding) ) $single_enable_tpadding = true;

	// fetching which page template is being used to render current post/page.
	if ( !empty($tt_post_id) ) { $current_page_template = get_post_meta($tt_post_id, '_wp_page_template', true); }
	if ( is_singular()) { $classes[] = 'tt-single'; } // add a custom class if single item is displayed except blog template is being used, for styling purpose.
	if ( $single_enable_headline ) { $classes[] = 'hdline_set'; } else { $classes[] = 'hdline_notset'; }
	( $settings6['enable_sticky'] ) ? $classes[] = 'header-sticky' : $classes[] = 'header-nosticky';
	if ( $single_enable_overlap ) { $classes[] = 'hdr-overlap'; } else { $classes[] = 'hdr-no-olap'; }
	if ( ! $single_enable_bpadding && ! is_page('blog') ) { $classes[] = 'no-bpadd'; }
	if ( ! $single_enable_tpadding && ! is_page('blog') ) { $classes[] = 'no-tpadd'; }
	$classes[] = tt_temptt_get_option( 'header_layout', 'header-1' );
	$classes[] = 'no-ttfmwrk'; // this class will be removed by templatation framework plugin when its active.
	return $classes;
  }
}

/*-----------------------------------------------------------------------------------*/
/* Load custom logo. */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_gmaster_logo' ) ) {
function tt_gmaster_logo () {
	global $tt_temptt_opt;
	$width = $height = $style = $logo = '';
	$settingsl = array(
					'retina_favicon' => '0',
					'texttitle' => '0',
					'logo' => '',
					'retina_logo_wh' => '',
				);
	$settingsl = tt_temptt_opt_values( $settingsl );

	if ( $settingsl['texttitle'] == '1' ) return; // Get out if we're not displaying the logo.

	$logo = esc_url( TT_TEMPTT_THEME_DIRURI . 'images/logo-1.png' );
	if ( isset( $tt_temptt_opt['tt_logo']['url'] ) && $tt_temptt_opt['tt_logo']['url'] != '' ) { $logo = $tt_temptt_opt['tt_logo']['url'] ; }
	if ( is_ssl() ) { $logo = str_replace( 'http://', 'https://', $logo ); }
?>

	<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'description' ) ); ?>">
	 <?php if ( '0' == $settingsl['retina_favicon'] ) { ?>
		<img src="<?php echo esc_url( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
	 <?php } else {
       if( $settingsl['retina_logo_wh']['width'] != '0px' && $settingsl['retina_logo_wh']['width'] != 'px' ) {
			if (strpos($settingsl['retina_logo_wh']['width'],'px') !== false) {
				$width = 'width:'.$settingsl['retina_logo_wh']['width'].';';
			}
			else{
				$width = 'width:'.$settingsl['retina_logo_wh']['width'].'px;';
			}
        }
        if ($settingsl['retina_logo_wh']['height'] != '0px' && $settingsl['retina_logo_wh']['height'] != 'px') {
			if (strpos($settingsl['retina_logo_wh']['height'],'px') !== false) {
				$height = 'height:'.$settingsl['retina_logo_wh']['height'].';';
			}
			else{
				$height = 'height:'.$settingsl['retina_logo_wh']['height'].'px;';
			}
        }
		if ( !empty($width) ) $style .= $width;
		if ( !empty($height) ) $style .= $height ;
		if ( !empty($style) ) $style = 'style="'.$style.'"';
		?>
		<img src="<?php echo esc_url( $logo ); ?>" <?php echo wp_strip_all_tags( $style ); ?> alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
	 <?php } ?>
	</a>
<?php
} // End tt_gmaster_logo()
}

/*-----------------------------------------------------------------------------------*/
/* Header classes. */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_temptt_hdr_class' ) ) {
function tt_temptt_hdr_class () {
	global $tt_temptt_opt, $wp_query; $tt_post_id = $single_data2 = '';
	$class = 'no-Olap';
	$single_enable_overlap = '';
	if ( !is_404() && !is_search() ) {
		if ( ! empty( $wp_query->post->ID ) ) {
			$tt_post_id = $wp_query->post->ID;
		}
	}
	if( isset($tt_post_id) ) $single_data2 = get_post_meta( $tt_post_id, '_tt_meta_page_opt', true );
	if( is_array($single_data2) ) {
		if ( isset( $single_data2['_single_enable_overlap'] ) ) $single_enable_overlap = $single_data2['_single_enable_overlap'];
	}
	if ( ! empty( $single_enable_overlap )) $class = ' Olap ';
	$class .= ' hdr ';
	if ( tt_temptt_get_option( 'enable_sticky', 1 )) $class .= ' sticky ';
	if ( (tt_temptt_get_option( 'header_layout', 'header-1' )) == 'header-2' ) $class .= ' header-2 ';

	return $class;

} // End tt_temptt_hdr_class()
}


/*-----------------------------------------------------------------------------------*/
/* Load 404 Page banner image. */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_error_page' ) ) {
function tt_error_page() {
	global $tt_temptt_opt;
	$error_page = '';
	$settingsl = array(
					'error_page' => '',
				);

	$settingsl = tt_temptt_opt_values( $settingsl );
	$error_page = esc_url( TT_TEMPTT_THEME_DIRURI . 'images/breadcrumb-bg.jpg' );
	if ( isset( $tt_temptt_opt['tt_error_page']['url'] ) && $tt_temptt_opt['tt_error_page']['url'] != '' ) { $error_page = $tt_temptt_opt['tt_error_page']['url'] ; }
	if ( is_ssl() ) { $error_page = str_replace( 'http://', 'https://', $error_page ); }
?>
	
		<img src="<?php echo esc_url( $error_page ); ?>" alt="error_img" style="width: 100%; height: auto;" />

<?php
} // End tt_gmaster_logo()
}


if( !function_exists( 'tt_gmaster_hdr_info' ) ) {
	function tt_gmaster_hdr_info( $onlysearch='' ) {
	global $tt_temptt_opt;

		$settingsl = array(
					'hdr_phone' => '1',
					'hdr_email' => '1',
					'hdr_search' => '1',
					'contact_phone' => '',
					'multiphone' => '0',
					'contact_email' => '',
				);
		$settingsl = tt_temptt_opt_values( $settingsl ); $output = ''; ob_start();
		if ( tt_temptt_get_option( 'header_layout', 'header-1' ) == 'header-2' ) {
			if( !$onlysearch ) return;
			else { ?>
				<div class="top-search">
					<div class="col-lg-10 col-md-10">
						<ul class="small-nav">
							<?php if ( !empty( $settingsl['hdr_search'] ) ) { ?>
							<li class="searchlink" id="searchlink">
								<div class="searchform">
									<?php if ( class_exists( 'woocommerce' ) ) get_product_search_form(); else get_search_form(); ?>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
		<?php
			}
		}
		else {
		?>
				<div class="col-lg-10 col-md-10">
					<ul class="small-nav">
						<?php if ( !empty( $settingsl['hdr_phone'] ) && !empty( $settingsl['contact_phone'] ) ) { ?><li><i class="fa fa-phone-square"></i> <span class="gm-comma">-:</span>
							<?php if ( $settingsl['multiphone'] ) { ?>
								<?php echo wp_kses_post($settingsl['contact_phone']) ; ?>
							<?php } else { ?>
							<a href="tel:<?php echo preg_replace('/(\W*)/', '', $settingsl['contact_phone']); ?>"><?php echo esc_attr($settingsl['contact_phone']); ?></a>
							<?php } ?>
							</li>
						<?php } if ( !empty( $settingsl['hdr_email'] ) && !empty( $settingsl['contact_email'] ) ) { ?>
						<li><i class="fa fa-envelope-square"></i> <span class="gm-comma">-:</span> <a href="mailto:<?php echo sanitize_email($settingsl['contact_email']); ?>"><?php echo sanitize_email($settingsl['contact_email']); ?></a></li>
						<?php } if ( !empty( $settingsl['hdr_search'] ) ) { ?>
						<li class="searchlink" id="searchlink">
							<div class="searchform">
								<?php if ( class_exists( 'woocommerce' ) ) get_product_search_form(); else get_search_form(); ?>
							</div>
						</li>
						<?php } ?>
					</ul>
				</div>
<?php   }
		$output = ob_get_clean();
		return $output;
	}
}

if( !function_exists( 'tt_gmaster_social' ) ) {
	function tt_gmaster_social() {
	global $tt_temptt_opt;


	$settingsl = array(
					'hdr_social'           => '1',
					'connect_rss'           => '',
					'connect_twitter'       => '',
					'connect_facebook'      => '',
					'connect_youtube'       => '',
					'connect_flickr'        => '',
					'connect_linkedin'      => '',
					'connect_pinterest'     => '',
					'connect_instagram'     => '',
					'connect_googleplus'    => '',
					'open_social_newtab' => false,
				);
		$settingsl = tt_temptt_opt_values( $settingsl ); $output = '';
		if ( !$settingsl['hdr_social']) return; // nothing left to do !
		$newtabopen = ''; if($settingsl['open_social_newtab' ]) $newtabopen = ' target="_blank" ';
		ob_start();
		if ( tt_temptt_get_option( 'header_layout', 'header-1' ) == 'header-2' ) {
			tt_gmaster_hdr_info( $onlysearch = true );
		}
			else { ?>


				<ul class="nav navbar-nav navbar-right nav-social-icons">
					<?php if ( ! empty( $settingsl['connect_twitter'] ) ) { ?>
						<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settingsl['connect_twitter'] ); ?>"><i
									class="fa fa-twitter"></i></a></li>
					<?php }
					if ( ! empty( $settingsl['connect_facebook'] ) ) { ?>
						<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settingsl['connect_facebook'] ); ?>"><i
									class="fa fa-facebook"></i></a></li>
					<?php }
					if ( ! empty( $settingsl['connect_youtube'] ) ) { ?>
						<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settingsl['connect_youtube'] ); ?>"><i
									class="fa fa-youtube"></i></a></li>
					<?php }
					if ( ! empty( $settingsl['connect_flickr'] ) ) { ?>
						<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settingsl['connect_flickr'] ); ?>"><i
									class="fa fa-flickr"></i></a></li>
					<?php }
					if ( ! empty( $settingsl['connect_linkedin'] ) ) { ?>
						<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settingsl['connect_linkedin'] ); ?>"><i
									class="fa fa-linkedin"></i></a></li>
					<?php }
					if ( ! empty( $settingsl['connect_pinterest'] ) ) { ?>
						<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settingsl['connect_pinterest'] ); ?>"><i
									class="fa fa-pinterest"></i></a></li>
					<?php }
					if ( ! empty( $settingsl['connect_instagram'] ) ) { ?>
						<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settingsl['connect_instagram'] ); ?>"><i
									class="fa fa-instagram"></i></a></li>
					<?php }
					if ( ! empty( $settingsl['connect_googleplus'] ) ) { ?>
						<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settingsl['connect_googleplus'] ); ?>"><i
									class="fa fa-google-plus"></i></a></li>
					<?php } ?>
				</ul>

			<?php
			}
		$output = ob_get_clean();
		return $output;
	}
}
/*-----------------------------------------------------------------------------------*/
/* Load Search Page banner image. */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_search_page' ) ) {
function tt_search_page() {
	global $tt_temptt_opt;
	$search_page = '';
	$settingsl = array(
					'search_page' => '',
				);

	$settingsl = tt_temptt_opt_values( $settingsl );
	$search_page = esc_url( TT_TEMPTT_THEME_DIRURI . 'images/breadcrumb-bg.jpg' );
	if ( isset( $tt_temptt_opt['tt_search_page']['url'] ) && $tt_temptt_opt['tt_search_page']['url'] != '' ) { $search_page = $tt_temptt_opt['tt_search_page']['url'] ; }
	if ( is_ssl() ) { $search_page = str_replace( 'http://', 'https://', $search_page ); }
?>
	
		<img src="<?php echo esc_url( $search_page ); ?>" alt="search_img" style="width: 100%; height: auto;" />

<?php
} // End tt_gmaster_logo()
}





/*-----------------------------------------------------------------------------------*/
/* return excerpt with given charlent.                                               */
/*-----------------------------------------------------------------------------------*/
// source https://codex.wordpress.org/Function_Reference/get_the_excerpt
if (!function_exists( 'tt_gmaster_excerpt_charlength')) {
	function tt_gmaster_excerpt_charlength( $charlength ) {
		$excerpt = get_the_excerpt();
		$charlength ++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex   = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut   = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo esc_attr($subex);
			}
			echo '...';
		} else {
			echo esc_attr($excerpt);
		}
	}
}
/*-----------------------------------------------------------------------------------*/
/* Templatation , render featured post thumb function.                               */
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'tt_gmaster_post_thumb') ) {
	function tt_gmaster_post_thumb( $width = null, $height = null, $lightbox = true, $crop = true, $srconly = false, $id = '' ) {
		global $tt_temptt_opt, $post;
		if( empty($id) ) $id = get_the_ID();
		if( empty($id) ) return ''; // no id, nothing left to do.
		$output = $single_w = $single_h = $featuredimg = '';

		$featuredimg =  get_post_thumbnail_id($id);
		$featuredimg = wp_get_attachment_image_src( $featuredimg, 'full' );
		$featuredimg = $featuredimg[0];

		if( !isset( $featuredimg ) || empty ( $featuredimg ) ) return ''; // no image found, return.

		// if width and height is not given, we dont need to resize so output the full image.
		if( empty( $width ) || empty ( $height ) ) {

			if( $srconly ) { $returnimg = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'full' ); $returnimg = $returnimg[0]; }
			else $returnimg = get_the_post_thumbnail($id, 'full');

			return $returnimg;
		}

		$featuredimg = tt_gmaster_aq_resize( $featuredimg, $width, $height, $crop, false ); // this returns an array if image was cropped, and url otherwise.

		if( is_array( $featuredimg )) {
			$output = '
				<a href="'.$featuredimg[0].'">
						<img width="'.$featuredimg[1].'" height="'.$featuredimg[2].'" class="img-responsive" alt="" src="'.$featuredimg[0].'">
				</a>
			';
			if ( !$lightbox ) $output = '<img width="'.$featuredimg[1].'" height="'.$featuredimg[2].'" class="img-responsive" alt="" src="'.$featuredimg[0].'">'; // if no lightbox needed, turn the link off.
		}
		else $output = '<img class="img-responsive" alt="" src="'.$featuredimg.'">'; // image was not cropped so AQ returned URL.

		if( $srconly )  $output = isset($featuredimg[0]) ? $featuredimg[0] : $featuredimg;
		return $output;
	}
}

/*-----------------------------------------------------------------------------------*/
/* Misc small functions for visuals.                                                 */
/*-----------------------------------------------------------------------------------*/
// Adding span tag in category widgets for styling purpose
add_filter('wp_list_categories', 'tt_gmaster_cat_count');
function tt_gmaster_cat_count($links) {
  $links = str_replace('</a> (', ' <span>(', $links);
  $links = str_replace(')', ')</span></a>', $links);
  return $links;
}


// adding filter to add class in previous/next post button.
add_filter('next_post_link', 'tt_gmaster_post_link_attr');
add_filter('previous_post_link', 'tt_gmaster_post_link_attr');
function tt_gmaster_post_link_attr($output) {
    $injection = 'class="button"';
    return str_replace('<a href=', '<a '.$injection.' href=', $output);
}

// modifying search form.
function tt_gmaster_search_form( $form ) {
    $form = '<form method="get" class="searchform search-form" action="' . esc_url(home_url( '/' )) . '" >
    <div><label class="screen-reader-text">' . esc_html__( 'Search for:', 'guardmaster' ) . '</label>
    <input class="search-field" type="text" value="' . get_search_query() . '" name="s" placeholder="'. esc_html__( 'Search&hellip;', 'guardmaster' ) .'"  />
    <input type="submit" class="searchsubmit"  value="'. esc_attr__( 'Go', 'guardmaster' ) .'" />
    </div>
    </form>';

    return $form;
}

add_filter( 'get_search_form', 'tt_gmaster_search_form', 100 );
/*-----------------------------------------------------------------------------------*/
/* Breadcrumb display */
/*-----------------------------------------------------------------------------------*/
// use Yoast

/*-----------------------------------------------------------------------------------*/
/* Comment Form Fields */
/*-----------------------------------------------------------------------------------*/

	add_filter( 'comment_form_default_fields', 'tt_gmaster_cmntform_fields' );

	if ( ! function_exists( 'tt_gmaster_cmntform_fields' ) ) {
		function tt_gmaster_cmntform_fields ( $fields ) {

			$commenter = wp_get_current_commenter();

			$required_text = ' <span class="required">(' . esc_html__( 'Required', 'guardmaster' ) . ')</span>';

			$req = get_option( 'require_name_email' );
			$aria_req = ( $req ? " aria-required='true'" : '' );
			$fields =  array(
				'author' => '<p class="comment-form-author">' .
							'<input id="author" placeholder="'.esc_html__( "Name", 'guardmaster'  ) . ( $req ? '*' : '' ).'" class="form-control txt" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />' .
							'</p>',
				'email'  => '<p class="comment-form-email">' .
				            '<input id="email" class="form-control txt" name="email" placeholder="' . esc_html__( 'Email', 'guardmaster'  ) . ( $req ? '*' : '' ) . '" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />' .
				            '</p>',
				'url'    => '<p class="comment-form-url">' .
				            '<input id="url" class="form-control txt" name="url" placeholder="' . esc_html__( 'Website', 'guardmaster'  ) . '" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />' .
				            '</p>',
			);

			return $fields;

		} // End tt_gmaster_cmntform_fields()
	}

/*-----------------------------------------------------------------------------------*/
/* Comment Form Settings */
/*-----------------------------------------------------------------------------------*/

	add_filter( 'comment_form_defaults', 'tt_gmaster_cmntform_settings' );

	if ( ! function_exists( 'tt_gmaster_cmntform_settings' ) ) {
		function tt_gmaster_cmntform_settings ( $settings ) {

			$settings['comment_notes_before'] = '';
			$settings['comment_notes_after'] = '';
			$settings['label_submit'] = esc_html__( 'Submit Comment', 'guardmaster' );
			$settings['title_reply'] = esc_html__( 'Reply', 'guardmaster' );
			$settings['cancel_reply_link'] = esc_html__( 'Click here to cancel reply.', 'guardmaster' );
			$settings['title_reply_before'] = '<div class="comment-title h2">'. esc_html__( 'Post Your', 'guardmaster' ) .'</div><div class="comment-subtitle h1">';
			$settings['title_reply_after'] = '</div><div class="line"></div>';
  // redefine your own textarea (the comment body)
        $settings['comment_field']  = '<p class="comment-form-comment"><textarea id="comment" class="form-control" name="comment" aria-required="true" placeholder="' . _x( 'Comment', 'noun', 'guardmaster' ) . '"></textarea></p>';
			return $settings;

		} // End tt_gmaster_cmntform_settings()
	}

/*-----------------------------------------------------------------------------------*/
/**
 * tt_gmaster_archive_desc()
 *
 * Display a description, if available, for the archive being viewed (category, tag, other taxonomy).
 *
 * @since V1.0.0
 * @uses do_atomic(), get_queried_object(), term_description()
 * @echo string
 * @filter tt_gmaster_archive_desc
 */

if ( ! function_exists( 'tt_gmaster_archive_desc' ) ) {
	function tt_gmaster_archive_desc ( $echo = true ) {
		do_action( 'tt_gmaster_archive_desc' );
		$description = '';
		// Archive Description, if one is available.
		$term_obj = get_queried_object();
		if(is_array($term_obj))
		$description = term_description( $term_obj->term_id, $term_obj->taxonomy );

		if ( $description != '' ) {
			// Allow child themes/plugins to filter here ( 1: text in DIV and paragraph, 2: term object )
			$description = apply_filters( 'tt_gmaster_archive_desc', '<div class="archive-description">' . $description . '</div><!--/.archive-description-->', $term_obj );
		}

		if ( $echo != true ) { return $description; }

		echo esc_attr($description);
	} // End tt_gmaster_archive_desc()
}


/*-----------------------------------------------------------------------------------*/
/* Check if WooCommerce is activated */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_gmaster_is_wc' ) ) {
	function tt_gmaster_is_wc() {
		if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
	}
}

/*-----------------------------------------------------------------------------------*/
/* Truncate */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_gmaster_truncate' ) ) {
	function tt_gmaster_truncate($text, $limit, $sep='...') {
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = strip_tags( $text );
			$text = substr($text, 0, $pos[$limit]) . $sep;
		}
		return $text;
	}
}


/*-----------------------------------------------------------------------------------*/
/* Fixing the font size for the tag cloud widget.                                    */
/*-----------------------------------------------------------------------------------*/
add_filter( 'widget_tag_cloud_args', 'tt_gmaster_tag_cloud_args' );
if (!function_exists( 'tt_gmaster_tag_cloud_args')) {
	function tt_gmaster_tag_cloud_args($args) {
	$args['number'] = 10; //adding a 0 will display all tags
	$args['largest'] = 13; //largest tag
	$args['smallest'] = 13; //smallest tag
	$args['unit'] = 'px'; //tag font unit
	$args['format'] = 'list'; //ul with a class of wp-tag-cloud
	return $args;
	}
}

/*-----------------------------------------------------------------------------------*/
/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function tt_gmaster_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}

	global $page, $paged;
	// Add the blog name
	$title .= $sep." ".get_bloginfo( 'name', 'display' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	// Add a page number if necessary: , commenting cause below results in an warning.
/*	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title .= " $sep " . sprintf( esc_html__( 'Page %s', '_s' ), max( $paged, $page ) );
	}*/

	return $title;
}
//add_filter( 'wp_title', 'tt_gmaster_wp_title', 10, 2 );



/*-----------------------------------------------------------------------------------*/
/* Function for Adding Retina support, thanks to C.bavota                            */
/*-----------------------------------------------------------------------------------*/
add_filter( 'wp_generate_attachment_metadata', 'tt_gmaster_retina_attchmt_meta', 10, 2 );
/**
 * Retina images
 *
 * This function is attached to the 'wp_generate_attachment_metadata' filter hook.
 */
if ( ! function_exists( 'tt_gmaster_retina_attchmt_meta' ) ) {
function tt_gmaster_retina_attchmt_meta( $metadata, $attachment_id ) {
    foreach ( $metadata as $key => $value ) {
        if ( is_array( $value ) ) {
            foreach ( $value as $image => $attr ) {
                if ( is_array( $attr ) )
                    tt_gmaster_retina_create_images( get_attached_file( $attachment_id ), $attr['width'], $attr['height'], true );
            }
        }
    }

    return $metadata;
}
}
/**
 * Create retina-ready images
 *
 * Referenced via tt_gmaster_retina_attchmt_meta().
 */
if ( ! function_exists( 'tt_gmaster_retina_create_images' ) ) {
function tt_gmaster_retina_create_images( $file, $width, $height, $crop = false ) {
    if ( $width || $height ) {
        $resized_file = wp_get_image_editor( $file );
        if ( ! is_wp_error( $resized_file ) ) {
            $filename = $resized_file->generate_filename( $width . 'x' . $height . '@2x' );
 
            $resized_file->resize( $width * 2, $height * 2, $crop );
            $resized_file->save( $filename );
 
            $info = $resized_file->get_size();
 
            return array(
                'file' => wp_basename( $filename ),
                'width' => $info['width'],
                'height' => $info['height'],
            );
        }
    }
    return false;
}
}
add_filter( 'delete_attachment', 'tt_gmaster_delete_retina_images' );
/**
 * Delete retina-ready images
 *
 * This function is attached to the 'delete_attachment' filter hook.
 */
if ( ! function_exists( 'tt_gmaster_delete_retina_images' ) ) {
function tt_gmaster_delete_retina_images( $attachment_id ) {
    $meta = wp_get_attachment_metadata( $attachment_id );
    $upload_dir = wp_upload_dir();
	if (isset($meta["file"])) {
		$path = pathinfo( $meta["file"] );
		foreach ( $meta as $key => $value ) {
			if ( "sizes" === $key ) {
				foreach ( $value as $sizes => $size ) {
					$original_filename = $upload_dir['basedir'] . '/' . $path['dirname'] . '/' . $size['file'];
					$retina_filename = substr_replace( $original_filename, '@2x.', strrpos( $original_filename, '.' ), strlen( '.' ) );
					if ( file_exists( $retina_filename ) )
						unlink( $retina_filename );
				}
			}
		}
	}
}
}


/*-----------------------------------------------------------------------------------*/
/* tt_gmaster_prev_post function */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_gmaster_prev_post' ) ) {
	function tt_gmaster_prev_post() {
		$output    = '';
		$prev_post = get_adjacent_post( true, '', true );
		if ( is_a( $prev_post, 'WP_Post' ) ) {
			$output = '<div class="fl button3"><a class="tt_prev_post tt-button" title="'. get_the_title( $prev_post->ID ) .'" href="' . get_permalink( $prev_post->ID ) . '"><i class="fa fa-long-arrow-left"></i>' . esc_html__( 'Previous Post', 'guardmaster' ) . '</a></div>';
		}

		return $output;
	} // End tt_gmaster_prev_post()
}
/*-----------------------------------------------------------------------------------*/
/* tt_gmaster_next_post function */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_gmaster_next_post' ) ) {
	function tt_gmaster_next_post() {
		$output    = '';
		$prev_post = get_adjacent_post( true, '', false );
		if ( is_a( $prev_post, 'WP_Post' ) ) {
			$output = '<div class="fr button3"><a class="tt_next_post tt-button" title="'. get_the_title( $prev_post->ID ) .'" href="' . get_permalink( $prev_post->ID ) . '">' . esc_html__( 'Next Post', 'guardmaster' ) . '<i class="fa fa-long-arrow-right"></i></a></div>';
		}

		return $output;
	} // End tt_gmaster_next_post()
}

/*-----------------------------------------------------------------------------------*/
/* tt_gmaster_post_title function */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_gmaster_post_title' ) ) {
function tt_gmaster_post_title ( ) {

	$hide_title_display = $tt_page_title = $titlesettings = ''; $id = get_the_ID();
	$hide_title_display = get_post_meta( $id, '_tt_meta_page_opt', true );
	if( isset($hide_title_display['_hide_title_display'])) $titlesettings = $hide_title_display['_hide_title_display'];

	ob_start(); ?>
		<header class="post-header lc_tt_title">
			<ul>
				<li><?php the_time( 'M j, Y' ); ?></li>
				<li><?php if(!comments_open($id)) echo 'Comments Off'; else comments_popup_link( esc_html__( 'Zero comments', 'guardmaster' ), esc_html__( '1 Comment', 'guardmaster' ), esc_html__( '% Comments', 'guardmaster' ) ); ?></li>
			</ul>
			<h1><?php if( strlen( get_the_title()) > 1 ) { ?> <a href="<?php the_permalink(); ?>" title="<?php esc_attr_e( 'Continue Reading &rarr;', 'guardmaster' ); ?>"><?php the_title(); ?></a> <?php } ?></h1>
		</header>
		<?php

	$tt_post_title = ob_get_clean();
	if( is_singular() ){
		if( empty($titlesettings) || $titlesettings == '0' ) {
			echo esc_attr($tt_post_title);
		} // display title if not being hidden in single post.
	}
	else { echo esc_attr($tt_post_title); }
	} // End tt_gmaster_post_title()
}

/*-----------------------------------------------------------------------------------*/
/* tt_gmaster_page_title function */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_gmaster_page_title' ) ) {
function tt_gmaster_page_title ( ) {

	$hide_title_display = $tt_page_title = $titlesettings = '';
	$hide_title_display = get_post_meta( get_the_ID(), '_tt_meta_page_opt', true );
	if( isset($hide_title_display['_hide_title_display'])) $titlesettings = $hide_title_display['_hide_title_display'];

	ob_start();
	if ( empty($titlesettings) || $titlesettings == '0' ) { ?>
		<header class="post-header">
			<h1><?php if( strlen( get_the_title()) > 1 ) the_title(); ?></h1>
		</header>
		<?php
	} // display title if not being hidden in single page/post.

	$tt_page_title = ob_get_clean();
	echo esc_attr($tt_page_title);

	} // End tt_gmaster_page_title()
}


function tt_gmaster_lc_search_sc() {
	global $post;
	$id = get_the_ID();
	$show_fake = false;

	if ( get_post_type( $id ) == 'tt_misc' ) {
		$show_fake = true;
	}

	if ( $show_fake ) {
		return '<div class="dslc-notification dslc-red">Search Form appears here.</div>';
	}
	else {

		ob_start();
		get_search_form();

		$output = ob_get_clean();
		if ( get_post_type( $id ) == 'tt_misc' ) {
			$output = '[tt_lc_search] Shortcode';
		}

		return $output;
	}
}


function tt_gmaster_lc_title(){
	global $tt_temptt_opt, $post;
	$post_id = get_the_ID();
	$show_fake = true;

	$hide_title_display = $titlesettings = "";
	if ( is_singular() ) {
		$show_fake = false;
	}

	if ( get_post_type( $post_id ) == 'dslc_templates' ) {
		$show_fake = true;
	}

	if ( $show_fake ) {
		return '<div class="dslc-notification dslc-red">Post Title Appears Here.</div>';
	}
	else {
		$hide_title_display = get_post_meta( $post_id, '_tt_meta_post_opt', true );
		if ( is_array( $hide_title_display ) ) {
			$titlesettings = $hide_title_display['_hide_title_display'];
		}
		if ( ! isset( $titlesettings ) || $titlesettings == '0' ) {
			ob_start(); ?>
			<header class="post-header lc_tt_title">
				<ul>
					<li><?php the_time( 'M j, Y' ); ?></li>
					<li><?php comments_popup_link( esc_html__( 'Zero comments', 'guardmaster' ), esc_html__( '1 Comment', 'guardmaster' ), esc_html__( '% Comments', 'guardmaster' ) ); ?></li>
				</ul>
				<h1><?php the_title(); ?></h1>
			</header>
		<?php } // display title if not being hidden in single page/post.
		$output = ob_get_clean();

		return $output;
	}
}

function tt_gmaster_tt_numComments() {
	$tt_numComments = get_comments_number(); // get_comments_number returns only a numeric value

	if ( $tt_numComments == 0 ) {
		$comments = esc_html__('No comments yet.', 'guardmaster');
	} elseif ( $tt_numComments > 1 ) {
		$comments = $num_comments . esc_html__(' Comments', 'guardmaster');
	} else {
		$comments = esc_html__('1 Comment', 'guardmaster');
	}

	$output = $comments;

	return '<h2><span>'. $output .'</span></h2>';
}


/*-----------------------------------------------------------------------------------*/
/* Add Custom Styling to HEAD */
/*-----------------------------------------------------------------------------------*/
// this is hooked into wp_head.

if ( ! function_exists( 'tt_gmaster_custom_styling' ) ) {
	function tt_gmaster_custom_styling( $force = false ) {
		global $tt_temptt_opt;
		$output = $woo_hdr_class = $body_image = '';
		// Get options
		$settings = array(
						'top_nav_color' => '',
						'main_acnt_clr' => '',
						'custom_css' => ''
						);
		$settings = tt_temptt_opt_values( $settings );


		if($force) { // we have been forced to show specific colors.
			$settings['main_acnt_clr'] = $tt_temptt_opt['tt_main_acnt_clr'];
		}

		// Type Check for Array
		if ( is_array($settings) ) {

		if ( ! ( $settings['main_acnt_clr'] == '' )) { // only if user changed!
			$output .= '

.sidebar .title::after,footer h4.title:after, .tagcloud a:hover,
.woocommerce .product form.cart button[type="submit"], .woocommerce #review_form #respond .form-submit input,
.mainblock .product-grid .product:hover .one-item h3,
.mainblock .product-grid .product:hover .price-block,
.mainblock .tt-prodloop .product:hover .one-item h3,
.mainblock .tt-prodloop .product:hover .price-block,
.woocommerce input[type="submit"], .wc-proceed-to-checkout a,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
.woocommerce-checkout .woocommerce #order_review .place-order input[type="submit"],
.mainblock .tt-prodloop .product:hover .one-item h3, .mainblock .tt-prodloop .product:hover .price-block,
.herotext .line,  .herotext-2 .line, .line,.line2, .home-our-services ul.services li .line2,
.shop .product:hover .info, #options ul li:hover a .line2, #options ul li a.selected .line2,
.working-with-us .herotext .line, footer .line2, .we-deliver.herotext .line,
.security-solutions .herotext .line,.security-solutions .block .line2,
.security-solutions .type1.block:hover .line2,.blog .blog-post .post-contents .button a.btn-orange,
.comments .line, .sidebar .line2, .sidebar .tagcloud ul li a:hover,
.send-us-message .line, .error .herotext .line,.search-keyword .line, .pricing-table-01 .herotext .line,
.pricing-table-02 .herotext .line,
.pricing-table-02 .block.best .button a.btn-transparent, mark.dotted-line, blockquote, blockquote.quote-1,
blockquote.quote-2, .pagination2>li>a:hover, .pagination2>.active>a, .pagination2>.active>a:focus, .pagination2>.active>a:hover,
.pagination2>.active>span,
.pagination2>.active>span:focus, .pagination2>.active>span:hover,
.table>thead>tr>th .line2, .table.table-striped>thead>tr>th,
.inner-banner-style .line, a.btn-orange,a.btn-black i, a.btn-black:hover,
.widget_categories select:focus, .widget_archive select:focus,
.textwidget select:focus, .herotext .line, .woocommerce .woocommerce-message,
.woocommerce form .form-row input.input-text:focus,
.select2-search input:focus,
input:focus,select:focus,textarea:focus, .line2,
.sidebar .tagcloud ul li a:hover,
.send-us-message .line, .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span,  .leave-a-reply .line, .sidebar .title, footer h4.title::after, input:focus, select:focus, textarea:focus
{ border-color: '.$settings['main_acnt_clr'].'; }


button, html input[type="button"], input[type="reset"], input[type="submit"],
.comment-form #submit,.page-pagination li a:hover, .tagcloud a:hover, .wpcf7-submit,
.header-2 .top-search .search-field,
 .woocommerce .product form.cart button[type="submit"], .woocommerce #review_form #respond .form-submit input,
 .mainblock .product-grid .product:hover .one-item h3,
 .mainblock .product-grid .product:hover .price-block,
 .mainblock .tt-prodloop .product:hover .one-item h3,
 .mainblock .tt-prodloop .product:hover .price-block,
 .woocommerce input[type="submit"], .wc-proceed-to-checkout a,
 .woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
 .woocommerce-checkout .woocommerce #order_review .place-order input[type="submit"],
 .mainblock .tt-prodloop .product:hover .one-item h3, .mainblock .tt-prodloop .product:hover .price-block,
 .top-contact ul.small-nav li.shop,#s-1, ul.small-nav-1 li.shop,  .tp-caption.NotGeneric-Button, .NotGeneric-Button,
 .herotext-2 .line, .home-our-team .block .picture,
 .shop .product:hover .info, .latest-news .block .button a.btn-orange,
 .working-with-us .herotext .line, .we-deliver.herotext .line,
 .security-solutions .herotext .line,  .security-solutions .block:hover,
 .progress-01.skills-graph.skill .progress-bar,
 .blog .blog-post .post-contents .comments-2,.blog .blog-post .post-contents .button a.btn-orange,
 .blog .blog-post .post-contents .comments, .blog .blog-post .post-contents .description blockquote,
 .leave-a-reply .line,.leave-a-reply .btn,.sidebar .tagcloud ul li a:hover,
 .contact-us .address .block .picture,.send-us-message .line,.send-us-message .btn,
 .error .herotext .line,.search-keyword .line,.search-keyword .btn,  .pricing-table-01 .herotext .line,
 .pricing-table-01 .block.best .circle, .active-panel, .pricing-table-02 .herotext .line,
 .pricing-table-02 .block.best .circle,
 .pricing-table-02 .block.best .button a.btn-transparent, mark.orange,
 blockquote.quote-3 .icon,blockquote.quote-4, .pagination2>li>a:hover,
 .pagination2>.active>a, .pagination2>.active>a:focus, .pagination2>.active>a:hover,
.pagination2>.active>span,
.pagination2>.active>span:focus, .pagination2>.active>span:hover, .btn,
.progress-01 .progress-01-a .block .progress-bar.bg1,
.progress-01 .progress-01-b .block .progress-bar.bg1,.inner-banner-style .line,
 .scrollup, a.btn-orange, a.btn-black i, a.btn-black:hover,.calendar_wrap table caption,
.blog .blog-post .post-contents .description blockquote,
 .top-contact .searchlink .search-field,.woocommerce .single-product-item .add_to_cart_button,
 .woocommerce span.onsale, .one-item .red-dot, .tt-prod-images .red-dot,
 .woocommerce div.product form.cart .button, .woocommerce div.product .woocommerce-tabs ul.tabs li a:before,
 .woocommerce-billing-fields h3:after,#order_review_heading:after,
 .select2-results .select2-highlighted,
.woocommerce input.button.alt, .line, .progress-01 .skills-graph .skill .progress-bar,
.sidebar .tagcloud ul li a:hover, .send-us-message .line, .dropcap.round, .active-panel, .herotext .line,
.line,.owl-theme .owl-dots .owl-dot.active span,.owl-theme .owl-dots .owl-dot:hover span,.comments .line,.leave-a-reply .line, .inner-color
{ background-color:'.$settings['main_acnt_clr'].'; }



 a, a:hover, .widget_categories ul li a:hover, .woocommerce .product p.stock,
.woocommerce div.product .wc-tabs-wrapper ul.tabs li.active a,
.woocommerce div.product .wc-tabs-wrapper ul.tabs li a:hover, .star-rating span,
ul.nav-social-icons li a:hover i,#navigation ul.nav li a i, #navigation .navbar-default ul.nav li a i,
#navigation ul.nav > li.menu-item-has-children > a:after, .funfacts .block .box h1,
.home-our-services ul.services li:hover .icon,
#options ul li:hover a, #options ul li a.selected, .callus h1 span, .about-us .funfacts .block .box h1,
.history-timeline-tabs .nav>li.active .caption, .history-timeline-tabs .nav>li>a:focus .caption, .history-timeline-tabs .nav>li>a:hover .caption,
.security-solutions .type1.block:hover .icon,
.progress-01.skills-graph.skill .progress-label .icon,
.blog .blog-post .post-contents .description ul.social-icons li a:hover,
.comments .media-body .button a.btn-transparent i,.sidebar .search .form-inline .input-group .input-group-addon,
.sidebar .search .form-inline .input-group .input-group-addon a, .widget ul li a:hover,
.orange-text,  ul.check li.cross:before,  ul.chevron-right li:before,ul.check-square li:before,
ul.check-square li.cross:before,ul.hand-o-right li:before, blockquote.quote-1 p,
blockquote.quote-1 p, .woocommerce .products .star-rating, .tt-prod-summary .star-rating,
.woocommerce .star-rating, .woocommerce .woocommerce-info a,
.woocommerce-checkout #payment .payment_method_paypal .about_paypal,
.progress-01 .skills-graph .skill .progress-label .icon,
.sidebar .blog-categories ul li a:hover, ul.nav-social-icons li a:hover i,
#navigation ul.nav > li.menu-item-has-children > a:after, .security-solutions .block:hover h1
{ color: '.$settings['main_acnt_clr'].'; }

 .security-solutions .block:hover a{ color: #fff; }

.working-with-us .herotext .line{ background:'.$settings['main_acnt_clr'].'; }
{ outline: 1px solid '.$settings['main_acnt_clr'].'; }



						';
		}

			if ( $settings['top_nav_color'] != '' ) {
				$output .= 'header.sticky { background: ' . $settings['top_nav_color'] . ' }' . "\n";
			}
		} // End If Statement

			if ( $settings['custom_css'] != '' ) {
				$output .= $settings['custom_css'];
			}

        // add logo offset css
        if ( function_exists( 'tt_gmaster_logo_offset' ) ) {
          $output .= tt_gmaster_logo_offset();
        }

		// Output styles
		if ( isset( $output ) && $output != '' ) {
			$output = strip_tags( $output );
			// Remove space after colons
			$output = str_replace(': ', ':', $output);
			// Remove whitespace
			$output = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '   ', '    '), '', $output);

			$output = "\n" . "<!-- Theme Custom Styling -->\n<style type=\"text/css\">\n" . $output . "</style>\n";
			echo  $output; // its already sanitized by redux.
		}

	} // End tt_gmaster_custom_styling()
}



/*-----------------------------------------------------------------------------------*/
/* Function for color switcher. */
/*-----------------------------------------------------------------------------------*/
/*
 * it only works on demo websites, where its needed by the way.
 */
if( strpos(get_site_url(),'livedemo.wpengine') ) {
	add_action('tt_after_body', 'tt_temptt_clr_switcher');
	add_action('wp_head', 'tt_temptt_clr_switcher_scripts');
}
if (!function_exists('tt_temptt_clr_switcher_scripts')) {
	function tt_temptt_clr_switcher_scripts() {
		$output = '';
		ob_start(); ?>
	<script type="text/javascript" src="<?php  echo get_template_directory_uri(); ?>/inc/switcher/jquery.cookie.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/switcher/jscript_styleswitcher.js"></script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/inc/switcher/styleswitcher.css" />

	<?php
		$output = ob_get_clean();
		print $output; // its safe to output, all variables already escaped above.
}}

if (!function_exists('tt_temptt_clr_switcher')) {
	function tt_temptt_clr_switcher( ) {
		$ttcookie1 = $ttcookie2 = '' ;
		if(isset($_COOKIE['ttcookie1'])) {
			$ttcookie1 = '#' . $_COOKIE['ttcookie1'];
		} else {
			$ttcookie1 = '#ff974f';
		}
		if(isset($_COOKIE['ttcookie2'])) {
			$ttcookie2 = '#' . $_COOKIE['ttcookie2'];
		} else {
			$ttcookie2 = '#FE5454';
		}
		ob_start(); ?>

<!-------------------------------------------------------------------/
Color switcher for demo, to be removed from live website.
<!------------------------------------------------------------------->
	<?php
	if( $ttcookie1 != '#ff974f' /*|| $ttcookie2 != 'FE5454'*/ ) { // only trigger following if user actually changed colors
		global $tt_temptt_opt;
		$tt_temptt_opt['tt_main_acnt_clr'] = $ttcookie1;
		$tt_temptt_opt['tt_second_color']  = $ttcookie2;
		tt_gmaster_custom_styling(true);

	}
	?>
<!-- ADD Switcher -->
<div class="demo_changer">
	<div class="demo-icon"><i class="fa fa-gear"></i></div>
	<div class="form_holder">
		<p class="color-title"><?php esc_attr_e('THEME OPTIONS', 'guardmaster'); ?></p>

		<div class="predefined_styles">
			<div class="clear"></div>
		</div>
		<p><?php esc_attr_e('Change color', 'guardmaster'); ?></p>
		<div class="color-box">
			<div class="col-col">
				<div  id="colorSelector">
					<div class="inner-color" style="background-color: <?php  echo esc_attr($ttcookie1);?>"></div>
				</div>
				<p><?php esc_attr_e('Select base Color', 'guardmaster'); ?></p>
			</div>
<!--			<div class="col-col">
				<div  id="colorSelector_2">
					<div class="inner-color" style="background-color: <?php /* echo esc_attr($ttcookie2);*/?>"></div>
				</div>
				<p><?php /*esc_attr_e('Color 2', 'guardmaster'); */?></p>
			</div>
-->		</div>
		<span class="switcherbutton clear switchspan"><a rel="stylesheet" class="switchapply switchinner" href=""><?php esc_attr_e('APPLY COLOR', 'guardmaster'); ?></a></span>
		<span class="switcherbutton switcherreset clear switchspan"><a rel="stylesheet" class="switcherreset switchinner" href=""><?php esc_attr_e('RESET TO DEFAULT', 'guardmaster'); ?></a></span>
<!--		<span class="clear switchspan"><a rel="stylesheet" class="normallink" href="http://plumberwp.wpengine.com/"><?php esc_attr_e('Back to landing page', 'guardmaster'); ?></a></span>
        <span class="clear switchspan"><a rel="stylesheet" class="buy normallink" href="http://themeforest.net/item/plumberx-plumber-and-construction-wordpress-theme/14036883"><?php esc_attr_e('Purchase Guardmaster', 'guardmaster'); ?></a></span>
-->        <span class="clear switchspan alignl"><?php esc_attr_e('Note: Some colors are controlled by Slider & Pagebuilder.', 'guardmaster'); ?></span>
	</div>
</div>

<!-- END Switcher -->
<!-------------------------------------------------------------------/
EOF Color switcher for demo, to be removed from live website.
<!------------------------------------------------------------------->
<?php
		$output = ob_get_clean();
		print $output; // its safe to output, all variables already escaped above.
	}
}


/*-----------------------------------------------------------------------------------*/
/* Logo offset fuction. */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'tt_gmaster_logo_offset' ) ) {
/**
 * Output CSS for logo adjustments
 */
function tt_gmaster_logo_offset() {
	global $tt_temptt_opt;
    $outputlogostyle = $top = $topactive = $marginleft = '';
	//setting up defaults.
	$settings10 = array(
					'logo_left_offset' => '0',
					'logo_top_offset' => '0'
					);

	$settings10 = tt_temptt_opt_values( $settings10 );
	$logo_left_offset = $logo_top_offset = "0"; // setting up default
	if( !empty($settings10['logo_left_offset']) ) $logo_left_offset = str_replace("px","",$settings10['logo_left_offset']['width']);
	if( !empty($settings10['logo_top_offset']) ) $logo_top_offset = str_replace("px","",$settings10['logo_top_offset']['height']);

	if( ( $logo_left_offset == "0" ) && ( $logo_top_offset == "0" ) ) return; // do nothing if 0,0 is entered == reset.

	if( $logo_left_offset <> "0" ) $outputlogostyle .= 'header .logo { margin-left: '. $logo_left_offset .'px; }'; // setting up left offset
	if( $logo_top_offset <> "0" ) $outputlogostyle .= 'header .logo { margin-top: '. $logo_top_offset .'px; }';  // setting up top offset


	// Output styles
	if ( $outputlogostyle != '' ) {
		$outputlogostyle = '@media (min-width: 801px){ ' .$outputlogostyle. '}'; // output it only on desktop
		$outputlogostyle = strip_tags($outputlogostyle);
		return stripslashes( $outputlogostyle );
	}
} // End tt_gmaster_logo_offset()
}




/*-----------------------------------------------------------------------------------*/
/* Load custom favicon. */
/*-----------------------------------------------------------------------------------*/

if ( ! function_exists( 'tt_temptt_favicon_info' ) ) {
function tt_temptt_favicon_info () {
	global $tt_temptt_opt;
	$fv = '';
	$fv = esc_url( TT_TEMPTT_THEME_DIRURI . 'favicon.ico' );
	if ( isset( $tt_temptt_opt['tt_favicon_info1']['url'] ) && $tt_temptt_opt['tt_favicon_info1']['url'] != '' ) {
		$fv = $tt_temptt_opt['tt_favicon_info1']['url'] ; }
	if ( is_ssl() ) { $fv = str_replace( 'http://', 'https://', $fv ); }
?>
    <link href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="shortcut icon" type="image/x-icon" />
	<link href="<?php echo esc_url( $fv ); ?>" rel="shortcut icon" type="image/x-icon" />
<?php
} // End tt_temptt_favicon_info()
}



/*-----------------------------------------------------------------------------------*/
/* Function for showing google map inside container.                                 */
/*-----------------------------------------------------------------------------------*/
if (!function_exists('tt_gmaster_gmap')) {
	function tt_gmaster_gmap() {
 	global $tt_temptt_opt;
	$settings7 = $gmap = "";

	ob_start();

	   if ( isset($tt_temptt_opt['tt_contactform_map_coords']) && $tt_temptt_opt['tt_contactform_map_coords'] != '' ) { $geocoords = $tt_temptt_opt['tt_contactform_map_coords']; }  else { $geocoords = ''; } ?>
		<?php if ($geocoords != '') { ?>
		<?php tt_gmaster_maps_contact_output("geocoords=$geocoords"); ?>
		<?php }

	$gmap =  ob_get_clean();
	return $gmap;

}
}

/*-----------------------------------------------------------------------------------*/
/* Google Maps */
/*-----------------------------------------------------------------------------------*/
// Thanks Adii.

function tt_gmaster_maps_contact_output($args){
	global $tt_temptt_opt;

	if ( !is_array($args) )
		parse_str( $args, $args );

	extract($args);
	$mode = '';
	$streetview = 'off';
	$map_height = $tt_temptt_opt['tt_maps_single_height'];
//	$featured_w = $tt_temptt_opt['tt_home_featured_w']['width'];
//	$featured_h = $tt_temptt_opt['tt_home_featured_h']['height'];
	$zoom = $tt_temptt_opt['tt_maps_default_mapzoom'];
	$type = $tt_temptt_opt['tt_maps_default_maptype'];
	$marker_title = $tt_temptt_opt['tt_contact_title'];
	if ( $zoom == '' ) { $zoom = 6; }
//	$lang = $tt_temptt_opt['tt_maps_directions_locale'];
	$locale = '';
	if(!empty($lang)){
		$locale = ',locale :"'.$lang.'"';
	}
	$extra_params = ',{travelMode:G_TRAVEL_MODE_WALKING,avoidHighways:true '.$locale.'}';

	if(empty($map_height)) { $map_height = 250;}

	if(is_home() && !empty($featured_h) && !empty($featured_w)){
	?>
    <div id="single_map_canvas" style="width:<?php echo esc_attr($featured_w); ?>px; height: <?php echo esc_attr($featured_h); ?>px"></div>
    <?php } else { ?>
    <div id="single_map_canvas" style="width:100%; height: <?php echo esc_attr($map_height); ?>px"></div>
    <?php } ?>
    <script type="text/javascript">
		jQuery(document).ready(function(){
			function initialize() {


			<?php if($streetview == 'on'){ ?>


			<?php } else { ?>

			  	<?php switch ($type) {
			  			case 'G_NORMAL_MAP':
			  				$type = 'ROADMAP';
			  				break;
			  			case 'G_SATELLITE_MAP':
			  				$type = 'SATELLITE';
			  				break;
			  			case 'G_HYBRID_MAP':
			  				$type = 'HYBRID';
			  				break;
			  			case 'G_PHYSICAL_MAP':
			  				$type = 'TERRAIN';
			  				break;
			  			default:
			  				$type = 'ROADMAP';
			  				break;
			  	} ?>

			  	var myLatlng = new google.maps.LatLng(<?php echo esc_attr($geocoords); ?>);
				var myOptions = {
				  zoom: <?php echo esc_attr($zoom); ?>,
				  center: myLatlng,
				  mapTypeId: google.maps.MapTypeId.<?php echo esc_attr($type); ?>
				};
				<?php if( $tt_temptt_opt['tt_maps_scroll'] == '1'){ ?>
			  	myOptions.scrollwheel = false;
			  	<?php } ?>
			  	var map = new google.maps.Map(document.getElementById("single_map_canvas"),  myOptions);

				<?php if($mode == 'directions'){ ?>
			  	directionsPanel = document.getElementById("featured-route");
 				directions = new GDirections(map, directionsPanel);
  				directions.load("from: <?php echo esc_attr($from); ?> to: <?php echo esc_attr($to); ?>" <?php if($walking == 'on'){ echo esc_attr($extra_params);} ?>);
			  	<?php
			 	} else { ?>

			  		var point = new google.maps.LatLng(<?php echo esc_attr($geocoords); ?>);
	  				var root = "<?php echo constant('TT_TEMPTT_THEME_DIRURI'); ?>";
	  				var callout = '<?php echo preg_replace("/[\n\r]/","<br/>", $tt_temptt_opt['tt_maps_callout_text']); ?>';
	  				var the_link = '<?php echo get_permalink(get_the_id()); ?>';
	  				<?php $title = str_replace(array('&#8220;','&#8221;'),'"', $marker_title); ?>
	  				<?php $title = str_replace('&#8211;','-',$title); ?>
	  				<?php $title = str_replace('&#8217;',"`",$title); ?>
	  				<?php $title = str_replace('&#038;','&',$title); ?>
	  				var the_title = '<?php echo html_entity_decode($title) ?>';

	  			<?php
			 	if(is_page()){
/*			 		$custom = $tt_temptt_opt['tt_cat_custom_marker_pages'];
					if(!empty($custom)){
						$color = $custom;
					}
					else {
						$color = $tt_temptt_opt['tt_cat_colors_pages'];
						if (empty($color)) {
							$color = 'red';
						}
					}*/
					$color = 'default';
			 	?>
			 		var color = '<?php echo esc_attr($color); ?>';
			 		createMarker(map,point,root,the_link,the_title,color,callout);
			 	<?php } else { ?>
			 		var color = '<?php echo esc_attr($tt_temptt_opt['tt_cat_colors_pages']); ?>';
	  				createMarker(map,point,root,the_link,the_title,color,callout);
				<?php
				}
					if(isset($_POST['tt_maps_directions_search'])){ ?>

					directionsPanel = document.getElementById("featured-route");
 					directions = new GDirections(map, directionsPanel);
  					directions.load("from: <?php echo htmlspecialchars($_POST['tt_maps_directions_search']); ?> to: <?php echo esc_textarea($address); ?>" <?php if($walking == 'on'){ echo esc_attr($extra_params);} ?>);



					directionsDisplay = new google.maps.DirectionsRenderer();
					directionsDisplay.setMap(map);
    				directionsDisplay.setPanel(document.getElementById("featured-route"));

					<?php if($walking == 'on'){ ?>
					var travelmodesetting = google.maps.DirectionsTravelMode.WALKING;
					<?php } else { ?>
					var travelmodesetting = google.maps.DirectionsTravelMode.DRIVING;
					<?php } ?>
					var start = '<?php echo htmlspecialchars($_POST['tt_maps_directions_search']); ?>';
					var end = '<?php echo esc_textarea($address); ?>';
					var request = {
       					origin:start,
        				destination:end,
        				travelMode: travelmodesetting
    				};
    				directionsService.route(request, function(response, status) {
      					if (status == google.maps.DirectionsStatus.OK) {
        					directionsDisplay.setDirections(response);
      					}
      				});

  					<?php } ?>
				<?php } ?>
			<?php } ?>


			  }
			  function handleNoFlash(errorCode) {
				  if (errorCode == FLASH_UNAVAILABLE) {
					alert("Error: Flash doesn't appear to be supported by your browser");
					return;
				  }
				 }

		initialize();

		});
	jQuery(window).load(function(){

		var newHeight = jQuery('#featured-content').height();
		newHeight = newHeight - 5;
		if(newHeight > 300){
			jQuery('#single_map_canvas').height(newHeight);
		}

	});

	</script>

<?php
}

/*-----------------------------------------------------------------------------------*/
/* Function to retrieve tags. */
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'tt_gmaster_get_tags' )) {
	function tt_gmaster_get_tags(){
		$tags = get_the_tags();
		$tags_count = 0;
	    $html = '<p class=tagtitle>'. esc_html__('tags ', 'guardmaster') .'&nbsp;</p>';
		if( !is_array($tags) ) return;
	    foreach ($tags as $tag){
		    $tags_count ++;
	        $tag_link = get_tag_link($tag->term_id);
						if ( $tags_count > 1 ) {
							$html .= ' '; // tag separator here
						}

	        $html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='tag {$tag->slug}'>";
	        $html .= "{$tag->name}</a>";
	    }
	    echo '<div class="detail-tags">'. $html .'</div>';
	}
}

/*-----------------------------------------------------------------------------------*/
/* Function to retrieve categories. */
/*-----------------------------------------------------------------------------------*/
/*
 * it can either return single category or all categories separated by comma.
 * by default it returns all category separated by comma but if single category is required, just pass 'single' into the fn.
 *
 */
if (!function_exists('tt_gmaster_get_cats')) {
	function tt_gmaster_get_cats( $return='' ) {
		global $post, $wp_query;
		$output = '';
		$post_type_taxonomies = get_object_taxonomies( get_post_type(), 'objects' );
		foreach ( $post_type_taxonomies as $taxonomy ) {
			if ( $taxonomy->hierarchical == true ) {

				$cats       = get_the_terms( get_the_ID(), $taxonomy->name );
				$cats_count = 0;
				if ( $cats ) {
					foreach ( $cats as $cat ) {
						$cats_count ++;
						if ( $cats_count > 1 && $return == 'single' ) {
							break;
						}
						if ( $cats_count > 1 ) {
							$output .= ', ';
						}
						$output .= '<a class="tt_cats" href="' . get_term_link( $cat, $taxonomy->name ) . '">' . $cat->name . '</a>';
					}
				}
			}
		}
		return $output;
	}
}

/*-----------------------------------------------------------------------------------*/
/* Subscribe / Connect */
/*-----------------------------------------------------------------------------------*/
// Thanks Adii
if (!function_exists( 'tt_gmaster_subs_connect')) {
	function tt_gmaster_subs_connect($widget = 'false', $title = '', $form = '', $social = '', $contact_template = 'false') {

		//Setup default variables, overriding them if the "Theme Options" have been saved.

		$settings = array(
						'connect' => '0',
						'connect_title' => esc_html__('Subscribe' , 'guardmaster'),
						'connect_related' => '0',
						'connect_content' => esc_html__( 'Subscribe to our profiles on the following social networks.', 'guardmaster' ),
						'connect_newsletter_id' => '',
						'connect_mailchimp_list_url' => '',
						'feed_url' => '',
						'connect_rss' => '',
						'connect_twitter' => '',
						'connect_facebook' => '',
						'connect_youtube' => '',
						'connect_flickr' => '',
						'connect_linkedin' => '',
						'connect_pinterest' => '',
						'connect_instagram' => '',
						'connect_googleplus' => ''
						);
		$settings = tt_temptt_opt_values( $settings );

		// Setup title
		if ( $widget != 'true' )
			$title = $settings[ 'connect_title' ];

		// Setup related post (not in widget)
		$related_posts = '';
		if ( $settings[ 'connect_related' ] == "1" AND $widget != "true" )
			$related_posts = do_shortcode( '[related_posts limit="5"]' );

?>
	<?php if ( $settings[ 'connect' ] == "1" OR $widget == 'true' ) : ?>
		<h4 class="title"><?php if ( $title ) echo apply_filters( 'widget_title', $title ); else esc_html_e('Subscribe','guardmaster'); ?></h4>

		<div <?php if ( $related_posts != '' ) echo 'class="col-left"'; ?>>
			<?php if ($settings[ 'connect_content' ] != '' AND $contact_template == 'false') echo '<p>' . stripslashes($settings[ 'connect_content' ]) . '</p>'; ?>

			<?php if ( $settings[ 'connect_newsletter_id' ] != "" AND $form != 'on' ) : ?>
			<form class="newsletter-form<?php if ( $related_posts == '' ) echo ' fl'; ?>" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open( 'http://feedburner.google.com/fb/a/mailverify?uri=<?php echo esc_attr($settings[ 'connect_newsletter_id' ]); ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520' );return true">
				<input class="email" type="text" name="email" value="<?php esc_attr_e( 'E-mail', 'guardmaster' ); ?>" onfocus="if (this.value == '<?php esc_html_e( 'E-mail', 'guardmaster' ); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php esc_html_e( 'E-mail', 'guardmaster' ); ?>';}" />
				<input type="hidden" value="<?php echo esc_attr($settings[ 'connect_newsletter_id' ]); ?>" name="uri"/>
				<input type="hidden" value="<?php bloginfo( 'name' ); ?>" name="title"/>
				<input type="hidden" name="loc" value="en_US"/>
				<input class="submit email-submit" type="submit" name="submit" value="<?php esc_html_e( 'Submit', 'guardmaster' ); ?>" />
			</form>
			<?php endif; ?>

			<?php if ( $settings['connect_mailchimp_list_url'] != "" AND $form != 'on' AND $settings['connect_newsletter_id'] == "" ) : ?>
			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup">
				<form class="newsletter-form" action="<?php echo esc_url($settings['connect_mailchimp_list_url']); ?>" method="post" target="popupwindow" onsubmit="window.open('<?php echo esc_url($settings['connect_mailchimp_list_url']); ?>', 'popupwindow', 'scrollbars=yes,width=650,height=520');return true">
					<input type="text" name="EMAIL" class="required email" value="<?php esc_html_e('E-mail','guardmaster'); ?>"  id="mce-EMAIL" onfocus="if (this.value == '<?php esc_html_e('E-mail','guardmaster'); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php esc_html_e('E-mail','guardmaster'); ?>';}">
					<input type="submit" value="<?php esc_html_e('Submit', 'guardmaster'); ?>" name="subscribe" id="mc-embedded-subscribe" class="btn submit button">
				</form>
			</div>
			<!--End mc_embed_signup-->
			<?php endif; ?>

			<?php if ( $social != 'on' ) : ?>
			<div class="social<?php if ( $related_posts == '' AND $settings['connect_newsletter_id' ] != "" ) echo ' fr'; ?>">
		   		<?php if ( $settings['connect_rss' ] == "1" ) { ?>
		   		<a href="<?php if ( $settings['feed_url'] ) { echo esc_url( $settings['feed_url'] ); } else { echo get_bloginfo_rss('rss2_url'); } ?>" class="subscribe" title="RSS"></a>

		   		<?php } if ( $settings['connect_twitter' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $settings['connect_twitter'] ); ?>" class="twitter" title="Twitter"></a>

		   		<?php } if ( $settings['connect_facebook' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $settings['connect_facebook'] ); ?>" class="facebook" title="Facebook"></a>

		   		<?php } if ( $settings['connect_youtube' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $settings['connect_youtube'] ); ?>" class="youtube" title="YouTube"></a>

		   		<?php } if ( $settings['connect_flickr' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $settings['connect_flickr'] ); ?>" class="flickr" title="Flickr"></a>

		   		<?php } if ( $settings['connect_linkedin' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $settings['connect_linkedin'] ); ?>" class="linkedin" title="LinkedIn"></a>

		   		<?php } if ( $settings['connect_pinterest' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $settings['connect_pinterest'] ); ?>" class="pinterest" title="Pinterest"></a>

		   		<?php } if ( $settings['connect_instagram' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $settings['connect_instagram'] ); ?>" class="instagram" title="Instagram"></a>

		   		<?php } if ( $settings['connect_googleplus' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $settings['connect_googleplus'] ); ?>" class="googleplus" title="Google+"></a>

				<?php } ?>
			</div>
			<?php endif; ?>

		</div><!-- col-left -->

	<?php endif; ?>
<?php
	}
}


/*-----------------------------------------------------------------------------------*/
/* Adding hero images for pages                                                      */
/*-----------------------------------------------------------------------------------*/

if( !function_exists('tt_gmaster_hero_section') ) {
	function tt_gmaster_hero_section() {
	global $tt_temptt_opt, $wp_query;
	$single_hero_tpadding = $single_hero_bpadding = $tt_post_id = $single_enable_headline = $single_headline_title = $single_text_align = $single_page_color = $single_page_img = $single_text_apprnce = $single_hero_breadcrumbs = $hide_title_display = $single_display_breadcrumbs = '';
	if ( is_404() || is_search() ) return;
	if( isset($wp_query->post->ID)) $tt_post_id = $wp_query->post->ID;
	if ( !shortcode_exists('tt_hero_shortcode') ) return; // nothing left to do!
	if(is_home()) $tt_post_id = get_option( 'page_for_posts' );
	if ( class_exists( 'woocommerce' ) ) {
		if ( is_shop() ) {
			$tt_post_id = get_option( 'woocommerce_shop_page_id' );
		}
		if ( is_account_page() ) {
			$tt_post_id = get_option( 'woocommerce_myaccount_page_id' );
		}
		if ( is_checkout() ) {
			$tt_post_id = get_option( 'woocommerce_checkout_page_id' );
		}
		if ( is_cart() ) {
			$tt_post_id = get_option( 'woocommerce_cart_page_id' );
		}
	}
	if( empty($tt_post_id) ) return; // nothing left to do!

	// if the current page template is contact page.
	//$current_page_template = get_post_meta($tt_post_id, '_wp_page_template', true);


	// fetching value from single posts .
	$single_data2 = get_post_meta( $tt_post_id, '_tt_meta_page_opt', true );
	if( is_array($single_data2) ) {
		if ( isset( $single_data2['_single_enable_headline'] ) ) $single_enable_headline = $single_data2['_single_enable_headline'];
		if ( isset( $single_data2['_single_headline_title'] ) ) $single_headline_title = esc_attr($single_data2['_single_headline_title']);
		if ( isset( $single_data2['_single_page_img'] ) ) $single_page_img = esc_textarea($single_data2['_single_page_img']);
		if ( isset( $single_data2['_single_page_color'] ) ) $single_page_color = esc_attr($single_data2['_single_page_color']);
		if ( isset( $single_data2['_single_text_apprnce'] ) ) $single_text_apprnce = esc_attr($single_data2['_single_text_apprnce']);
		if ( isset( $single_data2['_single_hero_breadcrumbs'] ) ) $single_hero_breadcrumbs = esc_attr($single_data2['_single_hero_breadcrumbs']);
		if ( isset( $single_data2['_single_hero_tpadding'] ) ) $single_hero_tpadding = esc_attr($single_data2['_single_hero_tpadding']);
		if ( isset( $single_data2['_single_hero_bpadding'] ) ) $single_hero_bpadding = esc_attr($single_data2['_single_hero_bpadding']);
	}

	if ( !$single_enable_headline ) return; // user dont want it!

	//setting up defaults
	if( empty($single_hero_tpadding)) $single_hero_tpadding = '200';
	if( empty($single_hero_bpadding)) $single_hero_bpadding = '92';

	echo do_shortcode('[tt_hero_shortcode
	heading="'. $single_headline_title .'"
	image="'. tt_temptt_get_image_id($single_page_img) .'"
	color="'. $single_page_color .'"
	text_appear="'. $single_text_apprnce .'"
	yoast_bdcmp="'. $single_hero_breadcrumbs .'"
	block_padding_top="'. $single_hero_tpadding .'"
	block_padding_bottom="'. $single_hero_bpadding .'"

	]'). '<div class="mbottom70 tpadd"></div>' ; // all variables in html blcok are already escaped. we can output directly.
	}
}
add_action( 'tt_before_mainblock', 'tt_gmaster_hero_section' );


// source: https://pippinsplugins.com/retrieve-attachment-id-from-image-url/
if( !function_exists('tt_temptt_get_image_id')) {
	function tt_temptt_get_image_id( $image_url ) {
		global $wpdb;
		$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ) );
		if( is_array($attachment) && isset($attachment[0]) )
		return $attachment[0];
		else return $image_url;
	}
}
/*-----------------------------------------------------------------------------------*/
/* Allowed tags                                                                      */
/*-----------------------------------------------------------------------------------*/

if(!( function_exists('tt_gmaster_allowed_tags') )){
	function tt_gmaster_allowed_tags(){
		return array(
		    'img' => array(
		        'src' => array(),
		        'alt' => array(),
		        'class' => array(),
		        'style' => array(),
		    ),
		    'a' => array(
		        'href' => array(),
		        'title' => array(),
		        'class' => array(),
		        'target' => array()
		    ),
		    'br' => array(),
		    'div' => array(
		        'class' => array(),
		        'style' => array(),
		    ),
		    'span' => array(
		        'class' => array(),
		        'style' => array(),
		    ),
		    'h1' => array(
		        'class' => array(),
		        'style' => array(),
		    ),
		    'h2' => array(
		        'class' => array(),
		        'style' => array(),
		    ),
		    'h3' => array(
		        'class' => array(),
		        'style' => array(),
		    ),
		    'h4' => array(
		        'class' => array(),
		        'style' => array(),
		    ),
		    'h5' => array(
		        'class' => array(),
		        'style' => array(),
		    ),
		    'h6' => array(
		        'class' => array(),
		        'style' => array(),
		    ),
		    'style' => array(),
		    'em' => array(),
		    'strong' => array(),
		    'p' => array(
		    	'class' => array(),
		        'style' => array(),
		    ),
		);
	}
}

function tt_gmaster_css_allow($allowed_attr) {

    if (!is_array($allowed_attr)) {
        $allowed_attr = array();
    }

    $allowed_attr[] = 'display';
    $allowed_attr[] = 'background-image';
    $allowed_attr[] = 'url';

    return $allowed_attr;
} add_filter('safe_style_css','tt_gmaster_css_allow');

/*-----------------------------------------------------------------------------------*/
/* Image Resizer script for on the fly resizing                                     */
/*-----------------------------------------------------------------------------------*/
// Source: https://github.com/syamilmj/Aqua-Resizer

if( ! class_exists('tt_gmaster_Aq_Resize') ) {

	/**
	 * Image resizing class
	 *
	 * @since 1.0
	 */
	class tt_gmaster_Aq_Resize {

		/**
		 * The singleton instance
		 */
		static private $instance = null;

		/**
		 * No initialization allowed
		 */
		private function __construct() {}

		/**
		 * No cloning allowed
		 */
		private function __clone() {}

		/**
		 * For your custom default usage you may want to initialize an Aq_Resize object by yourself and then have own defaults
		 */
		static public function getInstance() {
			if(self::$instance == null) {
				self::$instance = new self;
			}

			return self::$instance;
		}

		/**
		 * Run, forest.
		 */
		public function process( $url, $width = null, $height = null, $crop = null, $single = true, $upscale = true ) {

			// Validate inputs.
			if ( ! $url || ( ! $width && ! $height ) ) return false;

			$upscale = true;

			// Caipt'n, ready to hook.
			if ( true === $upscale ) add_filter( 'image_resize_dimensions', array($this, 'aq_upscale'), 10, 6 );

			// Define upload path & dir.
			$upload_info = wp_upload_dir();
			$upload_dir = $upload_info['basedir'];
			$upload_url = $upload_info['baseurl'];

			$http_prefix = "http://";
			$https_prefix = "https://";

			/* if the $url scheme differs from $upload_url scheme, make them match
			   if the schemes differe, images don't show up. */
			if(!strncmp($url,$https_prefix,strlen($https_prefix))){ //if url begins with https:// make $upload_url begin with https:// as well
				$upload_url = str_replace($http_prefix,$https_prefix,$upload_url);
			}
			elseif(!strncmp($url,$http_prefix,strlen($http_prefix))){ //if url begins with http:// make $upload_url begin with http:// as well
				$upload_url = str_replace($https_prefix,$http_prefix,$upload_url);
			}


			// Check if $img_url is local.
			if ( false === strpos( $url, $upload_url ) ) return false;

			// Define path of image.
			$rel_path = str_replace( $upload_url, '', $url );
			$img_path = $upload_dir . $rel_path;

			// Check if img path exists, and is an image indeed.
			if ( ! file_exists( $img_path ) or ! getimagesize( $img_path ) ) return false;

			// Get image info.
			$info = pathinfo( $img_path );
			$ext = $info['extension'];
			list( $orig_w, $orig_h ) = getimagesize( $img_path );

			// Get image size after cropping.
			$dims = image_resize_dimensions( $orig_w, $orig_h, $width, $height, $crop );
			$dst_w = $dims[4];
			$dst_h = $dims[5];

			// Return the original image only if it exactly fits the needed measures.
			if ( ! $dims && ( ( ( null === $height && $orig_w == $width ) xor ( null === $width && $orig_h == $height ) ) xor ( $height == $orig_h && $width == $orig_w ) ) ) {
				$img_url = $url;
				$dst_w = $orig_w;
				$dst_h = $orig_h;
			} else {
				// Use this to check if cropped image already exists, so we can return that instead.
				$suffix = "{$dst_w}x{$dst_h}";
				$dst_rel_path = str_replace( '.' . $ext, '', $rel_path );
				$destfilename = "{$upload_dir}{$dst_rel_path}-{$suffix}.{$ext}";

				if ( ! $dims || ( true == $crop && false == $upscale && ( $dst_w < $width || $dst_h < $height ) ) ) {
					// Can't resize, so return false saying that the action to do could not be processed as planned.
					return $url;
				}
				// Else check if cache exists.
				elseif ( file_exists( $destfilename ) && getimagesize( $destfilename ) ) {
					$img_url = "{$upload_url}{$dst_rel_path}-{$suffix}.{$ext}";
				}
				// Else, we resize the image and return the new resized image url.
				else {

					$editor = wp_get_image_editor( $img_path );

					if ( is_wp_error( $editor ) || is_wp_error( $editor->resize( $width, $height, $crop ) ) )
						return $url;

					$resized_file = $editor->save();

					if ( ! is_wp_error( $resized_file ) ) {
						$resized_rel_path = str_replace( $upload_dir, '', $resized_file['path'] );
						$img_url = $upload_url . $resized_rel_path;
					} else {
						return $url;
					}

				}
			}

			// Okay, leave the ship.
			if ( true === $upscale ) remove_filter( 'image_resize_dimensions', array( $this, 'aq_upscale' ) );

			// Return the output.
			if ( $single ) {
				// str return.
				$image = $img_url;
			} else {
				// array return.
				$image = array (
					0 => $img_url,
					1 => $dst_w,
					2 => $dst_h
				);
			}

			return $image;
		}

		/**
		 * Callback to overwrite WP computing of thumbnail measures
		 */
		function aq_upscale( $default, $orig_w, $orig_h, $dest_w, $dest_h, $crop ) {
			if ( ! $crop ) return null; // Let the wordpress default function handle this.

			// Here is the point we allow to use larger image size than the original one.
			$aspect_ratio = $orig_w / $orig_h;
			$new_w = $dest_w;
			$new_h = $dest_h;

			if ( ! $new_w ) {
				$new_w = intval( $new_h * $aspect_ratio );
			}

			if ( ! $new_h ) {
				$new_h = intval( $new_w / $aspect_ratio );
			}

			$size_ratio = max( $new_w / $orig_w, $new_h / $orig_h );

			$crop_w = round( $new_w / $size_ratio );
			$crop_h = round( $new_h / $size_ratio );

			$s_x = floor( ( $orig_w - $crop_w ) / 2 );
			$s_y = floor( ( $orig_h - $crop_h ) / 2 );

			return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
		}

	}

}


if ( ! function_exists('tt_gmaster_aq_resize') ) {

	/**
	 * Resize an image using tt_gmaster_Aq_Resize Class
	 *
	 * @since 1.0
	 *
	 * @param string $url     The URL of the image
	 * @param int    $width   The new width of the image
	 * @param int    $height  The new height of the image
	 * @param bool   $crop    To crop or not to crop, the question is now
	 * @param bool   $single  If true only returns the URL, if false returns array
	 * @param bool   $upscale If image not big enough for new size should it upscale
	 * @return mixed If $single is true return new image URL, if it is false return array
	 *               Array contains 0 = URL, 1 = width, 2 = height
	 */
	function tt_gmaster_aq_resize( $url, $width = null, $height = null, $crop = null, $single = true, $upscale = false ) {
		$aq_resize = tt_gmaster_Aq_Resize::getInstance();
		return $aq_resize->process( $url, $width, $height, $crop, $single, $upscale );
	}

}


if( !function_exists( 'tt_temptt_wc_social' ) ) {
	function tt_temptt_wc_social() {
		$output = '';
		if( ! tt_temptt_get_option( 'enable_post_share', true) ) return; // do nothing if not enabled in Themeoptions.

		if( shortcode_exists('templatation_pinterest')) $output .= do_shortcode('[templatation_pinterest float="fl" class="tt-mr15"]');

		if( shortcode_exists('templatation_twitter')) $output .= do_shortcode('[templatation_twitter use_post_url="true" float="fl" class="tt-mr15"]');

		if( shortcode_exists('templatation_fblike')) $output .= do_shortcode('[templatation_fblike style="button_count" float="fl" class="tt-mr15"]');

		return $output;
	}
}




//Widget Theme  options contact info
class tt_guard_widget_link extends WP_Widget {

    // Create Widget
    function tt_guard_widget_link() {
        parent::__construct(false, $name = 'GM: Info & Social', array('description' => 'Displays information and social links.'));
    }

    // Widget Content
    function widget($args, $instance) {
        extract( $args );
        $simple_title = strip_tags($instance['simple_title']);
        $simple_text = $instance['simple_text'];
        $show_contact = $instance['show_contact'] ? 'true' : 'false';
		?>


		<?php if (!empty($simple_title)) {?>
		<div class="sidebar-widget-title">
			<h4 class="title"><?php echo esc_attr($simple_title); ?></h4>
		</div>
		<?php } ?>
		<div class="widget-lpinfo">
			<?php echo wp_kses_post($simple_text) ?>
		</div>
	    <?php // social icons
	    		//Setup default variables, overriding them if the "Theme Options" have been saved.
		$settings = array(
						'contact_phone' => '',
						'contact_email' => '',
						'contact_address' => '',
						);
		$settings = tt_temptt_opt_values( $settings );
		?>
		<?php if (!empty($show_contact)) {?>
				<ul class="contact">
					<?php if ( !empty( $settings['contact_phone'] ) ) { ?><li><i class="fa fa-phone-square"></i> <span class="gm-comma">-:</span> <a href="tel:<?php echo preg_replace('/(\W*)/', '', $settings['contact_phone']); ?>"><?php echo esc_attr($settings['contact_phone']); ?></a></li>
					<?php } if ( !empty( $settings['contact_email'] ) ) { ?>
					<li><i class="fa fa-envelope-square"></i> <span class="gm-comma">-:</span> <a href="mailto:<?php echo sanitize_email($settings['contact_email']); ?>"><?php echo sanitize_email($settings['contact_email']); ?></a></li>
					<li><i class="fa fa-home"></i> <span class="gm-comma">-:</span> <?php echo do_shortcode($settings['contact_address']); ?></li>
					<?php } ?>
				</ul>
			<?php } ?>

        <?php
     }

    // Update and save the widget
    function update($new_instance, $old_instance) {
    return $new_instance;
    }

    // If widget content needs a form
     function form($instance) {
        //widgetform in backend
        $simple_title = (isset($instance['simple_title'])) ? strip_tags($instance['simple_title']) : '';
        $simple_text = (isset($instance['simple_text'])) ? $instance['simple_text'] : '';
        $show_contact = (isset($instance['show_contact'])) ? 'checked' : '';

        ?>
            <p>
                <label for="<?php echo $this->get_field_id('simple_title'); ?>">Widget title: </label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('simple_title')); ?>" name="<?php echo esc_attr($this->get_field_name('simple_title')); ?>" type="text" value="<?php echo esc_attr($simple_title); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('simple_text')); ?>">Text: </label>
                <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('simple_text')); ?>" name="<?php echo esc_attr($this->get_field_name('simple_text')); ?>"><?php echo wp_kses_post($simple_text); ?></textarea>
            </p>
            <p>

                <label for="<?php echo esc_html($this->get_field_id('show_contact')); ?>">Show contact info from themeoptions: </label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('show_contact')); ?>" name="<?php echo esc_attr($this->get_field_name('show_contact')); ?>" type="checkbox"  value="<?php echo esc_attr($show_contact);?>" <?php print esc_attr($show_contact); ?>/>
            </p>



        <?php
    }
}

register_widget('tt_guard_widget_link');



/*Sidebar latest post*/

class tt_guard_LP_widget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'gm_recent_posts',
			'GM: Latest Post Widget',
			array( 'description' => 'Widget with Latest posts' )
		);
	}


	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$posts_per_page = $instance['posts_per_page'];
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		$q = new WP_Query("posts_per_page=$posts_per_page");
		if( $q->have_posts() ):
			?><div class="recent-posts"><ul> <?php
			while( $q->have_posts() ): $q->the_post();?>


							<li class="clearfix">
								<div class="picture"><?php the_post_thumbnail(array(95, 85)); ?></div>
								<div class="info">
									<div class="caption"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></div>
									<div class="date"><span class="gm-comma">-:</span> <?php the_time('F d Y') ?></div>
								</div>
							</li>

				<?php
			endwhile;
			?></ul></div><!--.recent-posts--><?php
		endif;
		wp_reset_postdata();
		echo $args['after_widget'];
	}


	public function form( $instance ) {
        //widgetform in backend
        $title = (isset($instance['title'])) ? strip_tags($instance['title']) : '';
        $posts_per_page = (isset($instance['posts_per_page'])) ? $instance['posts_per_page'] : '';

		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php  esc_html_e('Title ', 'guardmaster'); ?> </label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'posts_per_page' )); ?>"><?php  esc_html_e('Posts per page:', 'guardmaster'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'posts_per_page' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'posts_per_page' )); ?>" type="text" value="<?php echo ($posts_per_page) ? esc_attr( $posts_per_page ) : '3'; ?>" size="3" />
		</p>
		<?php
	}


	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['posts_per_page'] = ( is_numeric( $new_instance['posts_per_page'] ) ) ? $new_instance['posts_per_page'] : '3';
		return $instance;
	}
}

function tt_guard_LP_widget_load() {
	register_widget( 'tt_guard_LP_widget' );
}
add_action( 'widgets_init', 'tt_guard_LP_widget_load' );


/*Footer connect*/

class tt_guard_LP_connect_widget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'gm_footer_connect',
			'GM: Footer Social Widget',
			array( 'description' => 'Widget displays social icons' )
		);
	}


	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		print $args['before_widget'];
		if ( ! empty( $title ) )
			print $args['before_title'] . $title . $args['after_title'];

		$settings = array(
						'connect_rss' => '',
						'connect_twitter' => '',
						'connect_facebook' => '',
						'connect_youtube' => '',
						'connect_flickr' => '',
						'connect_linkedin' => '',
						'connect_pinterest' => '',
						'connect_instagram' => '',
						'connect_googleplus' => '',
						'open_social_newtab' => false,
						);
		$settings = tt_temptt_opt_values( $settings );
		$newtabopen = ''; if($settings['open_social_newtab' ]) $newtabopen = ' target="_blank" ';
		?>

						<ul class="connect">
							<?php if ( $settings['connect_facebook' ] != "" ) { ?>
							<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settings['connect_facebook'] ); ?>"><i class="fa fa-facebook"></i> <?php  esc_html_e('Facebook', 'guardmaster'); ?></a></li>
							<?php } if ( $settings['connect_twitter' ] != "" ) { ?>
							<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settings['connect_twitter'] ); ?>"><i class="fa fa-twitter"></i> <?php  esc_html_e('Twitter', 'guardmaster'); ?></a></li>
							<?php } if ( $settings['connect_linkedin' ] != "" ) { ?>
							<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settings['connect_linkedin'] ); ?>"><i class="fa fa-linkedin"></i> <?php  esc_html_e('Linked In', 'guardmaster'); ?></a></li>
							<?php } if ( $settings['connect_googleplus' ] != "" ) { ?>
							<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settings['connect_googleplus'] ); ?>"><i class="fa fa-google-plus"></i> <?php  esc_html_e('Google+', 'guardmaster'); ?></a></li>
							<?php } if ( $settings['connect_instagram' ] != "" ) { ?>
							<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settings['connect_instagram'] ); ?>"><i class="fa fa-instagram"></i> <?php  esc_html_e('Instagram', 'guardmaster'); ?></a></li>
							<?php } if ( $settings['connect_pinterest' ] != "" ) { ?>
							<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settings['connect_pinterest'] ); ?>"><i class="fa fa-pinterest"></i> <?php  esc_html_e('Pinterest', 'guardmaster'); ?></a></li>
							<?php } if ( $settings['connect_flickr' ] != "" ) { ?>
							<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settings['connect_flickr'] ); ?>"><i class="fa fa-flickr"></i> <?php  esc_html_e('Flickr', 'guardmaster'); ?></a></li>
							<?php } if ( $settings['connect_youtube' ] != "" ) { ?>
							<li><a  <?php print $newtabopen; ?> href="<?php echo esc_url( $settings['connect_youtube'] ); ?>"><i class="fa fa-youtube"></i> <?php  esc_html_e('Youtube', 'guardmaster'); ?></a></li>
							<?php } ?>
						</ul>
		<?php
		print $args['after_widget'];
	}


	public function form( $instance ) {
        //widgetform in backend
        $title = (isset($instance['title'])) ? strip_tags($instance['title']) : '';

		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php  esc_html_e('Title (social icon values comes from themeoptions) ', 'guardmaster'); ?> </label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
	}


	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
}

function tt_guard_LP_connect_widget_load() {
	register_widget( 'tt_guard_LP_connect_widget' );
}
add_action( 'widgets_init', 'tt_guard_LP_connect_widget_load' );



/*-----------------------------------------------------------------------------------*/
/* END */
/*-----------------------------------------------------------------------------------*/
