<?php
if ( ! defined( 'ABSPATH' ) ) exit;
// Fist full of comments
if ( ! function_exists( 'tt_temptt_cust_cmnt' ) ) {
	function tt_temptt_cust_cmnt( $comment, $args, $depth ) {
	   $GLOBALS['comment'] = $comment; ?>

		<li <?php comment_class(); ?>>
			<div>
	    	<a id="comment-<?php comment_ID() ?>-start"></a>
			<div class="media-left">
				<?php print get_avatar( $comment, apply_filters( 'tt_comment_avatar_size', $size = 100 ) ); ?>
			</div>
			<div class="media-body">
				<div class="clearfix ">
					<div class="meta">
						<h4 class="media-heading"><?php tt_temptt_commenter_link(); ?> <span><?php esc_html_e( ' says', 'guardmaster' ); ?></span></h4>
						<div class="date"><span class="gm-comma">-:</span> <?php print get_comment_date( get_option( 'date_format' ) ). esc_html_e( ' at ', 'guardmaster' ) . get_comment_time( get_option( 'time_format' ) ); ?>
						<span class="perma"><a href="<?php print get_comment_link(); ?>-start" title="<?php esc_attr_e( 'Direct link to this comment', 'guardmaster' ); ?>">#</a></span>
		                <span class="edit"><?php edit_comment_link(esc_html__( 'Edit', 'guardmaster' ), '', '' ); ?></span>
						</div>
					</div>
					<?php $tt_reply_text = ''; $tt_reply_text = esc_html__( 'Reply', 'guardmaster' ); ?>
					<div class="button">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => $tt_reply_text,  'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- /.reply -->
					</div><!-- /.clearfix -->
				        <div class="comment-entry" id="comment-<?php comment_ID(); ?>">
							<?php comment_text(); ?>
							<?php if ( $comment->comment_approved == '0' ) { ?>
				                <p class='unapproved'><?php esc_html_e( 'Your comment is awaiting moderation.', 'guardmaster' ); ?></p>
				            <?php } ?>
						</div><!-- /comment-entry -->
			</div><!-- /.media-body -->
			</div>

	<?php
	} // End tt_gmaster_cust_cmnt()
}

// PINGBACK / TRACKBACK OUTPUT
if ( ! function_exists( 'tt_temptt_list_pings' ) ) {
	function tt_temptt_list_pings( $comment, $args, $depth ) {

		$GLOBALS['comment'] = $comment; ?>

		<li id="comment-<?php comment_ID(); ?>">
			<span class="author"><?php comment_author_link(); ?></span> -
			<span class="date"><?php print get_comment_date( get_option( 'date_format' ) ); ?></span>
			<span class="pingcontent"><?php comment_text(); ?></span>

	<?php
	} // End list_pings()
}

if ( ! function_exists( 'tt_temptt_commenter_link' ) ) {
	function tt_temptt_commenter_link() {
	    $commenter = get_comment_author_link();
	    print wp_kses_post($commenter);
	} // End tt_gmaster_commenter_link()
}



?>