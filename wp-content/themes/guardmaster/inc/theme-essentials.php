<?php
if ( ! defined( 'ABSPATH' ) ) exit;

$tt_temptt_opt = get_option( '$tt_temptt_opt' );

/*-----------------------------------------------------------------------------------*/
/* Theme essentials! */
/*-----------------------------------------------------------------------------------*/

update_option('revslider-notices', false);
set_transient( '_redux_activation_redirect', false, 30 );
delete_transient( '_dslc_activation_redirect_1' );


/**
 * Add default options and show Options Panel after activate
 * @since  4.0.0
 */
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) {
	// Flush rewrite rules.
	flush_rewrite_rules();
	// redirect
	$tt_update_log = get_option( 'tt_gmaster_updates_log');
	if( ! is_array($tt_update_log) ) tt_gmaster_activate_redirect(); // only redirect if its first time activation
}

// Adding redirect
function tt_gmaster_activate_redirect() {

	header( 'Location: ' . admin_url( 'themes.php?page=tgmpa-install-plugins' ) );

} // End tt_gmaster_activate_redirect()



// Adding versions
add_action( 'current_screen', 'tt_gmaster_update_version' );
function tt_gmaster_update_version( $current_screen ) {
	if ( 'appearance_page_tgmpa-install-plugins' == $current_screen->base ) {
		if( function_exists( 'tt_gmaster_firstInst_notice' )) add_action( 'admin_notices', 'tt_gmaster_firstInst_notice' ); // add notice.
	}
	if ( 'toplevel_page__templatation' == $current_screen->base ) {

		$woo_theme = wp_get_theme();
		$woo_this_theme_ver = $woo_theme->get( 'Version' );
		$theme_update_log = get_option( 'tt_gmaster_updates_log');

        if ( ! $theme_update_log ) $theme_update_log = array();

		// First update
		if ( ! in_array('1.0', $theme_update_log) ) {
			array_unshift($theme_update_log, '1.0');
			update_option( 'tt_gmaster_updates_log', $theme_update_log);
		}

		if ( ! in_array($woo_this_theme_ver, $theme_update_log) ) {
			array_unshift($theme_update_log, $woo_this_theme_ver);
			update_option( 'tt_gmaster_updates_log', $theme_update_log);
		}

	}
}

if( !function_exists( 'tt_gmaster_firstInst_notice' )) {
	function tt_gmaster_firstInst_notice() {

			 print '<div class="updated" style="padding: 25px 12px;"><span style="text-align:center;font-weight: bold;color:green;"> ' .
		     esc_html__( 'Thanks for Activating Guard Master WordPress theme.', 'guardmaster' ) . '</span>'
			 . '<br /> <br />' .
		     esc_html__( 'Theme requires few bundled plugins to function on its full power. Please Install and Activate plugins below.', 'guardmaster' ) . '</span>'
			 . '<br />' .
		     esc_html__( 'You can ofcourse choose not to install any particular plugin if you do not need it. ', 'guardmaster' ) . '</span>'
			 . '<br />' .
		     esc_html__( 'If you do not see any plugin listed below that means all required plugins are active and updated. If this is a first install, you can now move on to ThemeOptions/Demo Content on left menu to import demo content. ', 'guardmaster' ) . '</span>'
		     . '</div>';
	}
}


/**
 * Register Sidebars
 */

if ( ! function_exists( 'tt_gmaster_widgets_init' ) ) {
function tt_gmaster_widgets_init() {
    if ( ! function_exists( 'register_sidebar' ) )
        return;

    register_sidebar(array( 'name' => esc_html__( 'Sidebar ( Default )', 'guardmaster' ),'id' => 'default-sidebar','description' => esc_html__( 'Default sidebar.', 'guardmaster' ), 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h4 class="title">','after_title' => '</h4>'));

    // Footer widgetized areas
	$total = tt_temptt_get_option( 'footer_sidebars', 4 );
	if ( ! $total ) $total = 4;
	for ( $i = 1; $i <= intval( $total ); $i++ ) {
		register_sidebar( array( 'name' => sprintf( esc_html__( 'Footer %d', 'guardmaster' ), $i ), 'id' => sprintf( 'footer-%d', $i ), 'description' => sprintf( esc_html__( 'Widgetized Footer Region %d.', 'guardmaster' ), $i ), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h4 class="title">', 'after_title' => '</h4>' ) );
	}

} // End the_widgets_init()
}

add_action( 'widgets_init', 'tt_gmaster_widgets_init' );


if ( ! function_exists( 'tt_gmaster_load_scripts' ) ) {
	function tt_gmaster_load_scripts() {

		wp_enqueue_script( "comment-reply" );

		// Scripts
		if( tt_temptt_get_option('gmap_api') ) {
			$gmap_api = tt_temptt_get_option('gmap_api');
			wp_register_script( 'google-maps', '//maps.google.com/maps/api/js??sensor-true&key='.$gmap_api, '', null, true );
		} else
			wp_register_script( 'google-maps', '//maps.google.com/maps/api/js?sensor-true', '', null, true );
		wp_register_script( 'google-maps-markers', TT_TEMPTT_THEME_DIRURI . 'assets/js/markers.js', '', null, true );
		wp_enqueue_script( 'bootstrap', TT_TEMPTT_THEME_DIRURI . 'assets/js/bootstrap.min.js', array( 'jquery' ), null, true );
		//wp_enqueue_script( 'owl', TT_TEMPTT_THEME_DIRURI . 'assets/js/owl.carousel.min.js', '', null, true );
		wp_enqueue_script( 'appear', TT_TEMPTT_THEME_DIRURI . 'assets/js/jquery.appear.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'goog_maps', '//maps.google.com/maps/api/js', '', null, true );
		wp_enqueue_script( 'g_maps', TT_TEMPTT_THEME_DIRURI . 'assets/js/gmap.js', '', null, true );
		wp_enqueue_script( 'plugins', TT_TEMPTT_THEME_DIRURI . 'assets/js/plugins.min.js', '', null, true );
		wp_enqueue_script( 'main', TT_TEMPTT_THEME_DIRURI . 'assets/js/main.js', '', null, true );
		
		// Styles
		wp_enqueue_style( 'bootstrap', TT_TEMPTT_THEME_DIRURI . 'assets/css/bootstrap.min.css', '', null );
		wp_enqueue_style( 'style', TT_TEMPTT_THEME_DIRURI . 'assets/css/all-stylesheets.css', '', null );

		wp_enqueue_style( 'base', get_stylesheet_uri(), '', null );
		wp_enqueue_style( 'fontawesome', TT_TEMPTT_THEME_DIRURI . 'assets/css/font-awesome.min.css', '', null );

		// Fonts
		wp_enqueue_style( 'tt-fonts', tt_gmaster_g_fonts(), array(), null );

	} //tt_gmaster_load_scripts
	add_action( 'wp_enqueue_scripts', 'tt_gmaster_load_scripts' );
}

// calling google fonts needed for this theme.
if ( ! function_exists( 'tt_gmaster_g_fonts' ) ) {
	function tt_gmaster_g_fonts() {
		$font_url = '';

		/*
		Translators: If there are characters in your language that are not supported
		by chosen font(s), translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Google font: on or off', 'guardmaster' ) ) {
			$font_url = add_query_arg( 'family', urlencode( 'PT Sans:400,400italic,700,700italic|Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' ), "//fonts.googleapis.com/css" );
		}

		return $font_url;
	}
}

// admin styles.
if ( ! function_exists( 'tt_gmaster_admin_styles' ) ) {
	function tt_gmaster_admin_styles() {

		wp_enqueue_style( 'theme-admin-css', TT_TEMPTT_THEME_DIRURI . 'assets/css/tt-admin.css' );

	} add_action('admin_enqueue_scripts', 'tt_gmaster_admin_styles', 200);
}


/**
 * Pagination
 */

function tt_gmaster_post_pagination( $atts = NULL ) {

	$show_numbers = true;

	if ( ! $show_numbers ) {

		?>
			<div class="classic-pagination clearfix">

				<div class="fl">
					<?php previous_posts_link(); ?>
					&nbsp;
				</div>

				<div class="fr">
					&nbsp;
					<?php next_posts_link(); ?>
				</div>

			</div><!-- .classic-pagination -->
		<?php

	} else {

		global $paged;

		if ( ! isset( $atts['force_number'] ) ) $force_number = false; else $force_number = $atts['force_number'];
		if ( ! isset( $atts['pages'] ) ) $pages = false; else $pages = $atts['pages'];
		$range = 2;

		$showitems = ($range * 2)+1;

		if ( empty ( $paged ) ) { $paged = 1; }

		if ( $pages == '' ) {
			global $wp_query;
			$pages = $wp_query->max_num_pages;
			if( ! $pages ) {
				$pages = 1;
			}
		}

		if( 1 != $pages ) {

			?>
			<div class="page-pagination">
				<ul class="clearfix">
					<?php

						if($paged > 2 && $paged > $range+1 && $showitems < $pages) { print "<li class='inactive'><a class='num-type far-prev' href='".get_pagenum_link(1)."'></a></li>"; }
						if($paged > 1 && $showitems < $pages) { print "<li class='inactive'><a class='num-type prev' href='".get_pagenum_link($paged - 1)."' ></a></li>"; }

						for ($i=1; $i <= $pages; $i++){
							if (1 != $pages &&(!($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems)){
								print ($paged == $i)? "<li class='active'><a class='active num-type' href='".get_pagenum_link($i)."'>".$i."</a></li>":"<li class='inactive'><a class='num-type inactive' href='".get_pagenum_link($i)."'>".$i."</a></li>";
							}
						}

						if ($paged < $pages && $showitems < $pages) { print "<li class='inactive'><a class='num-type next' href='".get_pagenum_link($paged + 1)."'></a></li>"; }
						if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) { print "<li class='inactive'><a class='num-type far-next'  href='".get_pagenum_link($pages)."'></a></li>"; }

					?>
				</ul>
			</div><!-- .pagination --><?php
		}

	}

}

