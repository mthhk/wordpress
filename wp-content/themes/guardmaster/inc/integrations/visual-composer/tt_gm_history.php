<?php
/*
 * Templatation.com
 *
 * Banner with label slider for VC
 *
 */

function tt_gm_history_fn_vc() {
    vc_map( array(
			"icon" => 'tt-vc-block',
        'name'                    => esc_html__( 'History' , 'guardmaster' ),
        'base'                    => 'tt_gm_history_shortcode',
        'description'             => esc_html__( 'Show History in tabbed format with years as heading.', 'guardmaster' ),
        "category" => esc_html__('Guard Master', 'guardmaster'),
        'params' => array(
            array(
                'type' => 'param_group',
                'heading' => esc_html__( 'History', 'guardmaster' ),
                'param_name' => 'values',
                'params' => array(
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Year', 'guardmaster' ),
                        'param_name'  => 'year',
                        'value'       => '',
                        'description' => esc_html__( 'This appears as the clickable tab heading. Use Year or any other trigger for history.(like place name.)', 'guardmaster' ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__( 'Title', 'guardmaster' ),
                        'param_name'  => 'title',
                        'value'       => '',
                        'admin_label' => true,
                        'description' => esc_html__( 'Enter title.', 'guardmaster' ),
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__( 'Title color', 'guardmaster' ),
                        'description'     => esc_html__( 'leave blank for default.', 'guardmaster' ),
                        'param_name'  => 'titlecolor',
                        'value'       => '',
                    ),
                    array(
                        'type'        => 'textarea',
                        'heading'     => esc_html__( 'Info text', 'guardmaster' ),
                        'param_name'  => 'info',
                        'value'       => '',
                        'description' => esc_html__( 'description for this year', 'guardmaster' ),
                    ),
                    array(
                        'type'        => 'colorpicker',
                        'heading'     => esc_html__( 'Info color', 'guardmaster' ),
                        'description'     => esc_html__( 'leave blank for default.', 'guardmaster' ),
                        'param_name'  => 'infocolor',
                        'value'       => '',
                    ),
                ),
                'callbacks' => array(
                    'after_add' => 'vcChartParamAfterAddCallback'
                )
            ),

        ),
        'js_view' => 'VcIconElementView_Backend',
    ) );

}




add_action( 'vc_before_init', 'tt_gm_history_fn_vc' );

// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_tt_gm_history_shortcode extends WPBakeryShortCodesContainer {

    }
}

