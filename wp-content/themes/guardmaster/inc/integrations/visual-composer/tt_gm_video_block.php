<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */


function tt_gm_video_block_fn_vc() {

	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Video block", 'guardmaster'),
			"base" => "tt_gm_video_block_shortcode",
			'description' => esc_html__( 'Video block', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__( 'Background image', 'guardmaster' ),
                    'param_name' => 'image',
                    'value' => '',
                    'description' => esc_html__( 'Select image from media library.', 'guardmaster' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Title', 'guardmaster' ),
                    'param_name' => 'title',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Team member name.', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Subtitle', 'guardmaster' ),
                    'param_name' => 'subtitle',
                    'value' => '',
                    'description' => esc_html__( 'Team member position.', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Video link', 'guardmaster' ),
                    'param_name' => 'link',
                    'value' => '',
                    'description' => esc_html__( 'Enter link for video. This will open in a popup.', 'guardmaster' )
                ),
			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_video_block_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_video_block_shortcode extends WPBakeryShortCode {
	}
}