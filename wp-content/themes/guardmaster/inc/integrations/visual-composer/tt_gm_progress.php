<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */
function tt_gm_progress_fn_vc() {
	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Progress skills", 'guardmaster'),
			"base" => "tt_gm_progress_shortcode",
			'description' => esc_html__( 'Progress', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Skill name', 'guardmaster' ),
                    'param_name' => 'title',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Skill name.', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Progress number', 'guardmaster' ),
                    'param_name' => 'num',
                    'value' => '',
                    'description' => esc_html__( 'number', 'guardmaster' )
                ),
			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_progress_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_progress_shortcode extends WPBakeryShortCode {
	}
}