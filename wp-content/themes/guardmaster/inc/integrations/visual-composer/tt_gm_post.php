<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */


function tt_gm_post_fn_vc() {
	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("List post", 'guardmaster'),
			"base" => "tt_gm_post_shortcode",
			'description' => esc_html__( 'List posts beautifully in carousel.', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
            'params'          => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Number of posts', 'guardmaster' ),
                    'param_name'  => 'num_posts',
                    'value'       => '',
                    'description' => esc_html__( 'How many posts to display. enter number please, eg: 4, 6', 'guardmaster' ),
                ),
            )
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_post_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_post_shortcode extends WPBakeryShortCode {
	}
}