<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Button for VC *
 */

function tt_gm_info_block_fn_vc() {
	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Info block", 'guardmaster'),
			"base" => "tt_gm_info_block_shortcode",
			'description' => esc_html__( 'Info block with title, subtitle and desc.', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Type block', 'guardmaster' ),
                    'description'     => esc_html__( 'There are 2 type of title blocks. Type 1 is used mostly. Type 2 just adds icon to the box too. (icon settings appears on bottom of this same tab.)', 'guardmaster' ),
                    'param_name'  => 'type',
                    'value'       => array(
                        'Type 1'    =>  'type_1',
                        'Type 2'     =>  'type_2',
                    ),
                ) ,
				array(
                    'type'      => 'dropdown', 
                    'heading'     => esc_html__( 'First element', 'guardmaster' ),
                    'description'     => esc_html__( 'Select the first element of the block.', 'guardmaster' ),
                    'param_name'  => 'sort_1',
                    'value'       => array(
                        'None'    =>  'o-none',
                        'Subtitle'    =>  'o-subtitle',
                        'Title'     =>  'o-title',
                        'Separator' => 'o-separator',
                        'Description' => 'o-description',
                        'Teaser' => 'o-blockquote',
                        'Button' => 'o-button'
                    ),
                   ) ,
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Second element', 'guardmaster' ),
                    'description'     => esc_html__( 'Select the second element of the block.', 'guardmaster' ),
                    'param_name'  => 'sort_2',
                    'value'       => array(
                        'None'    =>  'o-none',
                        'Subtitle'    =>  'o-subtitle',
                        'Title'     =>  'o-title',
                        'Separator' => 'o-separator',
                        'Description' => 'o-description',
                        'Teaser' => 'o-blockquote',
                        'Button' => 'o-button'
                    ),),
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Third element', 'guardmaster' ),
                    'description'     => esc_html__( 'Select the third element of the block.', 'guardmaster' ),
                    'param_name'  => 'sort_3',
                    'value'       => array(
                        'None'    =>  'o-none',
                        'Subtitle'    =>  'o-subtitle',
                        'Title'     =>  'o-title',
                        'Separator' => 'o-separator',
                        'Description' => 'o-description',
                        'Teaser' => 'o-blockquote',
                        'Button' => 'o-button'
                    ),),
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Forth element ', 'guardmaster' ),
                    'description'     => esc_html__( 'Select the forth element of the block. Select none if you do not need this.', 'guardmaster' ),
                    'param_name'  => 'sort_4',
                    'value'       => array(
                        'None'    =>  'o-none',
                        'Subtitle'    =>  'o-subtitle',
                        'Title'     =>  'o-title',
                        'Separator' => 'o-separator',
                        'Description' => 'o-description',
                        'Teaser' => 'o-blockquote',
                        'Button' => 'o-button'
                    ),),
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Fifth element ', 'guardmaster' ),
                    'description'     => esc_html__( 'Select the fifth element of the block. Select none if you do not need this.', 'guardmaster' ),
                    'param_name'  => 'sort_5',
                    'value'       => array(
                        'None'    =>  'o-none',
                        'Subtitle'    =>  'o-subtitle',
                        'Title'     =>  'o-title',
                        'Separator' => 'o-separator',
                        'Description' => 'o-description',
                        'Teaser' => 'o-blockquote',
                        'Button' => 'o-button'
                    ),
                ),
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Sixth element ', 'guardmaster' ),
                    'description'     => esc_html__( 'Select the sixth element of the block. Select none if you do not need this.', 'guardmaster' ),
                    'param_name'  => 'sort_6',
                    'value'       => array(
                        'None'    =>  'o-none',
                        'Subtitle'    =>  'o-subtitle',
                        'Title'     =>  'o-title',
                        'Separator' => 'o-separator',
                        'Description' => 'o-description',
                        'Teaser' => 'o-blockquote',
                        'Button' => 'o-button'
                    ),
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Title', 'guardmaster' ),
                    'param_name'  => 'title',
                    'admin_label' => true,
                    "group" => "Title",
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Title font size', 'guardmaster' ),
                    'description'     => esc_html__( 'Title font size in px in px. Do not include px please, eg: 14', 'guardmaster' ),
                    'param_name'  => 'title_size',
                    "group" => "Title",
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Title bottom space', 'guardmaster' ),
                    'description'     => esc_html__( 'Title bottom space in px. Do not include px please, eg: 20', 'guardmaster' ),
                    'param_name'  => 'title_space',
                    "group" => "Title",
                    'value'       => '',
                ),
                array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => esc_html__("Title color",'guardmaster'),
					"param_name" => "title_color",
                    "group" => "Title",
					"value" => "",
			    ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Subtitle', 'guardmaster' ),
                    'param_name'  => 'subtitle',
                    'admin_label' => true,
                    "group" => "Subtitle",
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Subtitle font size', 'guardmaster' ),
                    'description'     => esc_html__( 'Subtitle font size in px. Do not include px please, eg: 14', 'guardmaster' ),
                    'param_name'  => 'sub_size',
                    "group" => "Subtitle",
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Subtitle bottom space', 'guardmaster' ),
                    'description'     => esc_html__( 'Subtitle bottom space in px. Do not include px please, eg: 14', 'guardmaster' ),
                    'param_name'  => 'sbtitle_space',
                    "group" => "Subtitle",
                    'value'       => '',
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Subtitle color",'guardmaster'),
                    "group" => "Subtitle",
                    "param_name" => "subtitle_color",
                    "value" => "",
                ),
                array(
                    'type'       => 'checkbox',
                    'heading'    => esc_html__( 'Make subtitle bold?', 'guardmaster' ),
                    'description'    => esc_html__( 'By default, subtitle is lighter than title, select if you want it in strong/bold font too.', 'guardmaster' ),
                    'param_name' => 'sub_bold',
                    "group" => "Subtitle",
                    "value" => false,
                ),
                array(
	            	"type" => "textarea_html",
                    'heading'     => esc_html__( 'Description', 'guardmaster' ),
                    "group" => "Description",
                    //'param_name'  => 'description',
	            	"param_name" => "content",
                ),
/*         array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => esc_html__( "Content", "my-text-domain" ),
            "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
            "value" => esc_html__( "<p>I am test text block. Click edit button to change this text.</p>", "my-text-domain" ),
            "description" => esc_html__( "Enter your content.", "my-text-domain" )
         ),*/
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Description font size', 'guardmaster' ),
                    'description'     => esc_html__( 'Description font size in px. Do not include px please, eg: 14', 'guardmaster' ),
                    'param_name'  => 'desc_size',
                    "group" => "Description",
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Description bottom space', 'guardmaster' ),
                    'description'     => esc_html__( 'In px. Do not include px please, eg: 20', 'guardmaster' ),
                    "group" => "Description",
                    'param_name'  => 'desc_space',
                    'value'       => '',
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Description color",'guardmaster'),
                    "group" => "Description",
                    "param_name" => "description_color",
                    "value" => "",
                ),
                array(
                    'type'        => 'textarea',
                    'heading'     => esc_html__( 'Teaser text', 'guardmaster' ),
                    'param_name'  => 'blq',
                    'value'       => '',
                    "group" => "Teaser",
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Teaser font size', 'guardmaster' ),
                    'description'     => esc_html__( 'Teaser font size in px. Do not include px please, eg: 14', 'guardmaster' ),
                    'param_name'  => 'teaser_size',
                    "group" => "Teaser",
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Teaser bottom space', 'guardmaster' ),
                    'description'     => esc_html__( 'In px. Do not include px please, eg: 20', 'guardmaster' ),
                    'param_name'  => 'blq_space',
                    'value'       => '',
                    "group" => "Teaser",
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Teaser color",'guardmaster'),
                    "param_name" => "blq_color",
                    "value" => "",
                    "group" => "Teaser",
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Separator bottom space.', 'guardmaster' ),
                    'description'     => esc_html__( 'In px. Do not include px please, eg: 20', 'guardmaster' ),
                    'param_name'  => 'sp_space',
                    'value'       => '',
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Button Text', 'guardmaster' ),
                    'param_name'  => 'b_title',
                    'value'       => '',
                    "group" => "Button",
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Button link', 'guardmaster' ),
                    'param_name'  => 'link',
                    'value'       => '',
                    "group" => "Button",
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Button text color",'guardmaster'),
                    "param_name" => "txt_color",
                    "value" => "",
                    "group" => "Button",
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Button background color', 'guardmaster' ),
                    'param_name'  => 'btn_bg',
                    'value'       => '',
                    "group" => "Button",
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Border color', 'guardmaster' ),
                    'param_name'  => 'border',
                    'value'       => '',
                    "group" => "Button",
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Button bottom space', 'guardmaster' ),
                    'description'     => esc_html__( 'In px. Do not include px please, eg: 20', 'guardmaster' ),
                    'param_name'  => 'btn_space',
                    'value'       => '',
                    'dependency'  => array( 'element' => 'line', 'value' => array('yes') ),
                    "group" => "Button",
                ),
/*                array(
                    'type'       => 'checkbox',
                    'heading'    => esc_html__( 'Insert icon?', 'guardmaster' ),
                    'description'    => esc_html__( 'Insert icon on top or left side of block. Once checked, other settings will appear in Icon tab.', 'guardmaster' ),
                    'param_name' => 'add_icon',
                    'value'      => array( esc_html__( 'Yes, please', 'guardmaster' ) => 'yes' ),
                    'dependency'  => array( 'element' => 'type', 'value' => array('type_2') )
                ),*/
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Icon', 'guardmaster' ),
                    'description' => esc_html__( 'Please enter theme specific icon class. You can use fontawesome or flaticons. . Note that if you enter anything here, the below icon selection box will not work.', 'guardmaster' ),
                    'param_name' => 'custom_icon',
                    'value' => '',
                    'dependency'  => array( 'element' => 'type', 'value' => array('type_2') )
                ),
                array(
                    'type'        => 'iconpicker',
                    'heading'     => esc_html__( 'Type icon', 'guardmaster' ),
                    'param_name'  => 'type_icon',
                    'value'       => '',
                    'settings' => array(
                        'emptyIcon' => false,
                        'iconsPerPage' => 300,
                    ),
                    'dependency'  => array( 'element' => 'type', 'value' => array('type_2') )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Icon color', 'guardmaster' ),
                    'param_name'  => 'icon_color',
                    'value'       => '',
                    'dependency'  => array( 'element' => 'type', 'value' => array('type_2') )
                ),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Content Alignment", 'guardmaster'),
					"param_name" => "alignment",
					"value" => array(
						'Left' => 'left',
						'Center' => 'center',
						'Right' => 'right',
					)
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'guardmaster' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'guardmaster' ),
				),
	            array(
	                'type' => 'css_editor',
	                'heading' => esc_html__( 'CSS box', 'guardmaster' ),
	                'param_name' => 'css',
	                'group' => esc_html__( 'Design Options', 'guardmaster' )
	            )

			)
		)
	);

}
add_action( 'vc_before_init', 'tt_gm_info_block_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_info_block_shortcode extends WPBakeryShortCode {
	}
}