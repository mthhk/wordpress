<?php
/*
 * Templatation.com
 *
 * Banner with label slider for VC
 *
 */

function tt_gm_list_content_fn_vc() {
    vc_map(
        array(
			"icon" => 'tt-vc-block',
            'name'                    => esc_html__( 'Benefits list' , 'guardmaster' ),
            'base'                    => 'tt_gm_list_content_shortcode',
            'description'             => esc_html__( 'Benefits list', 'guardmaster' ),
            'as_parent'               => array('only' => 'tt_gm_list_content_item_shortcode'),
            'content_element'         => true,
            "js_view" => 'VcColumnView',
            "category" => esc_html__('Guard Master', 'guardmaster'),
            "params" => array(
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Select positioning', 'guardmaster' ),
                    'param_name'  => 'list_content_type',
                    'value'       => array(
                        'Left'    =>  'style_1',
                        'Right'     =>  'style_2'
                    )
                ),
            ),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_list_content_fn_vc' );
// Nested Element
function tt_gm_list_content_item_fn_vc() {
    vc_map(
        array(
            'name'            => esc_html__('Item list', 'guardmaster'),
            'base'            => 'tt_gm_list_content_item_shortcode',
            'description'     => esc_html__( 'Item list', 'guardmaster' ),
            "category" => esc_html__('Guard Master', 'guardmaster'),
            'content_element' => true,
            'as_child'        => array('only' => 'tt_gm_list_content_shortcode'), // Use only|except attributes to limit parent (separate multiple values with comma)
            'params'          => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Name item', 'guardmaster' ),
                    'param_name'  => 'title',
                    'value'       => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Name item', 'guardmaster' ),
                ),
                array(
                    'type'        => 'textarea',
                    'heading'     => esc_html__( 'Info text', 'guardmaster' ),
                    'param_name'  => 'info',
                    'value'       => '',
                    'description' => esc_html__( 'Info text', 'guardmaster' ),
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Benefits color', 'guardmaster' ),
                    'param_name'  => 'itemcolor',
                    'value'       => '',
                ),
            ),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_list_content_item_fn_vc' );

// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_tt_gm_list_content_shortcode extends WPBakeryShortCodesContainer {

    }
}

// Replace Wbc_Inner_Item with your base name from mapping for nested element
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_tt_gm_list_content_item_shortcode extends WPBakeryShortCode {

    }
}