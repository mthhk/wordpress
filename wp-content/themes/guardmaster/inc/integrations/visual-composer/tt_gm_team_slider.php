<?php
/*
 * Templatation.com
 *
 * Banner with label slider for VC
 *
 */

function tt_gm_team_slider_fn_vc() {
    vc_map(
        array(
			"icon" => 'tt-vc-block',
            'name'                    => esc_html__( 'Team slider' , 'guardmaster' ),
            'base'                    => 'tt_gm_team_slider_shortcode',
            'description' => esc_html__( 'Team slider', 'guardmaster' ),
            'as_parent'               => array('only' => 'tt_gm_team_slider_item_shortcode'),
            'content_element'         => true,
            "js_view" => 'VcColumnView',
            "category" => esc_html__('Guard Master', 'guardmaster'),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_team_slider_fn_vc' );
// Nested Element
function tt_gm_team_slider_item_fn_vc() {
    vc_map(
        array(
            'name'            => esc_html__('Team member', 'guardmaster'),
            'base'            => 'tt_gm_team_slider_item_shortcode',
            'description'     => esc_html__( 'Team member', 'guardmaster' ),
            "category" => esc_html__('Guard Master', 'guardmaster'),
            'content_element' => true,
            'as_child'        => array('only' => 'tt_gm_team_slider_shortcode'), // Use only|except attributes to limit parent (separate multiple values with comma)
            'params'          => array(
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__( 'Team member image', 'guardmaster' ),
                    'param_name' => 'image',
                    'value' => '',
                    'description' => esc_html__( 'Select image from media library.', 'guardmaster' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Name', 'guardmaster' ),
                    'param_name' => 'name',
                    'admin_label' => true,
                    'value' => '',
                    'description' => esc_html__( 'Team member name.', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Position', 'guardmaster' ),
                    'param_name' => 'position',
                    'value' => '',
                    'description' => esc_html__( 'Team member position.', 'guardmaster' )
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Facebook', 'guardmaster' ),
                    'param_name'  => 'social_fb',
                    'value'       => '',
                    'description' => esc_html__( 'Enter facebook social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Instagram', 'guardmaster' ),
                    'param_name'  => 'social_in',
                    'value'       => '',
                    'description' => esc_html__( 'Enter instagram social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Linkedin', 'guardmaster' ),
                    'param_name'  => 'social_li',
                    'value'       => '',
                    'description' => esc_html__( 'Enter linkedin social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Twitter', 'guardmaster' ),
                    'param_name'  => 'social_tw',
                    'value'       => '',
                    'description' => esc_html__( 'Enter twitter social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Google', 'guardmaster' ),
                    'param_name'  => 'social_google',
                    'value'       => '',
                    'description' => esc_html__( 'Enter google social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),
            ),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_team_slider_item_fn_vc' );

// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_tt_gm_team_slider_shortcode extends WPBakeryShortCodesContainer {

    }
}

// Replace Wbc_Inner_Item with your base name from mapping for nested element
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_tt_gm_team_slider_item_shortcode extends WPBakeryShortCode {

    }
}