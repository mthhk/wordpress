<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */
function tt_gm_team_fn_vc() {
	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Team", 'guardmaster'),
			"base" => "tt_gm_team_shortcode",
			'description' => esc_html__( 'Different types of team member block.', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__( 'Team member image', 'guardmaster' ),
                    'param_name' => 'image',
                    'value' => '',
                    'description' => esc_html__( 'Select image from media library.', 'guardmaster' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Name', 'guardmaster' ),
                    'param_name' => 'name',
                    'value' => '',
                    'description' => esc_html__( 'Team member name.', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Position', 'guardmaster' ),
                    'param_name' => 'position',
                    'value' => '',
                    'description' => esc_html__( 'Team member position.', 'guardmaster' )
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Facebook', 'guardmaster' ),
                    'param_name'  => 'social_fb',
                    'value'       => '',
                    'description' => esc_html__( 'Enter facebook social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Instagram', 'guardmaster' ),
                    'param_name'  => 'social_in',
                    'value'       => '',
                    'description' => esc_html__( 'Enter instagram social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Linkedin', 'guardmaster' ),
                    'param_name'  => 'social_li',
                    'value'       => '',
                    'description' => esc_html__( 'Enter linkedin social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Twitter', 'guardmaster' ),
                    'param_name'  => 'social_tw',
                    'value'       => '',
                    'description' => esc_html__( 'Enter twitter social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Google', 'guardmaster' ),
                    'param_name'  => 'social_google',
                    'value'       => '',
                    'description' => esc_html__( 'Enter google social link url.', 'guardmaster' ),
                    'group'       => 'Social URL'
                ),
                 
			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_team_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_team_shortcode extends WPBakeryShortCode {
	}
}