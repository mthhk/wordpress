<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Button for VC *
 */

function tt_gm_button_fn_vc() {
	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Button", 'guardmaster'),
			"base" => "tt_gm_button_shortcode",
			'description' => esc_html__( 'Few types of btn.', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
				array(
                    'type'      => 'dropdown', 
                    'heading'     => esc_html__( 'Select type of button', 'guardmaster' ),
                    'param_name'  => 'btn_type',
                    'value'       => array(
                        'Normal button'    =>  'btn_normal',
                        'Big button'     =>  'btn_big',
                    )
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Text button', 'guardmaster' ),
                    'param_name'  => 'title',
                    'admin_label' => true,
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Button link', 'guardmaster' ),
                    'param_name'  => 'link',
                    'value'       => '',
                ),
                array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => esc_html__("Text button color",'guardmaster'),
					"param_name" => "txt_color",
					"value" => "",
			    ),
			    array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Button background color', 'guardmaster' ),
                    'param_name'  => 'btn_bg',
                    'value'       => '',
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Border color', 'guardmaster' ),
                    'param_name'  => 'border',
                    'value'       => '',
                ),
                array(
                    'type'       => 'checkbox',
                    'heading'    => esc_html__( 'Insert icon?', 'guardmaster' ),
                    'description'    => esc_html__( 'Insert default right arrow icon on button.', 'guardmaster' ),
                    'param_name' => 'add_icon',
                    'value'      => array( esc_html__( 'Yes, please', 'guardmaster' ) => 'yes' )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Icon color', 'guardmaster' ),
                    'param_name'  => 'icon_color',
                    'value'       => '',
                    'dependency'  => array( 'element' => 'add_icon', 'value' => array('yes') )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Icon background', 'guardmaster' ),
                    'param_name'  => 'icon_bg',
                    'value'       => '',
                    'dependency'  => array( 'element' => 'add_icon', 'value' => array('yes') )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Icon border color', 'guardmaster' ),
                    'param_name'  => 'border_icon',
                    'value'       => '',
                    'dependency'  => array( 'element' => 'add_icon', 'value' => array('yes') )
                ),
			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_button_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_button_shortcode extends WPBakeryShortCode {
	}
}