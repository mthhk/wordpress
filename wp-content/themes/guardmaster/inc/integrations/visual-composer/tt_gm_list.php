<?php
/*
 * Templatation.com
 *
 * Banner with label slider for VC
 *
 */

function tt_gm_list_fn_vc() {
    vc_map(
        array(
			"icon" => 'tt-vc-block',
            'name'                    => esc_html__( 'List' , 'guardmaster' ),
            'base'                    => 'tt_gm_list_shortcode',
            'description' => esc_html__( 'List types', 'guardmaster' ),
            'as_parent'               => array('only' => 'tt_gm_list_item_shortcode'),
            'content_element'         => true,
            "js_view" => 'VcColumnView',
            "category" => esc_html__('Guard Master', 'guardmaster'),
            "params" => array(
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Select type of list', 'guardmaster' ),
                    'param_name'  => 'list_type',
                    'value'       => array(
                        'List with icon'    =>  'style_1',
                        'List with Number'     =>  'style_2'
                    )
                ),
            ),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_list_fn_vc' );
// Nested Element
function tt_gm_list_item_fn_vc() {
    vc_map(
        array(
            'name'            => esc_html__('Item list', 'guardmaster'),
            'base'            => 'tt_gm_list_item_shortcode',
            'description'     => esc_html__( 'Item list', 'guardmaster' ),
            "category" => esc_html__('Guard Master', 'guardmaster'),
            'content_element' => true,
            'as_child'        => array('only' => 'tt_gm_list_shortcode'), // Use only|except attributes to limit parent (separate multiple values with comma)
            'params'          => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Name item', 'guardmaster' ),
                    'param_name'  => 'title',
                    'value'       => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Name item', 'guardmaster' ),
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Item color', 'guardmaster' ),
                    'param_name'  => 'itemcolor',
                    'value'       => '',
                ),
                array(
                    'type'        => 'iconpicker',
                    'heading'     => esc_html__( 'Type icon', 'guardmaster' ),
                    'param_name'  => 'type_icon',
                    'value'       => '',
                    'settings' => array(
                        'emptyIcon' => false,
                        'iconsPerPage' => 300,
                    ),
                ),
                 array(
                     'type'        => 'colorpicker',
                     'heading'     => esc_html__( 'Icon color', 'guardmaster' ),
                     'param_name'  => 'iconcolor',
                     'value'       => '',
                 ),
            ),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_list_item_fn_vc' );

// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_tt_gm_list_shortcode extends WPBakeryShortCodesContainer {

    }
}

// Replace Wbc_Inner_Item with your base name from mapping for nested element
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_tt_gm_list_item_shortcode extends WPBakeryShortCode {

    }
}