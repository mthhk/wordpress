<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */


function tt_gm_count_fn_vc() {
	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Count box", 'guardmaster'),
			"base" => "tt_gm_count_shortcode",
			'description' => esc_html__( 'Count box', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Title', 'guardmaster' ),
                    'param_name' => 'title',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Title', 'guardmaster' )
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Title  color",'guardmaster'),
                    "param_name" => "txt_color",
                    "value" => "",
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Number', 'guardmaster' ),
                    'param_name' => 'number',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Number', 'guardmaster' )
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Number  color",'guardmaster'),
                    "param_name" => "num_color",
                    "value" => "",
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Bottom arrow color",'guardmaster'),
                    "param_name" => "arrow_color",
                    "value" => "",
                ),
			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_count_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_count_shortcode extends WPBakeryShortCode {
	}
}