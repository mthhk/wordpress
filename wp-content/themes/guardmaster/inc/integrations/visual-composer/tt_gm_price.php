<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */


function tt_gm_price_fn_vc() {

	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Pricing plan", 'guardmaster'),
			"base" => "tt_gm_price_shortcode",
			'description' => esc_html__( 'Pricing plan', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Select style price', 'guardmaster' ),
                    'param_name'  => 'price_type',
                    'value'       => array(
                        'Style 1'    =>  'style_1',
                        'Style 2'     =>  'style_2',
                    )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Name plan', 'guardmaster' ),
                    'param_name' => 'title',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Name plan', 'guardmaster' )
                ),
                array(
                    'type'       => 'checkbox',
                    'heading'    => esc_html__( 'Create as popular plan?', 'guardmaster' ),
                    'param_name' => 'popular',
                    'value'      => array( esc_html__( 'Yes, please', 'guardmaster' ) => 'yes' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Info plan', 'guardmaster' ),
                    'param_name' => 'info',
                    'value' => '',
                    'description' => esc_html__( 'This info appears below the Title. plan', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Price currency', 'guardmaster' ),
                    'param_name' => 'currency',
                    'value' => '',
                    'description' => esc_html__( 'eg $ ', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Price Timing', 'guardmaster' ),
                    'param_name' => 'timing',
                    'value' => '',
                    'description' => esc_html__( 'eg monthly/yearly', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Price plan', 'guardmaster' ),
                    'param_name' => 'price',
                    'value' => '',
                    'description' => esc_html__( 'Price of plan, eg 99', 'guardmaster' )
                ),
                array(
                    'type' => 'textarea_html',
                    'heading' => esc_html__( 'Detail plan', 'guardmaster' ),
                    'param_name' => 'content',
                    'value' => '',
                    'description' => esc_html__( 'Please create bulleted list', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Text btn', 'guardmaster' ),
                    'param_name' => 'text',
                    'value' => '',
                    'description' => esc_html__( 'Text', 'guardmaster' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Link btn', 'guardmaster' ),
                    'param_name' => 'link',
                    'value' => '',
                    'description' => esc_html__( 'Link', 'guardmaster' )
                ),

			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_price_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_price_shortcode extends WPBakeryShortCode {
	}
}