<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1'); }
/*
 * Templatation.com
 *
 * VC integration
 *
 */

// Set VC as theme.
if( function_exists('vc_set_as_theme') ){
	function tt_temptt_vcAsTheme() {
		vc_set_as_theme(true);
	}
	add_action( 'vc_before_init', 'tt_temptt_vcAsTheme');
}

// Initialize VC modules.
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_button.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_info_block.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_icon_box.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_team.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_full_image.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_headline.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_list.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_list_content.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_history.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_brands.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_video_block.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_message.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_progress.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_accordion.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_price.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_count.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_title.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_testimonials.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_post.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_gm_team_slider.php';
include_once TT_TEMPTT_THEME_DIR. 'inc/integrations/visual-composer/tt_vc_hero_block.php';

if ( class_exists( 'woocommerce' ) ) {
	include_once TT_TEMPTT_THEME_DIR . 'inc/integrations/visual-composer/tt_gm_list_products_block.php';
}