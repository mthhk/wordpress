<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Social Accordion for VC
 *
 */
function tt_gm_icon_box_fn_vc() {

	$target_arr = array(
		esc_html__( 'Same window', 'guardmaster' ) => '_self',
		esc_html__( 'New window', 'guardmaster' ) => '_blank',
	);

	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Icon Box", 'guardmaster'),
			"base" => "tt_gm_icon_box_shortcode",
			'description' => esc_html__( 'Icon box with title, subtitle and desc.', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Insert Graphic", 'guardmaster'),
					'description' => esc_html__( 'Do you want to insert image or icon on this infobox. Icon settings can be done in Icon tab.', 'guardmaster' ),
					"param_name" => "insert_graphic",
					"value" => array(
						'No' => 'no',
						'Icon' => 'icon',
					)
				),

// icon params
				array(
					'type' => 'textarea',
					'heading' => esc_html__( 'Specific Icon', 'guardmaster' ),
					'description' => esc_html__( 'This theme comes with some pre-built icons. Please enter theme specific icon class(flaticons). Note that if you enter anything here, the below icon selection box will not work.', 'guardmaster' ),
					'param_name' => 'custom_icon',
					'value' => '',
					'group' => 'Icon',
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Icon library', 'guardmaster' ),
					'value' => array(
						esc_html__( 'Font Awesome', 'guardmaster' ) => 'fontawesome',
						esc_html__( 'Open Iconic', 'guardmaster' ) => 'openiconic',
						esc_html__( 'Typicons', 'guardmaster' ) => 'typicons',
						esc_html__( 'Entypo', 'guardmaster' ) => 'entypo',
						esc_html__( 'Linecons', 'guardmaster' ) => 'linecons',
					),
					'param_name' => 'type',
					'description' => esc_html__( 'Select icon library.', 'guardmaster' ),
					'group' => 'Icon',
					'dependency' => array(
						'element' => 'insert_graphic',
						'value' => array('icon')
					),
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Icon', 'guardmaster' ),
					'param_name' => 'icon_fontawesome',
					'value' => 'fa fa-adjust', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false,
						// default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'fontawesome',
					),
					'description' => esc_html__( 'Select icon from library.', 'guardmaster' ),
					'group' => 'Icon'
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Icon', 'guardmaster' ),
					'param_name' => 'icon_openiconic',
					'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false, // default true, display an "EMPTY" icon?
						'type' => 'openiconic',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'openiconic',
					),
					'description' => esc_html__( 'Select icon from library.', 'guardmaster' ),
					'group' => 'Icon'
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Icon', 'guardmaster' ),
					'param_name' => 'icon_typicons',
					'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false, // default true, display an "EMPTY" icon?
						'type' => 'typicons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'typicons',
					),
					'description' => esc_html__( 'Select icon from library.', 'guardmaster' ),
					'group' => 'Icon'
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Icon', 'guardmaster' ),
					'param_name' => 'icon_entypo',
					'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false, // default true, display an "EMPTY" icon?
						'type' => 'entypo',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'entypo',
					),
					'group' => 'Icon'
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Icon', 'guardmaster' ),
					'param_name' => 'icon_linecons',
					'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false, // default true, display an "EMPTY" icon?
						'type' => 'linecons',
						'iconsPerPage' => 4000, // default 100, how many icons per/page to display
					),
					'dependency' => array(
						'element' => 'type',
						'value' => 'linecons',
					),
					'description' => esc_html__( 'Select icon from library.', 'guardmaster' ),
					'group' => 'Icon'
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Icon color', 'guardmaster' ),
					'param_name' => 'icon_color',
					'value' => array_merge( getVcShared( 'colors' ), array( esc_html__( 'Custom color', 'guardmaster' ) => 'custom' ) ),
					'description' => esc_html__( 'Select icon color.', 'guardmaster' ),
					'param_holder_class' => 'vc_colored-dropdown',
					'group' => 'Icon',
					'dependency' => array(
						'element' => 'insert_graphic',
						'value' => array('icon')
					),
				),
				array(
					'type' => 'colorpicker',
					'heading' => esc_html__( 'Custom color', 'guardmaster' ),
					'param_name' => 'icon_custom_color',
					'description' => esc_html__( 'Select custom icon color.', 'guardmaster' ),
					'dependency' => array(
						'element' => 'icon_color',
						'value' => 'custom',
					),
					'group' => 'Icon'
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Background shape', 'guardmaster' ),
					'param_name' => 'background_style',
					'value' => array(
						esc_html__( 'None', 'guardmaster' ) => '',
						esc_html__( 'Circle', 'guardmaster' ) => 'rounded',
						esc_html__( 'Square', 'guardmaster' ) => 'boxed',
						esc_html__( 'Rounded', 'guardmaster' ) => 'rounded-less',
						esc_html__( 'Outline Circle', 'guardmaster' ) => 'rounded-outline',
						esc_html__( 'Outline Square', 'guardmaster' ) => 'boxed-outline',
						esc_html__( 'Outline Rounded', 'guardmaster' ) => 'rounded-less-outline',
					),
					'description' => esc_html__( 'Select background shape and style for icon.', 'guardmaster' ),
					'group' => 'Icon',
					'dependency' => array(
						'element' => 'insert_graphic',
						'value' => array('icon')
					),
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Background color', 'guardmaster' ),
					'param_name' => 'background_color',
					'value' => array_merge( getVcShared( 'colors' ), array( esc_html__( 'Custom color', 'guardmaster' ) => 'custom' ) ),
					'std' => 'grey',
					'description' => esc_html__( 'Select background color for icon.', 'guardmaster' ),
					'param_holder_class' => 'vc_colored-dropdown',
					'dependency' => array(
						'element' => 'background_style',
						'not_empty' => true,
					),
					'group' => 'Icon'
				),
				array(
					'type' => 'colorpicker',
					'heading' => esc_html__( 'Custom background color', 'guardmaster' ),
					'param_name' => 'custom_background_color',
					'description' => esc_html__( 'Select custom icon background color.', 'guardmaster' ),
					'dependency' => array(
						'element' => 'background_color',
						'value' => 'custom',
					),
					'group' => 'Icon'
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Size', 'guardmaster' ),
					'param_name' => 'size',
					'value' => array_merge( getVcShared( 'sizes' ), array( 'Extra Large' => 'xl' ) ),
					'std' => 'md',
					'description' => esc_html__( 'Icon size.', 'guardmaster' ),
					'group' => 'Icon',
					'dependency' => array(
						'element' => 'insert_graphic',
						'value' => array('icon')
					),
				),
// end of Icon params


				array(
					"type" => "dropdown",
					"heading" => esc_html__("Heading type", 'guardmaster'),
					'description' => esc_html__( 'This also determines the size of text for both heading/subheading. There is also SEO benefits of using heading tags. H1 is biggest in fontsize, and in important. Recommended/default: h4', 'guardmaster' ),
					"param_name" => "title_tag",
					"value" => array(
						'default' => 'default',
						'h1' => 'h1',
						'h2' => 'h2',
						'h3' => 'h3',
						'h4' => 'h4',
						'h5' => 'h5',
						'h6' => 'h6',
						'div' => 'div',
					)
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'guardmaster'),
					"description" => esc_html__("The above tag will wrap around this title.", 'guardmaster'),
					"param_name" => "title",
					"admin_label" => true,
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Title Text Transform", 'guardmaster'),
					'description' => esc_html__( 'Text transform for title tag. Recommended: none', 'guardmaster' ),
					"param_name" => "title_text_transform",
					"value" => array(
						'None' => 'none',
						'Capitalize' => 'capitalize',
						'Uppercase' => 'uppercase',
						'Lowercase' => 'lowercase',
					)
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Subtitle", 'guardmaster'),
					"description" => esc_html__("Optional Subtitle.", 'guardmaster'),
					"param_name" => "subtitle",
				),
				array(
					"type" => "checkbox",
					"class" => "",
					"heading" => esc_html__("Insert Link on title ?", 'guardmaster'),
					"param_name" => "ins_title_link",
					"value" => false,
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title link", 'guardmaster'),
					"param_name" => "title_link",
					"description" => esc_html__("Enter link for title.", 'guardmaster'),
					'dependency' => array(
						'element' => 'ins_title_link',
						'value' => array('true')
					),
				),
				array(
					"type" => "textarea_html",
					"heading" => esc_html__("Paragraph", 'guardmaster'),
					"description" => esc_html__("Paragraph for this info box. ", 'guardmaster'),
					"param_name" => "content",
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Content Alignment", 'guardmaster'),
					"param_name" => "alignment",
					"value" => array(
						'Left' => 'left',
						'Center' => 'center',
						'Right' => 'right',
					)
				),
				array(
					"type" => "checkbox",
					"class" => "",
					"heading" => esc_html__("Insert Button", 'guardmaster'),
					"param_name" => "ins_button",
					"value" => false,
					"description" => esc_html__("Do you want to insert button in this info box ?", 'guardmaster')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Button Text", 'guardmaster'),
					"param_name" => "button_text",
					'dependency' => array(
						'element' => 'ins_button',
						'value' => array('true')
					),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Button link", 'guardmaster'),
					"param_name" => "button_link",
					'dependency' => array(
						'element' => 'ins_button',
						'value' => array('true')
					),
				),
/*				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Link Target', 'guardmaster' ),
					'param_name' => 'button_link_target',
					"value" => array(
						'Same Window' => '_self',
						'New Window' => '_blank',
					),
					'dependency' => array(
						'element' => 'ins_button',
						'value' => array('true')
					),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Text Appearance", 'guardmaster'),
					"description" => esc_html__("Based on the color of background image, you might need to make text color Light or Dark.", 'guardmaster'),
					"param_name" => "text_appear",
					"value" => array(
						'Dark' => 'dark',
						'Light' => 'light',
					)
				),*/
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'guardmaster' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'guardmaster' ),
				),
				array(
					'type' => 'css_editor',
					'heading' => esc_html__( 'Css', 'guardmaster' ),
					'param_name' => 'css',
					'group' => esc_html__( 'Design options', 'guardmaster' ),
				),

			)
		)
	);

}
add_action( 'vc_before_init', 'tt_gm_icon_box_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_icon_box_shortcode extends WPBakeryShortCode {
	}
}