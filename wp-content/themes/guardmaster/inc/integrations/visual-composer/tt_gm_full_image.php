<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */


function tt_gm_full_image_fn_vc() {

	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Full with image", 'guardmaster'),
			"base" => "tt_gm_full_image_shortcode",
			'description' => esc_html__( 'Full image', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__( 'Full with image', 'guardmaster' ),
                    'param_name' => 'image',
                    'value' => '',
                    'description' => esc_html__( 'Select image from media library.', 'guardmaster' ),
                ),
			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_full_image_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_full_image_shortcode extends WPBakeryShortCode {
	}
}