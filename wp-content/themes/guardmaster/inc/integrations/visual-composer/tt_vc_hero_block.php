<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Hero block sc for VC
 *
 */
function tt_hero_shortcode_fn_vc() {
	

	vc_map( 
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("GM Hero Area", 'guardmaster'),
			"base" => "tt_hero_shortcode",
			'description' => esc_html__( 'Highlighted section with BG image and text.', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => esc_html__("Heading", 'guardmaster'),
					"description" => esc_html__("Heading for Hero area.", 'guardmaster'),
					"admin_label" => true,
					"param_name" => "heading",
				),
/*				array(
					"type" => "textarea_html",
					"heading" => esc_html__("Subheading", 'guardmaster'),
					"description" => esc_html__("Subheading for Hero area.", 'guardmaster'),
					"param_name" => "content",
				),*/
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Hero Area Background Image", 'guardmaster'),
					"description" => esc_html__("Choose image for background. This image is set to cover mode so please check image size. recommended size is 1200x400px, or any landscape size.", 'guardmaster'),
					"param_name" => "image"
				),
				array(
					"type" => "colorpicker",
					"heading" => esc_html__("Background color", 'guardmaster'),
					"description" => esc_html__("You can choose to have background color, also, if you make this color transparent, it will work as overlay color on the image chosen above.", 'guardmaster'),
					"param_name" => "color"
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Text Appearance", 'guardmaster'),
					"description" => esc_html__("Based on the color of background image, you might need to make text color Light or Dark. Note: Button appearance is being managed from Button tab.", 'guardmaster'),
					"param_name" => "text_appear",
					"value" => array(
						'Dark' => 'dark',
						'Light' => 'light',
					)
				),
				array(
					"type" => "checkbox",
					"class" => "",
					"heading" => esc_html__("Insert Breadcrumb", 'guardmaster' ),
					"param_name" => "yoast_bdcmp",
					"value" => "",
					"description" => esc_html__("Insert breadcrumb in this area. Note: Yoast SEO plugin must be active, and Breadcrumb enabled in SEO/Advanced in wp-admin.", 'guardmaster' ),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Top Space", 'guardmaster'),
	                'description' => esc_html__( 'In-case you want to display big BG image. With block of content aligned in top, you can enter blank space you want on bottom of content. Enter only integer, without px, like 200,400. Note: This will not work if you did not set BG image & inserted padding in Design Options tab.', 'guardmaster' ),
					"param_name" => "block_padding_top",
					"std" => "80",
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Bottom Space", 'guardmaster'),
	                'description' => esc_html__( 'In-case you want to display big BG image. With block of content aligned bottom, you can enter blank space you want on top of content. Enter only integer, without px, like 200,400. Note: This will not work if you did not set BG image & inserted padding in Design Options tab.', 'guardmaster' ),
					"param_name" => "block_padding_bottom",
					"std" => "80",
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra Class", 'guardmaster'),
	                'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'guardmaster' ),
					"param_name" => "el_class",
				),
	            array(
	                'type' => 'css_editor',
	                'heading' => esc_html__( 'CSS box', 'guardmaster' ),
	                'param_name' => 'css',
	                // 'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'guardmaster' ),
	                'group' => esc_html__( 'Design Options', 'guardmaster' )
	            )
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'tt_hero_shortcode_fn_vc' );


if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_hero_shortcode extends WPBakeryShortCode {
	}
}