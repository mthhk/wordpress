<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Social block sc for VC
 *
 */
if( !function_exists( 'tt_temptt_list_products_shortcode_fn_vc' ) ) {
	function tt_temptt_list_products_shortcode_fn_vc() {

		// Get categories
		$cats         = get_categories();
		$cats_choices = array();

		// Generate usable array of categories
		foreach ( $cats as $cat ) {
			$cats_choices[ $cat->name ] = $cat->name;
		}

		vc_map(
			array(
				'name'             => esc_html__( 'List Products', 'guardmaster' ),
				'base'             => 'tt_gm_list_products_shortcode',
				"icon"              => 'tt-vc-block',
				"category"         => esc_html__( 'Guard Master', 'guardmaster' ),
				'description'      => esc_html__( 'List products in tab format with carousel', 'guardmaster' ),
				'admin_enqueue_js' => array( TT_TEMPTT_THEME_DIRURI . 'assets/js/owlcarousel.js', ),
				'params'           => array(
					array(
						"type"        => "checkbox",
						"heading"     => esc_html__( "Product tabs Graphic", 'guardmaster' ),
						'description' => esc_html__( 'Select which products do you want to display. Try to check only one product category. Note: This is special component designed for specific purpose, for all options, please select Woocommerce components in Woocommerce tab.', 'guardmaster' ),
						"param_name"  => "product_show",
						"value"       => array(
							'Best Selling Products' => 'best_selling_products',
							'Featured Products'     => 'featured_products',
							'Recent Products'       => 'recent_products',
							'On Sale'               => 'sale_products',
							'Top Rated Products'    => 'top_rated_products',
						)
					),
					
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__( 'Per page', 'guardmaster' ),
						'value'       => 12,
						'save_always' => true,
						'param_name'  => 'per_page',
						'description' => esc_html__( 'How much items per page to show', 'guardmaster' ),
					)
				),
			)
		);

	}
	add_action( 'vc_before_init', 'tt_temptt_list_products_shortcode_fn_vc' );

	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_tt_gm_list_products_shortcode extends WPBakeryShortCode {
		}
	}
}