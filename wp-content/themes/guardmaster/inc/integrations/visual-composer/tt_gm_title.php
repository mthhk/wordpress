<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */


function tt_gm_title_fn_vc() {
	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Title block", 'guardmaster'),
			"base" => "tt_gm_title_shortcode",
			'description' => esc_html__( 'Title block', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Type of title', 'guardmaster' ),
                    'description'     => esc_html__( 'Type of title, there are 4 available.', 'guardmaster' ),
                    'param_name'  => 'title_type',
                    'value'       => array(
                        'Type - 1'    =>  'type_1',
                        'Type - 2'    =>  'type_2',
                        'Type - 3'    =>  'type_3',
                        'Type - 4'    =>  'type_4',
                    )
                ),
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Title alignment', 'guardmaster' ),
                    'description'     => esc_html__( 'Text alignment of the title', 'guardmaster' ),
                    'param_name'  => 'pos_type',
                    'value'       => array(
                        'Center'    =>  'pos_1',
                        'Left'    =>  'pos_2',
                        'Right'    =>  'pos_3',
                    )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Title', 'guardmaster' ),
                    'param_name' => 'title',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Title', 'guardmaster' )
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Title color",'guardmaster'),
                    "param_name" => "title_color",
                    "value" => "",
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Title span', 'guardmaster' ),
                    'param_name' => 'titles',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'If you want to style part of title different, please enter it in span.', 'guardmaster' ),
                    'dependency'  => array( 'element' => 'title_type', 'value' => array('type_4') )
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Title span color",'guardmaster'),
                    "param_name" => "titles_color",
                    "value" => "",
                    'dependency'  => array( 'element' => 'title_type', 'value' => array('type_4') )
                ),
                array(
                    'type' => 'textarea',
                    'heading' => esc_html__( 'Subtitle', 'guardmaster' ),
                    'param_name' => 'sbtitle',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Enter subtitle, if you need one.', 'guardmaster' ),
                    'dependency'  => array( 'element' => 'title_type', 'value' => array('type_2', 'type_3', 'type_4') )

                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Subtitle color",'guardmaster'),
                    "param_name" => "sbtitle_color",
                    "value" => "",
                    'dependency'  => array( 'element' => 'title_type', 'value' => array('type_2', 'type_3', 'type_4') )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Superscript text', 'guardmaster' ),
                    'param_name' => 'sptitle',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'This text appears between the lines, as a superscript text.', 'guardmaster' ),
                    'dependency'  => array( 'element' => 'title_type', 'value' => array('type_1', 'type_2', 'type_3') )
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_html__("Superscript color",'guardmaster'),
                    "param_name" => "sptitle_color",
                    "value" => "",
                    'dependency'  => array( 'element' => 'title_type', 'value' => array('type_1', 'type_2', 'type_3') )
                ),
                array(
                    'type'       => 'checkbox',
                    'heading'    => esc_html__( 'Show separator?', 'guardmaster' ),
                    'param_name' => 'separator',
                    'value'      => array( esc_html__( 'Yes, please', 'guardmaster' ) => 'yes' ),
                    'dependency'  => array( 'element' => 'title_type', 'value' => array('type_1') )
                ),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'guardmaster' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'guardmaster' ),
				),
	            array(
	                'type' => 'css_editor',
	                'heading' => esc_html__( 'CSS box', 'guardmaster' ),
	                'param_name' => 'css',
	                'group' => esc_html__( 'Design Options', 'guardmaster' )
	            )

			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_title_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_title_shortcode extends WPBakeryShortCode {
	}
}