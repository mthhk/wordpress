<?php
/*
 * Templatation.com
 *
 * Banner with label slider for VC
 *
 */

function tt_gm_testimonials_fn_vc() {
    vc_map(
        array(
			"icon" => 'tt-vc-block',
            'name'                    => esc_html__( 'Testimonial' , 'guardmaster' ),
            'base'                    => 'tt_gm_testimonials_shortcode',
            'description'             => esc_html__( '', 'guardmaster' ),
            'description' => esc_html__( 'Testimonial types', 'guardmaster' ),
            'as_parent'               => array('only' => 'tt_gm_testimonials_item_shortcode'),
            'content_element'         => true,
            "js_view" => 'VcColumnView',
            "category" => esc_html__('Guard Master', 'guardmaster'),
            "params" => array(
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Select type of Testimonial', 'guardmaster' ),
                    'param_name'  => 'testimonials_type',
                    'value'       => array(
                        'Left position'    =>  'style_1',
                        'Right position'     =>  'style_2',
                        'Center position'     =>  'style_3'
                    )
                ),
            ),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_testimonials_fn_vc' );
// Nested Element
function tt_gm_testimonials_item_fn_vc() {
    vc_map(
        array(
			"icon" => 'tt-vc-block',
            'name'            => esc_html__('Item Testimonial', 'guardmaster'),
            'base'            => 'tt_gm_testimonials_item_shortcode',
            'description'     => esc_html__( 'Item Testimonial', 'guardmaster' ),
            "category" => esc_html__('Guard Master', 'guardmaster'),
            'content_element' => true,
            'as_child'        => array('only' => 'tt_gm_testimonials_shortcode'), // Use only|except attributes to limit parent (separate multiple values with comma)
            'params'          => array(
                array(
                    'type'        => 'textarea',
                    'heading'     => esc_html__( 'Text testimonial', 'guardmaster' ),
                    'param_name'  => 'text',
                    'value'       => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Text testimonial', 'guardmaster' ),
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Text color', 'guardmaster' ),
                    'param_name'  => 'textcolor',
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Author', 'guardmaster' ),
                    'param_name'  => 'author',
                    'value'       => '',
                    'description' => esc_html__( 'Author name', 'guardmaster' ),
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Text color', 'guardmaster' ),
                    'param_name'  => 'acolor',
                    'value'       => '',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Position', 'guardmaster' ),
                    'param_name'  => 'position',
                    'value'       => '',
                    'description' => esc_html__( 'Author name', 'guardmaster' ),
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Position', 'guardmaster' ),
                    'param_name'  => 'poscolor',
                    'value'       => '',
                )

            ),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_testimonials_item_fn_vc' );

// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_tt_gm_testimonials_shortcode extends WPBakeryShortCodesContainer {

    }
}

// Replace Wbc_Inner_Item with your base name from mapping for nested element
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_tt_gm_testimonials_item_shortcode extends WPBakeryShortCode {

    }
}