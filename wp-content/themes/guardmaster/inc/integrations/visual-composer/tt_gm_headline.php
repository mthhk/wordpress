<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */


function tt_gm_headline_fn_vc() {

	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Headline", 'guardmaster'),
			"base" => "tt_gm_headline_shortcode",
			'description' => esc_html__( 'Different types of headline', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Select style headline', 'guardmaster' ),
                    'param_name'  => 'headline_type',
                    'value'       => array(
                        'Style - 1'    =>  'style_1',
                        'Style - 2'     =>  'style_2',
                    )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Headline title', 'guardmaster' ),
                    'param_name' => 'title',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Headline title.', 'guardmaster' )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Title color', 'guardmaster' ),
                    'param_name'  => 'titlecolor',
                    'value'       => '',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Headline subtitle', 'guardmaster' ),
                    'param_name' => 'subtitle',
                    'value' => '',
                    'description' => esc_html__( 'Headline subtitle.', 'guardmaster' ),
                    'dependency'  => array( 'element' => 'headline_type', 'value' => array('style_1') )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Subtitle color', 'guardmaster' ),
                    'param_name'  => 'sutitlecolor',
                    'value'       => '',
                    'dependency'  => array( 'element' => 'headline_type', 'value' => array('style_1') )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Link to download', 'guardmaster' ),
                    'param_name' => 'link',
                    'value' => '',
                    'description' => esc_html__( 'link', 'guardmaster' ),
                    'dependency'  => array( 'element' => 'headline_type', 'value' => array('style_2') )
                ),
			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_headline_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_headline_shortcode extends WPBakeryShortCode {
	}
}