<?php
if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/*
 * Templatation.com
 *
 * Team for VC
 *
 */
function tt_gm_message_fn_vc() {
	vc_map(
		array(
			"icon" => 'tt-vc-block',
			"name" => esc_html__("Message", 'guardmaster'),
			"base" => "tt_gm_message_shortcode",
			'description' => esc_html__( 'Alert Message', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
			"params" => array(
                array(
                    'type'      => 'dropdown',
                    'heading'     => esc_html__( 'Type message', 'guardmaster' ),
                    'param_name'  => 'ms_type',
                    'value'       => array(
                        'Style - 1'    =>  'style_1',
                        'Style - 2'     =>  'style_2',
                    )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Title', 'guardmaster' ),
                    'param_name' => 'title',
                    'value' => '',
                    'admin_label' => true,
                    'description' => esc_html__( 'Message title.', 'guardmaster' )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Title color', 'guardmaster' ),
                    'param_name'  => 'titlecolor',
                    'value'       => '',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Info', 'guardmaster' ),
                    'param_name' => 'info',
                    'value' => '',
                    'description' => esc_html__( 'Message title.', 'guardmaster' ),
                    'dependency'  => array( 'element' => 'ms_type', 'value' => array('style_1') )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Info color', 'guardmaster' ),
                    'param_name'  => 'infocolor',
                    'value'       => '',
                    'dependency'  => array( 'element' => 'ms_type', 'value' => array('style_1') )
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Message background', 'guardmaster' ),
                    'param_name'  => 'bgcolor',
                    'value'       => '',
                ),
                array(
                    'type'        => 'iconpicker',
                    'heading'     => esc_html__( 'Type icon', 'guardmaster' ),
                    'param_name'  => 'type_icon',
                    'value'       => '',
                    'settings' => array(
                        'emptyIcon' => false,
                        'iconsPerPage' => 300,
                    ),
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Icon color', 'guardmaster' ),
                    'param_name'  => 'icon_color',
                    'value'       => '',
                ),
                 
			)
		)
	);
	
}
add_action( 'vc_before_init', 'tt_gm_message_fn_vc' );

if(class_exists('WPBakeryShortCode')) {
	class WPBakeryShortCode_tt_gm_message_shortcode extends WPBakeryShortCode {
	}
}