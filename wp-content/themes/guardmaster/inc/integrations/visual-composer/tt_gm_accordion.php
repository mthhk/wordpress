<?php
/*
 * Templatation.com
 *
 * Social Accordion for VC
 *
 */

// Parent Element
function tt_gm_accordion_fn_vc() {
	vc_map( 
		array(
			"icon" => 'tt-vc-block',
		    'name'                    => esc_html__( 'Accordion' , 'guardmaster' ),
		    'base'                    => 'tt_gm_accordion_shortcode',
		    'description'             => esc_html__( 'Create accordion', 'guardmaster' ),
		    'as_parent'               => array('only' => 'tt_gm_accordion_content_shortcode'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		    'content_element'         => true,
		    "js_view" => 'VcColumnView',
			"category" => esc_html__('Guard Master', 'guardmaster'),
		) 
	);
}
add_action( 'vc_before_init', 'tt_gm_accordion_fn_vc' );

// Nested Element
function tt_gm_accordion_content_fn_vc() {
	
	vc_map(
		array(
		    'name'            => esc_html__('Accordion Content', 'guardmaster'),
		    'base'            => 'tt_gm_accordion_content_shortcode',
		    'description'     => esc_html__( 'Content Element', 'guardmaster' ),
			"category" => esc_html__('Guard Master', 'guardmaster'),
		    'content_element' => true,
		    'as_child'        => array('only' => 'tt_gm_accordion_shortcode'), // Use only|except attributes to limit parent (separate multiple values with comma)
		    'params'          => array(
		    	array(
		    		"type" => "textfield",
		    		"heading" => esc_html__("Title", 'guardmaster'),
		    		"param_name" => "title",
				    'description'     => esc_html__( 'Enter title for accordion.', 'guardmaster' ),
		    		'holder' => 'div'
		    	),
	            array(
	            	"type" => "textarea_html",
	            	"heading" => esc_html__("Block Content", 'guardmaster'),
				    'description'     => esc_html__( 'Enter content for accordion.', 'guardmaster' ),
	            	"param_name" => "content"
	            ),

		    ),
		) 
	);
}
add_action( 'vc_before_init', 'tt_gm_accordion_content_fn_vc' );

// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_tt_gm_accordion_shortcode extends WPBakeryShortCodesContainer {

    }
}

// Replace Wbc_Inner_Item with your base name from mapping for nested element
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_tt_gm_accordion_content_shortcode extends WPBakeryShortCode {

    }
}