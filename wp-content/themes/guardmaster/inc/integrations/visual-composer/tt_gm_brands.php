<?php
/*
 * Templatation.com
 *
 * Banner with label slider for VC
 *
 */

function tt_gm_brands_fn_vc() {
    vc_map(
        array(
			"icon" => 'tt-vc-block',
            'name'                    => esc_html__( 'Brands' , 'guardmaster' ),
            'base'                    => 'tt_gm_brands_shortcode',
            'description'             => esc_html__( 'Brands images', 'guardmaster' ),
            'as_parent'               => array('only' => 'tt_gm_brands_item_shortcode'),
            'content_element'         => true,
            "js_view" => 'VcColumnView',
            "category" => esc_html__('Guard Master', 'guardmaster'),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_brands_fn_vc' );
// Nested Element
function tt_gm_brands_item_fn_vc() {
    vc_map(
        array(
            'name'            => esc_html__('Brands item', 'guardmaster'),
            'base'            => 'tt_gm_brands_item_shortcode',
            'description'     => esc_html__( 'Brands item', 'guardmaster' ),
            "category" => esc_html__('Guard Master', 'guardmaster'),
            'content_element' => true,
            'as_child'        => array('only' => 'tt_gm_brands_shortcode'), // Use only|except attributes to limit parent (separate multiple values with comma)
            'params'          => array(
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__( 'Brand image', 'guardmaster' ),
                    'param_name' => 'image',
                    'value' => '',
                    'description' => esc_html__( 'Select image from media library.', 'guardmaster' ),
                ),
            ),
        )
    );
}
add_action( 'vc_before_init', 'tt_gm_brands_item_fn_vc' );

// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_tt_gm_brands_shortcode extends WPBakeryShortCodesContainer {

    }
}

// Replace Wbc_Inner_Item with your base name from mapping for nested element
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_tt_gm_brands_item_shortcode extends WPBakeryShortCode {

    }
}