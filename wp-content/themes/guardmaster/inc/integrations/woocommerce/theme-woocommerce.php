<?php
if ( ! defined( 'ABSPATH' ) ) exit;

$tt_temptt_opt = get_option( 'tt_options' );

/*-----------------------------------------------------------------------------------*/
/* This theme supports WooCommerce, woo! */
/*-----------------------------------------------------------------------------------*/

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

add_action( 'after_switch_theme', 'templatation_wc_image_dimensions', 1 );
/**
 * Define woocommerce image sizes
 */
function templatation_wc_image_dimensions() {

	global $pagenow;
	if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
		return;
	}
	if ( get_option('tt_wc_img_size_set') == '1' ) return;
  	$catalog = array(
		'width' 	=> '270',	// px
		'height'	=> '345',	// px
		'crop'		=> 1 		// true
	);
 
	$single = array(
		'width' 	=> '400',	// px
		'height'	=> '510',	// px
		'crop'		=> 1 		// true
	);
 
	$thumbnail = array(
		'width' 	=> '80',	// px
		'height'	=> '100',	// px
		'crop'		=> 1 		// false
	);
 
	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
	update_option( 'tt_wc_img_size_set', '1' ); 	// Image gallery thumbs
}

// Remove WC breadcrumb (we're using the Yoast SEO)
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 51 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

