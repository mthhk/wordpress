<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mss_wordpress' );

/** Database username */
define( 'DB_USER', 'min' );

/** Database password */
define( 'DB_PASSWORD', '9Assw0rd$#' );

/** Database hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uzyeB4=6)ME:SSm |YtB!0KZ-FO:.tYPPnx1&l2tbH;D[M5^=!Q+ncl;yWzDmd-2');
define('SECURE_AUTH_KEY',  'A1#)?`var~XG6O(U-E]+=r(P@&-6-Mu6!6+S&Cs|?!}P>V>(uo<ImUG)?`!Hcr2`');
define('LOGGED_IN_KEY',    '93W~m$9o?47*@O(V9Kbdlj-tYSuK)[FOePz ?iM/IT9Ez2QjIWBK9O_,C&l9*58L');
define('NONCE_KEY',        '{;_jrhVke7<|||uQ#2hrkqZ2F99^f6-%<n{>O:a[.FBh4p*cf7irn?qiHa[#FEp-');
define('AUTH_SALT',        'D%YrTT%=Ah_j{I4#]F/[VAfmo1-YAy2xG+t=M:(1RU-s+z}9jZcdafVphr,DrA#!');
define('SECURE_AUTH_SALT', '.[-SD/+!3e*$t3TARLV%-H+AsU>lEuZiR5f9`EeB;M6#&ad4<.Bab[=nYID2&+jn');
define('LOGGED_IN_SALT',   '|~||wfI[y+,v6)}oNwp=+JlEe%Bt]Gw+X}3N[doLZ3@gNj7okbNBRVpX&br1y]V~');
define('NONCE_SALT',       'VO>^o5VT7^uq7h.,`c^/W:-by(W7CB$fb*P_RVQteGpVv)0YawLV:-H!bL;EVW&5');
/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
